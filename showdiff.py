#!/usr/bin/python

import os, sys
import subprocess

curPwd = os.getcwd()
dstPwd = None

forbidden = ['venv', '.json', '.log', '.git', '.pyc', '.contam/db/', '.dblite', '.dbbkp', '.o', 'contam/db', '.dot', '.class', 'tree.', 'folderStruct.py', 'ui/', 'ui2/']

dev150pwd = '/mnt/nexenta/assembly/nobackup/dev_150/scripts'

if   curPwd == "/home/assembly/tomato150/scripts":
    dstPwd = dev150pwd
elif curPwd == dev150pwd:
    dstPwd = "/home/assembly/tomato150/scripts"
else:
    print "Current path is not valid: %s" % curPwd
    sys.exit(1)

print "running find"
findRes = subprocess.Popen("find . -xtype f", shell=True, stdout=subprocess.PIPE)
#findRes = subprocess.Popen("ls -l", shell=True, stdout=subprocess.PIPE)
print "find run"


files = findRes.communicate()[0]
for fileName in files.split("\n"):
    skip = False

    if len(fileName) == 0: continue

    for proi in forbidden:
        if proi in fileName:
            skip = True
            break

    if skip: continue

    filePath = os.path.abspath( fileName )
    DST = os.path.abspath(os.path.join(dstPwd, fileName))


    if not os.path.exists(filePath):
        print "SRC %s DST %s :: NO SRC" % (filePath, DST)
        continue

    if not os.path.exists(DST):
        print "SRC %s DST %s :: NO DST" % (filePath, DST)
        continue

    diffRes = subprocess.Popen('diff -Bd "%s" "%s"' % ( filePath, DST ), shell=True, stdout=subprocess.PIPE)
    diffs   = diffRes.communicate()[0]
    if len(diffs) > 0:
        print "SRC %s DST %s :: DIFF" % (filePath, DST)
        for diffLine in diffs.split("\n"):
            print "  DIFF %s" % diffLine


if findRes.returncode != 0:
    print "error running find"
    sys.exit(findRes.returncode)


#filesHere.split("\n")



#grep -vE 'venv|\.json|\.log|\.git|\.pyc|\.contam\/db\/|\.dblite|\.dbbkp|\.o|contam\/db|\.dot|\.class|tree\.' | \
#xargs -I{} -n1 bash -c 'D="'${DST}'"; bn=`readlink -f $D/"{}"`; echo "diff <{}> = <$bn>"; diff -Bd "{}" "$bn"; echo'
