if [[ "$PWD" == "/home/assembly/tomato150/scripts" ]]; then
	DST="/home/assembly/dev_150/scripts"
fi

if [[ "$PWD" == "/home/assembly/dev_150/scripts" ]]; then
	DST="/home/assembly/tomato150/scripts"
fi

if [[ -z "$DST" ]]; then
	echo $PWD not a valid folder
	exit 1
fi


find . -xtype f | \
grep -vE 'venv|\.json|\.log|\.git|\.pyc|\.contam\/db\/|\.dblite|\.dbbkp|\.o|contam\/db|\.dot|\.class|tree\.' | \
xargs -I{} -n1 bash -c 'D="'${DST}'"; bn=`readlink -f $D/"{}"`; echo "diff <{}> = <$bn>"; diff -Bd "{}" "$bn"; echo'
