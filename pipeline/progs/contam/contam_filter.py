#!/usr/bin/python
import os,sys
import re
import subprocess
from   threading  import Thread
import traceback
import atexit
import tempfile
import shutil
from   Queue import Queue, Empty
import random

# settings
#tmp_work_dir = '/home/smits065/tmp/workdir/big_job/'
#tmp_dir      = '/run/shm'
THREADS      = 10
sff_exe_path = '/home/assembly/bin/saulo/sff_extract'
tmp_dir      = '/mnt/nexenta/assembly/nobackup/tmp'
test_folder  = '/home/assembly/dev_150/scripts/aux/contam_test'
linker       = './linker.fasta'
bwa_exe      = 'bwa'
newbler_bin  = '/opt/454/2.6_1/bin'


TEST = False


def enqueue_pipe(pipe, queue):
    for line in iter(pipe.readline, b''):
        queue.put(line)
    pipe.close()



def runString(id, cmdFinal):
    try:
        sys.stdout.write(id + " :: OPENING PROCESS FOR CMD '" + cmdFinal + "'" + "\n")
        q_out = Queue()
        q_err = Queue()
        p = subprocess.Popen( cmdFinal, shell=True,
            stdout = subprocess.PIPE,
            stderr = subprocess.PIPE )
        #http://stackoverflow.com/questions/375427/non-blocking-read-on-a-subprocess-pipe-in-python
        t_err = Thread(target=enqueue_pipe, args=(p.stderr, q_err))
        t_err.daemon = True # thread dies with the program
        t_err.start()
        t_out = Thread(target=enqueue_pipe, args=(p.stdout, q_out))
        t_out.daemon = True # thread dies with the program
        t_out.start()
        pid           = p.pid
        try:

            sys.stdout.write(id + " :: CHECKING POOL" + "\n")
            while p.poll() is None:
                #print "JOB :: " + id + " :: TRYING TO READ PIPE (" + str(p.poll()) + ")"
                try:
                    #lineOut = q_out.get_nowait()
                    lineOut = q_out.get(timeout=1)
                    sys.stdout.write(id + " :: " + lineOut)
                except Empty:
                    pass
                    #print('no stderr output yet')
                #else: # got line


                try:
                    #lineErr = q_err.get_nowait()
                    lineErr = q_err.get(timeout=1)
                    sys.stderr.write(id + " :: " + lineErr)
                except Empty:
                    pass
                    #print('no stderr output yet')
                #else: # got line



            sys.stdout.write(id + " :: GETTING RETURN CODE" + "\n")
            sys.stdout.write(id + " :: WAITING" + "\n")
            #print "WAITING"
            exitCode = p.returncode
            p.wait()

            sys.stdout.write(id + " :: JOINING THREADS" + "\n")
            t_err.join()
            t_out.join()

            sys.stdout.write(id + " :: EMPTYING QUEUE" + "\n")
            while not q_out.empty():
                sys.stdout.write(id + " :: " + q_out.get_nowait() + "\n")
            while not q_err.empty():
                sys.stderr.write(id + " :: " + q_err.get_nowait() + "\n")

            sys.stdout.write(id + " :: QUEUE EMPTY" + "\n")

            if exitCode:
                sys.stderr.write(id + " :: STR {" + cmdFinal + "} :: RETURNED: " + str(exitCode) + " THEREFORE FAILED " + "\n")
                sys.stderr.write(id + " :: FAILED TO RUN " + cmdFinal + " :: RETURNED: " + str(exitCode) + " THEREFORE FAILED " + "\n")
                return(exitCode)
            sys.stdout.write(id + " :: REACHED END. FINISHING WITH STATUS " + str(exitCode) + "\n")

            return exitCode

        except Exception, e:
            sys.stderr.write(id + " :: Exception (Job__launch_out): " + str(e) + "\n")
            sys.stderr.write(id + " :: FAILED TO RUN " + cmdFinal + " EXCEPTION " + str(e) + "\n")
            sys.stderr.write(id + " :: TRACEBACK :: "  + " ".join(traceback.format_stack()) + "\n")
            exitCode = 252
            return exitCode

    except Exception, e:
        sys.stderr.write(id + " :: Exception (Job__launch): " + str(e) + "\n")
        sys.stderr.write(id + " :: FAILED TO RUN " + cmdFinal + " EXCEPTION " + str(e) + "\n")
        sys.stderr.write(id + " :: TRACEBACK :: "  + " ".join(traceback.format_stack()) + "\n")
        exitCode = 253
        return exitCode



def exec_cmd(cmd):
    commands = 'contam_filter'
    print cmd, '\n'
    r = runString(str(commands), cmd)
    if r != 0:
        raise Exception, cmd



# logic for sam handling
def sam_reader(sam_file, out_file_name, max_percentage_contamination):
    fh = open(sam_file, 'r')
    out_file = open(out_file_name, 'a')
    filter_dic = {}
    filter_set = set()
    for line in fh:
        split_line = line.split('\t')
        if len(split_line) >= 5:
            star = split_line[2]
            if star != '*':
                tabbed = line.split('\t')
                seq_length = len(tabbed[9])
                match_list = re.findall('(\d+)M', tabbed[5])
                match_length = 0
                for match in match_list:
                    match_length += int(match)
                percentage = (match_length/float(seq_length)) * 100
                if percentage >= max_percentage_contamination or percentage == 0: # if percentage == 0, no cigar available so filter anyway
                    filter_dic[split_line[0]] = True
                    filter_set.add(split_line[0])
                    out_file.write("%s\n" %(split_line[0]))

    out_file.close()
  #  exec_cmd("mv %s/contamination.txt.tmp %s/contamination.txt" %(tmp_work_dir, out_dir))
    return filter_dic



def filter_fastq(fh, out_file, filter_dic):
    write = True
    line = True
    count = 0
    #print filter_dic

    while line:
        line = fh.readline()

        try:
            split_line = line.split()

            header_id = line.split()[0][1:]
            header_id = header_id[:-2]

        except IndexError:
            header_id = ''
            print line, 'end of file'
            line = False
            break

        if filter_dic.has_key(header_id):
            write = False
            count += 1
        else:
            write = True
##        for item in filter_set:
##            if item in line:
##                write = False
        for i in range(0, 3):
            linep  = fh.readline()
            if len(linep) > 1500:
                print "line too big", len(linep)
                sys.exit(1)
            line  += linep

        if write:
            out_file.write(line)

        write = True

    print count, 'sequences filtered in this file!'
    out_file.close()



def cleanTmp(tmp_index_dir):
    if os.path.exists( tmp_index_dir ):
        shutil.rmtree( tmp_index_dir )
        pass


def makeTmp(output):
    #tmp_index_dir = tempfile.mkdtemp(prefix='contamfilter_', dir='/run/shm')
    tmp_index_dir = tempfile.mkdtemp(prefix='contamfilter_', dir=tmp_dir)
    #tmp_index_dir = '/run/shm/contamfilter_tmp'
    if not os.path.exists(tmp_index_dir):
        os.makedirs(tmp_index_dir)

    outputTmp     = output
    outputTmp     = os.path.basename(outputTmp)
    outputTmp     = os.path.join(tmp_index_dir, outputTmp)
    return (tmp_index_dir, outputTmp)



def main():
    db                           = None
    max_percentage_contamination = None
    contamination_file           = None

    fw_reads                     = None
    rv_reads                     = None
    fw_reads_out                 = None
    rv_reads_out                 = None

    # parameters
    if TEST:
        fw_reads                     = ''
        rv_reads                     = ''
        rv_reads_out                 = ''
        fw_reads_out                 = '%s/fw_reads_filtered.fastq' % test_folder
        rv_reads_out                 = '%s/rv_reads_filtered.fastq' % test_folder
        db                           = '%s/test.fa'                 % test_folder
        max_percentage_contamination = 97
    else:
        if len(sys.argv) != 8 and len(sys.argv) != 6:
            print "invalid number of arguments %d" % len(sys.argv)
            print "usage:"
            print"  %s <bwa_db> <max_percentage_contamination> <contamination_file> <fw_reads.fastq> <rv_reads.fastq> <fw_reads_filtered.fastq> <rv_reads_filtered.fastq> " % (sys.argv[0])
            print " %s <bwa_db> <max_percentage_contamination> <contamination_file> <fw_reads.fastq> <fw_reads_filtered.fastq>"                                             % (sys.argv[0])
            exit(1)

        paired = None

        if   len(sys.argv) == 8:
            paired = True
        elif len(sys.argv) == 6:
            paired  = False

        db                           =     sys.argv[1]
        if not os.path.exists(db + '.bwt'):
            print 'database file does not exists: %s' % db
            sys.exit(3)
        db = os.path.abspath(db)


        try:
            max_percentage_contamination = int(sys.argv[2])
        except ValueError:
            print 'percentage value not valid: %s' % str(max_percentage_contamination)
            sys.exit(3)

        contamination_file           =     sys.argv[3]
        contamination_file           = os.path.abspath(contamination_file)

        if paired:
            fw_reads                 =     sys.argv[4]
            rv_reads                 =     sys.argv[5]
            fw_reads_out             =     sys.argv[6]
            rv_reads_out             =     sys.argv[7]

            if not os.path.exists(rv_reads):
                print 'reverse read %s does not exists' % rv_reads
                sys.exit(2)
            rv_reads     = os.path.abspath(rv_reads    )
            rv_reads_out = os.path.abspath(rv_reads_out)
        else:
            fw_reads                 =     sys.argv[4]
            fw_reads_out             =     sys.argv[5]

        if not os.path.exists(fw_reads):
            print 'forward read %s does not exists' % fw_reads
            sys.exit(2)

        fw_reads     = os.path.abspath(fw_reads    )
        fw_reads_out = os.path.abspath(fw_reads_out)


    (tmp_work_dir, outputTmp) = makeTmp('none')

    atexit.register(cleanTmp, tmp_work_dir)

    os.chdir(tmp_work_dir)

    if fw_reads.endswith('.fastq'):
        # create sam
        aln1       = os.path.join(tmp_work_dir, "aln1.aln")
        cmd2       = "%s aln -t %s -f %s %s %s" % (bwa_exe, THREADS, aln1, db, fw_reads)
        alignments = aln1
        reads      = fw_reads
        try:
            exec_cmd(cmd2)
        except:
            print "error running command:", cmd2
            sys.exit(1)

        if paired:
            aln2       = os.path.join(tmp_work_dir, "aln2.aln")
            cmd2b      = "%s aln -t %s -f %s %s %s" % (bwa_exe, THREADS, aln2, db, rv_reads)
            alignments += " " + aln2
            reads      += " " + rv_reads
            try:
                exec_cmd(cmd2b)
            except:
                print "error running command:", cmd2b
                sys.exit(1)


        samfile    = os.path.join(tmp_work_dir, 'bwa.sam')
        contamFile =  samfile + ".contamination.txt"
        #exec_cmd("cat %s %s > %s/fw_and_rv.fastq" %(fw_reads, rv_reads, tmp_work_dir))
        #exec_cmd("bwa bwasw -t 30 %s %s/fw_and_rv.fastq -f %s/bwa.sam" %(db, tmp_work_dir, tmp_work_dir))
        cmd3 = "%s sampe -f %s %s %s %s" % (bwa_exe, samfile, db, alignments, reads )
        try:
            exec_cmd(cmd3)

        except:
            print "error running command:", cmd3
            sys.exit(1)

        filter_dic     = sam_reader(samfile, contamFile, max_percentage_contamination)


        fw_out_fn      = os.path.join(tmp_work_dir, "fw_reads.fastq")
        fw_out_file_fh = open(fw_out_fn,'w')
        fw_file_fh     = open(fw_reads, 'r')
        out_dir        = os.path.dirname(fw_reads_out)
        if not os.path.exists(out_dir):
            os.makedirs(out_dir)
        filter_fastq(fw_file_fh, fw_out_file_fh, filter_dic)
        shutil.move(fw_out_fn, fw_reads_out)

        if paired:
            rv_out_fn      = os.path.join(tmp_work_dir, "rv_reads.fastq")
            rv_out_file_fh = open(rv_out_fn,'w')
            rv_file_fh     = open(rv_reads, 'r')
            filter_fastq(rv_file_fh, rv_out_file_fh, filter_dic)
            out_dir = os.path.dirname(rv_reads_out)
            if not os.path.exists(out_dir):
                os.makedirs(out_dir)
            shutil.move(rv_out_fn, rv_reads_out)


        shutil.copy(contamFile, contamination_file)
    elif fw_reads.endswith('.sff'):
        if paired:
            print "can't be paired and sff at the same time"
            sys.exit(1)


        # create fasta file
        #exec_cmd("/opt/454/2.6_1/bin/sffinfo -s %s > %s/out.fasta" %(sff_file, tmp_work_dir))
        fastqFileName = os.path.join(tmp_work_dir, 'out.fastq')
        cmd1 = "%s -l %s %s -s %s" %(sff_exe_path, linker, fw_reads, fastqFileName)
        exec_cmd(cmd1)



        # create sam
        samfile    = os.path.join(tmp_work_dir, 'bwa.sam')
        contamFile =  samfile + ".contamination.txt"
        cmd2 = "%s bwasw -t %s %s %s -f %s" %(bwa_exe, THREADS, db, fastqFileName, samfile)
        exec_cmd(cmd2)



        sam_reader(samfile, contamFile, max_percentage_contamination)
        # todo get contamiation file, clean_file_ids


        # create sff files
        #exec_cmd("/opt/454/2.6_1/bin/sfffile -i %s/contamination.txt -o %s/contamination.sff.tmp %s" %(out_dir, tmp_work_dir, sff_file))
        filteredTmp = os.path.join(tmp_work_dir, 'filtered.sff')
        cmd3 = "%s/sfffile -e %s -o %s %s" %(newbler_bin, contamFile, filteredTmp, fw_reads)
        exec_cmd(cmd3)




        out_dir = os.path.dirname(fw_reads_out)
        if not os.path.exists(out_dir):
            os.makedirs(out_dir)

        shutil.copy(filteredTmp, fw_reads_out)
        shutil.copy(contamFile, contamination_file)


if __name__=="__main__": main()
