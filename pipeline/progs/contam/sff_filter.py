#!/usr/bin/python
import os
import re
import sys

import subprocess
from threading  import Thread
import sys, traceback
import tempfile
from Queue import Queue, Empty
import random

# settings
#tmp_work_dir   = '/home/smits065/tmp/workdir/big_job/'
tmp_work_dir   = '/mnt/nexenta/assembly/nobackup/tmp'
tmp_dir        = '/mnt/nexenta/assembly/nobackup/tmp'
tmp_dir        = '/run/shm'
sff_exe_path   = '/home/assembly/bin/saulo/sff_extract'
test_folder    = '/home/assembly/dev_150/scripts/aux/contam_test'
linker         = '/home/assembly/dev_150/scripts/aux/contam_db/linker.fasta'
bwa_exe        = 'bwa'
newbler_bin    = '/opt/454/2.6_1/bin'
sff_filter_exe = 'sff_filter'

THREADS        = 18
TEST           = False




# parameters


def enqueue_pipe(pipe, queue):
    for line in iter(pipe.readline, b''):
        queue.put(line)
    pipe.close()



def runString(id, cmdFinal):
    try:
        sys.stdout.write(id + " :: OPENING PROCESS FOR CMD '" + cmdFinal + "'" + "\n")
        q_out = Queue()
        q_err = Queue()
        p = subprocess.Popen( cmdFinal, shell=True,
            stdout = subprocess.PIPE,
            stderr = subprocess.PIPE )
        #http://stackoverflow.com/questions/375427/non-blocking-read-on-a-subprocess-pipe-in-python
        t_err = Thread(target=enqueue_pipe, args=(p.stderr, q_err))
        t_err.daemon = True # thread dies with the program
        t_err.start()
        t_out = Thread(target=enqueue_pipe, args=(p.stdout, q_out))
        t_out.daemon = True # thread dies with the program
        t_out.start()
        pid           = p.pid
        try:

            sys.stdout.write(id + " :: CHECKING POOL" + "\n")
            while p.poll() is None:
                #print "JOB :: " + id + " :: TRYING TO READ PIPE (" + str(p.poll()) + ")"
                try:
                    #lineOut = q_out.get_nowait()
                    lineOut = q_out.get(timeout=1)
                    sys.stdout.write(id + " :: " + lineOut)
                except Empty:
                    pass
                    #print('no stderr output yet')
                #else: # got line


                try:
                    #lineErr = q_err.get_nowait()
                    lineErr = q_err.get(timeout=1)
                    sys.stderr.write(id + " :: " + lineErr)
                except Empty:
                    pass
                    #print('no stderr output yet')
                #else: # got line



            sys.stdout.write(id + " :: GETTING RETURN CODE" + "\n")
            sys.stdout.write(id + " :: WAITING" + "\n")
            #print "WAITING"
            exitCode = p.returncode
            p.wait()

            sys.stdout.write(id + " :: JOINING THREADS" + "\n")
            t_err.join()
            t_out.join()

            sys.stdout.write(id + " :: EMPTYING QUEUE" + "\n")
            while not q_out.empty():
                sys.stdout.write(id + " :: " + q_out.get_nowait() + "\n")
            while not q_err.empty():
                sys.stderr.write(id + " :: " + q_err.get_nowait() + "\n")

            sys.stdout.write(id + " :: QUEUE EMPTY" + "\n")

            if exitCode:
                sys.stderr.write(id + " :: STR {" + cmdFinal + "} :: RETURNED: " + str(exitCode) + " THEREFORE FAILED " + "\n")
                sys.stderr.write(id + " :: FAILED TO RUN " + cmdFinal + " :: RETURNED: " + str(exitCode) + " THEREFORE FAILED " + "\n")
                return(exitCode)
            sys.stdout.write(id + " :: REACHED END. FINISHING WITH STATUS " + str(exitCode) + "\n")

            return exitCode

        except Exception, e:
            sys.stderr.write(id + " :: Exception (Job__launch_out): " + str(e) + "\n")
            sys.stderr.write(id + " :: FAILED TO RUN " + cmdFinal + " EXCEPTION " + str(e) + "\n")
            sys.stderr.write(id + " :: TRACEBACK :: "  + " ".join(traceback.format_stack()) + "\n")
            exitCode = 252
            return exitCode

    except Exception, e:
        sys.stderr.write(id + " :: Exception (Job__launch): " + str(e) + "\n")
        sys.stderr.write(id + " :: FAILED TO RUN " + cmdFinal + " EXCEPTION " + str(e) + "\n")
        sys.stderr.write(id + " :: TRACEBACK :: "  + " ".join(traceback.format_stack()) + "\n")
        exitCode = 253
        return exitCode


def exec_cmd(cmd):
    commands = sff_filter_exe
    print cmd, '\n'
    r = runString(str(commands), cmd)
    if r != 0:
        raise Exception, cmd



# logic for sam handling
def sam_reader(sam_file):
    fh = open(sam_file, 'r')
    out_file = open("%scontamination.txt.tmp" %(tmp_work_dir), 'a')
    filter_dic = {}
    filter_set = set()
    for line in fh:
        split_line = line.split('\t')
        if len(split_line) >= 5:
            star = split_line[2]
            if star != '*':
                tabbed = line.split('\t')
                seq_length = len(tabbed[9])
                match_list = re.findall('(\d+)M', tabbed[5])
                match_length = 0
                for match in match_list:
                    match_length += int(match)
                percentage = (match_length/float(seq_length)) * 100
                if percentage >= max_percentage_contamination:
                    filter_dic[split_line[0]] = True
                    filter_set.add(split_line[0])
                    out_file.write("%s\n" %(split_line[0]))

    out_file.close()
    exec_cmd("mv %s/contamination.txt.tmp %s" %(tmp_work_dir, contamination_file))




def main():
    db                           = None
    max_percentage_contamination = None
    contamination_file           = None
    sff_file                     = None
    out_sff                      = None

    if TEST:
        out_sff  = '%s/bla.sff' % test_folder
        sff_file = '%s/out.sff' % test_folder
        db       = '%s/test.fa' % test_folder
        max_percentage_contamination = 97
    else:
        if len(sys.argv) != 6:
            print "usage:\n%s <bwa_db> <max_percentage_contamination> <contamination_file> <input_sff_file> <output_sff_file_clean>"%(sys.argv[0])
            exit(0)

        db                           =     sys.argv[0]
        max_percentage_contamination = int(sys.argv[1])
        contamination_file           =     sys.argv[2]
        sff_file                     =     sys.argv[3]
        out_sff                      =     sys.argv[4]

    sff_file_name = os.path.basename(sff_file)



    # create outdir
    out_dir = os.path.dirname(out_sff)
    exec_cmd("mkdir -p %s" %(out_dir))

    tmp_work_dir += str(random.randint(0, 9999999999999))+'/'
    exec_cmd("mkdir -p %s" %(tmp_work_dir))
    os.chdir(tmp_work_dir)

    # create fasta file
    #exec_cmd("/opt/454/2.6_1/bin/sffinfo -s %s > %s/out.fasta" %(sff_file, tmp_work_dir))
    exec_cmd("%s -l %s %s -s %s/out.fastq" %(sff_exe_path, linker, sff_file, tmp_work_dir))

    # create sam
    exec_cmd("%s bwasw -t %s %s %s/out.fastq -f %s/bwa.sam" %(bwa_exe, THREADS, db, tmp_work_dir, tmp_work_dir))




    sam_reader("%s/bwa.sam" %(tmp_work_dir))
    # todo get contamiation file, clean_file_ids


    # create sff files
    #exec_cmd("/opt/454/2.6_1/bin/sfffile -i %s/contamination.txt -o %s/contamination.sff.tmp %s" %(out_dir, tmp_work_dir, sff_file))
    exec_cmd("%s/sfffile -e %s -o %s/filtered.sff.tmp %s" %(newbler_bin, contamination_file, tmp_work_dir, sff_file))
    exec_cmd("mv %s/filtered.sff.tmp %s" %(tmp_work_dir, out_sff))
    #exec_cmd("mv %s/contamination.sff.tmp %s/%s_contamination.sff" %(tmp_work_dir, out_dir, sff_file_name))



    # clean up workdir
    exec_cmd("rm %s/*" %(tmp_work_dir))
    exec_cmd("rmdir %s" %(tmp_work_dir))


if __name__ == '__main__': main()
