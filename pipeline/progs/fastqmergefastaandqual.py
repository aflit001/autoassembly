#!/usr/bin/python
import sys
from Bio import SeqIO
from Bio.SeqIO.QualityIO import PairedFastaQualIterator as pfqi

infasta  = sys.argv[1]
inqual   = sys.argv[2]
outfastq = sys.argv[3]
handle   = open(outfastq, "w")
print "acquiring records"
records  = pfqi(open(infasta), open(inqual))
print "converting"
count    = SeqIO.write(records, handle, 'fastq')
handle.close()
print "converted %i records" % count
