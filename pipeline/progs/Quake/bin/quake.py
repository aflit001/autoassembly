#!/usr/bin/env python
from optparse import OptionParser, OptionGroup, SUPPRESS_HELP
from math import log, ceil
import os, random, sys, math, subprocess, pdb, shutil
import cov_model

import multiprocessing
import traceback
import signal
import time

################################################################################
# quake.py
#
# Launch full pipeline to correct errors
################################################################################

quake_dir     = os.path.abspath(os.path.dirname(sys.argv[0]))
jellyfish_dir = quake_dir
quake_bin     = '/home/assembly/bin/Quake/bin'

################################################################################
# main
################################################################################
def main():
    usage = 'usage: %prog [options]'
    parser = OptionParser(usage)

    # General options
    parser.add_option('-r',                dest='readsf',                                                        help='Fastq file of reads')
    parser.add_option('-f',                dest='reads_listf',                                                   help='File containing fastq file names, one per line or two per line for paired end reads.')
    parser.add_option('-k',                dest='k',                                  type='int',                help='Size of k-mers to correct')
    parser.add_option('-p',                dest='proc',                               type='int', default=4,     help='Number of processes [default: %default]')
    parser.add_option('-q',                dest='quality_scale',                      type='int', default=-1,    help='Quality value ascii scale, generally 64 or 33. If not specified, it will guess.')
    parser.add_option('--tmp',             dest='tmpFolder',                                                     help='Temporary Folder')
    parser.add_option('--dst',             dest='dstFolder',                                                     help='Destination Folder')

    # Count options
    count_group = OptionGroup(parser, 'K-mer counting')
    count_group.add_option('--no_jelly',   dest='no_jelly',      action='store_true',             default=False, help='Count k-mers using a simpler program than Jellyfish')
    count_group.add_option('--no_count',   dest='no_count',      action='store_true',             default=False, help='Kmers are already counted and in expected file [reads file].qcts or [reads file].cts [default: %default]')
    count_group.add_option('--int',        dest='count_kmers',   action='store_true',             default=False, help='Count kmers as integers w/o the use of quality values [default: %default]')
    count_group.add_option('--count_only', dest='count_only',    action='store_true',             default=False, help=SUPPRESS_HELP)
    count_group.add_option('--hash_size',  dest='hash_size',                          type='int',                help='Jellyfish hash-size parameter. Quake will estimate using k if not given')
    parser.add_option_group(count_group)

    # Model options
    model_group = OptionGroup(parser, 'Coverage model')
    model_group.add_option('--no_cut',     dest='no_cut',        action='store_true',             default=False, help='Coverage model is optimized and cutoff was printed to expected file cutoff.txt [default: %default]')
    model_group.add_option('--ratio',      dest='ratio',                              type='int', default=200,   help='Likelihood ratio to set trusted/untrusted cutoff.  Generally set between 10-1000 with lower numbers suggesting a lower threshold. [default: %default]')
    model_group.add_option('--cutoff',     dest='cutoff',                             type='int', default=0,     help='Manually set cutoff. [default: %default]')
    parser.add_option_group(model_group)

    # Correct options
    correct_group = OptionGroup(parser, 'Correction')
    correct_group.add_option('-l',         dest='min_read',                           type='int', default=30,    help='Return only reads corrected and/or trimmed to <min_read> bp')
    correct_group.add_option('-u',         dest='out_err',       action='store_true',             default=False, help='Output error reads even if they can\'t be corrected, maintaing paired end reads')
    correct_group.add_option('-t',         dest='trim_par',                           type='int', default=3,     help='Use BWA-like trim parameter <trim_par>')
    correct_group.add_option('--headers',  dest='headers',       action='store_true',             default=False, help='Output only the original read headers without correction messages')
    correct_group.add_option('--log',      dest='log',           action='store_true',             default=False, help='Output a log of all corrections into *.log as "quality position new_nt old_nt"')
    parser.add_option_group(correct_group)

    # help='Model kmer coverage as a function of GC content of kmers [default: %default]'
    parser.add_option('--gc',              dest='model_gc',      action='store_true',             default=False, help=SUPPRESS_HELP)

    print "parsing arguments"
    (options, args) = parser.parse_args()




    if not options.readsf and not options.reads_listf:
        parser.error('Must provide fastq file of reads with -r or file with list of fastq files of reads with -f')




    dstFolder = options.dstFolder
    if dstFolder is None:
        dstFolder = os.getcwd()
        print "getting path",dstFolder



    tmpFolder = options.tmpFolder
    if tmpFolder is not None:
        if not os.path.exists(tmpFolder):
            os.makedirs(tmpFolder)
        print "going to tmp",tmpFolder
        os.chdir(tmpFolder)




    if not options.k:
        parser.error('Must provide k-mer size with -k')
    elif options.k > 20:
        print >> sys.stderr, 'k=%d is a high k-mer value and should be reserved for enormous genomes (see the FAQ on the Quake website). Nevertheless, the program will proceed assuming you have chosen well.' % options.k



    if options.count_kmers:
        cts_suf = 'cts'
    else:
        cts_suf = 'qcts'



    readsSplits = []
    ctsf        = None
    if options.readsf:
        #ctsf      = '%s.%s' % (os.path.splitext( os.path.split(options.readsf)[1] )[0], cts_suf)
        ctsf      = '%s.%s' % ( options.readsf, cts_suf)
        #reads_str = '-r %s' % options.readsf
        readsSplits.append( { options.readsf: None } )

    else:
        #ctsf = '%s.%s' % (os.path.split(options.reads_listf)[1], cts_suf)
        ctsf = '%s.%s' % (options.reads_listf, cts_suf) #saulo

        #reads_str = '-f %s' % options.reads_listf

        splitsCount = 0
        with open(options.reads_listf, 'r') as f:
            for line in f:
                line         = line.rstrip()
                #print "line '%s'" % line
                if line == '': continue
                newListFileName       = "%s.%04d" % (options.reads_listf, splitsCount)
                newListFileName       = os.path.basename(newListFileName)
                splitsCount          += 1
                files                 = []
                data                  = {}
                data[newListFileName] = {}

                exists = True
                for origFileName in line.split('\t'):
                    fileTmpName  = origFileName.replace('/', '_').replace('\\', '_').replace('-', '_').replace('__', '_').replace('__', '_')
                    fileTmpPath  = os.path.join(tmpFolder, fileTmpName)
                    errorTmpPath = os.path.join(tmpFolder, 'error_model.' + fileTmpName)
                    errorDstPath = origFileName


                    #print "linking %s to %s" % ( origFileName, fileTmpPath )
                    if os.path.exists(origFileName):
                        if not os.path.exists(fileTmpPath):
                            os.symlink(origFileName, fileTmpPath)
                    else:
                        print "source file %s does not exists" % origFileName
                        sys.exit(1)

                    files.append(fileTmpPath)
                    data[newListFileName][fileTmpPath] = []


                    fileTmpBase = None
                    if fileTmpPath[-6:] == '.fastq':
                        fileTmpBase  = fileTmpPath[ :-6]
                        errorTmpPath = errorTmpPath[:-6] + '.txt'
                    else:
                        print "NOT FASTQ FILE: %s" % fileTmpPath
                        sys.exit(1)


                    fileDstBase = None
                    if origFileName[-6:] == '.fastq':
                        fileDstBase  = origFileName[:-6]
                        errorDstPath = errorDstPath[:-6] + '.quake.error_model.txt'
                    else:
                        print "NOT FASTQ FILE: %s" % origFileName
                        sys.exit(1)


                    #4.0K cutoff.txt
                    # 20K error_model._mnt_nexenta_assembly_nobackup_dev_150_sample_heinz_tiny_tmp_illumina_PE_500_sample_R1_001_fastq_seq.fastq.contamCleaned.txt
                    # 20K error_model._mnt_nexenta_assembly_nobackup_dev_150_sample_heinz_tiny_tmp_illumina_PE_500_sample_R2_001_fastq_seq.fastq.contamCleaned.txt
                    # 64K kmers.txt
                    #4.0K _mnt_nexenta_assembly_nobackup_dev_150_sample_heinz_tiny_tmp_illumina_MP_5000_3018DAAXX_4_f_fastq_seq.fastq.contamCleaned.fastq
                    #4.0K _mnt_nexenta_assembly_nobackup_dev_150_sample_heinz_tiny_tmp_illumina_MP_5000_3018DAAXX_4_r_fastq_seq.fastq.contamCleaned.fastq
                    # 18G _mnt_nexenta_assembly_nobackup_dev_150_sample_heinz_tiny_tmp_illumina_PE_500_sample_R1_001_fastq_seq.fastq.contamCleaned.cor.fastq
                    # 11G _mnt_nexenta_assembly_nobackup_dev_150_sample_heinz_tiny_tmp_illumina_PE_500_sample_R1_001_fastq_seq.fastq.contamCleaned.cor_single.fastq
                    #   0 _mnt_nexenta_assembly_nobackup_dev_150_sample_heinz_tiny_tmp_illumina_PE_500_sample_R1_001_fastq_seq.fastq.contamCleaned.fastq
                    #161M _mnt_nexenta_assembly_nobackup_dev_150_sample_heinz_tiny_tmp_illumina_PE_500_sample_R1_001_fastq_seq.fastq.contamCleaned.fastq.log
                    #4.0K _mnt_nexenta_assembly_nobackup_dev_150_sample_heinz_tiny_tmp_illumina_PE_500_sample_R1_001_fastq_seq.fastq.contamCleaned.stats.txt
                    #   0 _mnt_nexenta_assembly_nobackup_dev_150_sample_heinz_tiny_tmp_illumina_PE_500_sample_R2_001_fastq_seq.fastq.contamCleaned.fastq
                    #398M _mnt_nexenta_assembly_nobackup_dev_150_sample_heinz_tiny_tmp_illumina_PE_500_sample_R2_001_fastq_seq.fastq.contamCleaned.fastq.log
                    #4.0K _mnt_nexenta_assembly_nobackup_dev_150_sample_heinz_tiny_tmp_illumina_PE_500_sample_R2_001_fastq_seq.fastq.contamCleaned.stats.txt
                    #4.0K r.log
                    #4.0K sample_heinz_tiny.quake.lst.0000
                    #4.0K sample_heinz_tiny.quake.lst.0001


                    for extNfo in [
                                    ['.cor.fastq'       , True ],
                                    ['.cor_single.fastq', True ],
                                    ['.err_single.fastq', False],
                                    ['.err.fastq'       , False],
                                    ['.fastq.log'       , True ],
                                    ['.stats.txt'       , True ]]:
                        ext, compulsory = extNfo
                        outFile         = fileTmpBase + ext
                        dstFile         = fileDstBase + '.quake' + ext
                        exists          = exists and os.path.exists(dstFile)
                        data[newListFileName][fileTmpPath].append( [ outFile, dstFile, compulsory ] )

                    data[newListFileName][fileTmpPath].append( [ errorTmpPath, errorDstPath, False    ] )


                if not exists:
                    readsSplits.append(data)
                    with open(newListFileName, 'w') as g:
                        g.write("\t".join(files) + '\n')


    #import pprint
    #pprint.pprint(readsSplits)
    #sys.exit(0)


    if options.quality_scale == -1:
        print "guessing quality scale"
        quality_scale = guess_quality_scale(options.readsf, options.reads_listf)
    else:
        quality_scale = options.quality_scale


    ############################################
    # count kmers
    ############################################
    if not options.no_count and not options.no_cut:
        print "not ready for that yet"
        sys.exit(1)
        if options.no_jelly:
            count_kmers(options.readsf, options.reads_listf, options.k, ctsf, quality_scale)
        else:
            jellyfish(options.readsf, options.reads_listf, options.k, ctsf, quality_scale, options.hash_size, options.proc)

        if options.count_only:
            exit(0)

    ############################################
    # model coverage
    ############################################
    kmerFile   = os.path.join(dstFolder, 'kmers.txt' )
    cutoffFile = os.path.join(dstFolder, 'cutoff.txt')
    cutoff     = None

    if options.cutoff == 0:
        if not options.no_cut:
            print "cut"
            # clear file


            if os.path.isfile(cutoffFile):
                print 'cut - removing remote cutoff file'
                os.remove(cutoffFile)

            if options.count_kmers:
                print "cut - count kmer %s / %d" % ( ctsf, options.ratio )

                if os.path.exists('cutoff.txt'):
                    print 'cut - removing local cutoff file'
                    os.remove('cutoff.txt')

                cov_model.model_cutoff(ctsf, options.ratio)

                if not os.path.exists('cutoff.txt'):
                    print "error creating cutoff"
                    sys.exit(1)

                cutoff = get_cutoff()
                print 'Cutoff: %.f' % float(cutoff)
                if float(cutoff) < 1:
                    cutoff = '1'


            else:
                print "cut - no count kmer"
                if options.model_gc:
                    print "cut - no count kmer - model gc %s" % ctsf
                    cov_model.model_q_gc_cutoffs(ctsf, 15000, options.ratio)
                    shutil.copy('kmers.txt', kmerFile)

                else:
                    print "cut - no count kmer - model cutoff %s" % ctsf
                    # clear file

                    if os.path.isfile(cutoffFile):
                        print 'cut - no count kmer - model cutoff - removing %s' % cutoffFile
                        os.remove(cutoffFile)

                    if os.path.isfile(kmerFile):
                        print 'cut - no count kmer - model cutoff - removing %s' % kmerFile
                        os.remove(kmerFile)

                    if not (os.path.exists('kmers.txt') and os.path.exists('cutoff.txt')):
                        print 'cut - no count kmer - model cutoff - calculating cutoff'
                        cov_model.model_q_cutoff(ctsf, 100000, options.ratio)

                        print 'cut - no count kmer - model cutoff - calculating cutoff - generating'
                        cutoff = get_cutoff()
                        print 'Cutoff: %.f' % float(cutoff)
                        if float(cutoff) < 1:
                            cutoff = '1'

                        if not (os.path.exists('kmers.txt') and os.path.exists('cutoff.txt')):
                            print "error calculating cutoff (kmers %s cutoff %s)" % (os.path.exists('kmers.txt'), os.path.exists('cutoff.txt'))
                            sys.exit(1)
                    else:
                        print "cut - no count kmer - model cutoff - cutoff and kmers already exists. let them be"
                        cutoff = get_cutoff()

                        print 'Cutoff: %.f' % float(cutoff)
                        if float(cutoff) < 1:
                            cutoff = '1'

                shutil.copy('kmers.txt',  kmerFile  )
                shutil.copy('cutoff.txt', cutoffFile)


        else:
            print "no cut"

            if not os.path.exists('cutoff.txt'):
                if os.path.exists(cutoffFile):
                    print "no cut - copying remote cutoff"
                    shutil.copy(cutoffFile, 'cutoff.txt')
                else:
                    print "no cut - couldn't find remote cutoff",cutoffFile
                    sys.exit(1)

            if not os.path.exists('kmers.txt'):
                if os.path.exists(kmerFile):
                    print "no cut - copying remote kmers"
                    shutil.copy(kmerFile, 'kmers.txt')
                else:
                    print "no cut - couldn't find remote kmer",kmerFile
                    sys.exit(1)

            cutoff = get_cutoff()

    else:
        cutoff = str(options.cutoff)

        with open('cutoff.txt', 'w') as c:
            c.write(str(cutoff) + "\n")
            c.write("manual\n")

        with open('kmers.txt', 'w') as k:
            k.write("none\n")

    print 'Cutoff: %.f' % float(cutoff)
    if float(cutoff) < 1:
        cutoff = '1'

    if os.path.exists('cutoff.txt'):
        if not os.path.exists(cutoffFile):
            shutil.copy('cutoff.txt', cutoffFile)
    else:
        print "no cutoff file"
        sys.exit(1)


    if os.path.exists('kmers.txt'):
        if not os.path.exists(kmerFile):
            shutil.copy('kmers.txt', kmerFile)
    else:
        print "no kmers file"
        sys.exit(1)


    ############################################
    # correct reads
    ############################################
    correct_options = make_cor_opts(options)
    cutoff          = None
    if not options.model_gc:
        if cutoff is None:
            print " generating cutoff"
            cutoff = get_cutoff()
            if float(cutoff) < 1:
                cutoff = '1'


        if not os.path.exists('bithash.bh'):
            print " creating bithash"
            #cmdH = '%s/build_bithash -k %d -m %s -c %s -o bithash.bh' % (quake_dir, options.k, ctsf, cutoff)
            cmdH = '%s/build_bithash -k %d -m %s -c %s -o bithash.bh' % (quake_bin, options.k, ctsf, cutoff)
            print "   cmd",cmdH

            sys.stderr.flush()
            sys.stdout.flush()

            ph          = subprocess.Popen(cmdH, shell=True)
            pid, status = os.waitpid(ph.pid, 0)
            if status != 0:
                print 'failed to create bithash'
                sys.exit(1)


    pool    = multiprocessing.Pool(5, preexec_function)
    results = []
    for data in readsSplits:
        for listFileName in data:

            if options.quality_scale == -1:
                print "guessing quality scale"
                quality_scale = guess_quality_scale(options.readsf, listFileName)
                print "  quality scale",quality_scale

            else:
                quality_scale = options.quality_scale

            setup = {}
            setup['quality_scale'  ] = quality_scale
            setup['listFileName'   ] = listFileName
            setup['model_gc'       ] = options.model_gc
            setup['readsf'         ] = options.readsf
            setup['quake_dir'      ] = quake_dir
            setup['correct_options'] = correct_options
            setup['ctsf'           ] = ctsf
            setup['cutoff'         ] = cutoff
            setup['data'           ] = data

            results.append( [ listFileName, pool.apply_async(runAnalysis, [setup]) ] )

    pool.close()
    print "processes sent. waiting"
    sys.stderr.flush()
    sys.stdout.flush()

    logDone = open("logdone.log", "w")
    try:
        pcount = 0
        ptotal = len(results)

        for result in results:
            listFileName, procRes = result
            pcount += 1

            while True:
                res = None
                try:
                    print 'waiting for %s (%d/%d)' % ( listFileName, pcount, ptotal )
                    sys.stderr.flush()
                    sys.stdout.flush()

                    res = procRes.get(timeout=5)
                    #print 'res',res
                    resData, resName, resStatus = res
                    if resStatus != 0:
                        print 'list %s returned an error: %d' % ( listFileName, resStatus )
                        logDone.write('list %s returned an error: %d\n' % ( listFileName, resStatus ))
                        logDone.flush()
                        sys.exit(1)
                    else:
                        print '%s finished successfully' % listFileName
                        logDone.write('%s finished successfully\n' % listFileName)
                        logDone.flush()
                        sys.stderr.flush()
                        sys.stdout.flush()

                    break

                except multiprocessing.TimeoutError:
                    time.sleep(10)
                    pass

                except:
                    nfo = sys.exc_info()
                    print "threw another exception", nfo[0], nfo[1]
                    logDone.write("threw another exception ")
                    logDone.write( str(nfo[0]) )
                    logDone.write(" ")
                    logDone.write( str(nfo[1]) )
                    logDone.write("\n")
                    logDone.flush()
                    raise
                    sys.exit(1)

    except KeyboardInterrupt:
        print 'pressed ctrl+c. terminating'
        pool.terminate()
        #print 'waiting'
        #pool.wait()
        print 'bye'
        sys.exit(1)

    print 'joining'
    logDone.write('joining\n')
    logDone.flush()
    sys.stderr.flush()
    sys.stdout.flush()
    pool.join()
    print 'joined'
    sys.stderr.flush()
    sys.stdout.flush()
    print 'checking files'
    logDone.write('checking files\n')
    logDone.flush()

    maxWaitingTime = 5 * 60 # 1 min
    for data in readsSplits:
        for listFileName in data:
            nfo = data[listFileName]
            if nfo is not None:
                #data[newListFileName][fileTmpPath].append( [ outFile, dstFile ] )
                for fileTmpPath in nfo:
                    for outFiles in nfo[fileTmpPath]:
                        tmpFile, dstFile, compulsory = outFiles
                        if compulsory:
                            if not os.path.exists(tmpFile):
                                print "failed to create tmp file %s for %s" % ( tmpFile, dstFile )
                                logDone.write("failed to create tmp file %s for %s\n" % ( tmpFile, dstFile ))
                                logDone.flush()
                                sys.exit(4)

                            initialT = time.time()
                            while not os.path.exists(dstFile):
                                time.sleep(1)
                                timeDiff = time.time() - initialT
                                if timeDiff > maxWaitingTime:
                                    print "waited for too long for %s to %s" % ( tmpFile, dstFile )
                                    logDone.write("waited for too long for %s to %s\n" % ( tmpFile, dstFile ))
                                    logDone.flush()
                                    sys.exit(3)


    #TODO: WAIT FOR FILES

    print '!'*50
    print 'finished analysis'
    print '!'*50
    print 'done'
    print 'bye'
    logDone.write("done\n")
    logDone.close()

def runAnalysis(setup):
    quality_scale    = setup['quality_scale'  ]
    listFileName     = setup['listFileName'   ]
    model_gc         = setup['model_gc'       ]
    readsf           = setup['readsf'         ]
    quake_dir        = setup['quake_dir'      ]
    correct_options  = setup['correct_options']
    ctsf             = setup['ctsf'           ]
    cutoff           = setup['cutoff'         ]
    data             = setup['data'           ]

    print "%s :: correcting" % listFileName
    for key in setup:
        if key == 'data': continue
        print '%s :: correcting :: %-16s: %s' % ( listFileName, key, setup[key] )

    allOk = True
    nfo   = data[listFileName]
    if nfo is not None:
        #data[newListFileName][fileTmpPath].append( [ outFile, dstFile ] )
        for fileTmpPath in nfo:
            for outFiles in nfo[fileTmpPath]:
                tmpFile, dstFile, compulsory = outFiles
                if compulsory:
                    if not os.path.exists(dstFile):
                        allOk = False

                    if os.path.exists(dstFile + '.ok'):
                        allOk = False


    if allOk:
        print '%s :: correcting :: %-16s: %s : !! already corrected. skipping !!' % ( listFileName, key, setup[key] )
        return [data, listFileName, 0]

    else:
        if nfo is not None:
            #data[newListFileName][fileTmpPath].append( [ outFile, dstFile ] )
            for fileTmpPath in nfo:
                for outFiles in nfo[fileTmpPath]:
                    tmpFile, dstFile, compulsory = outFiles
                    if compulsory:
                        if os.path.exists(dstFile):
                            os.remove(dstFile)

                        if os.path.exists(dstFile + '.ok'):
                            os.remove(dstFile + '.ok')



        resp = []
        if model_gc:
            # run correct C++ code
            listFileNameCmd = "-f "
            if readsf:
                listFileNameCmd = "-r "
            listFileNameCmd += listFileName
            #cmd = '%s/correct %s %s -m %s -a cutoffs.gc.txt -q %d' % (quake_dir, correct_options, listFileNameCmd, ctsf, quality_scale)
            cmd = '%s/correct %s %s -m %s -a cutoffs.gc.txt -q %d' % (quake_bin, correct_options, listFileNameCmd, ctsf, quality_scale)

            print "%s :: correcting :: %s" % ( listFileName, cmd )

            p           = subprocess.Popen(cmd, shell=True)
            pid, status = os.waitpid(p.pid, 0)

            print "%s :: correcting :: finished %d" % ( listFileName, status )
            resp = [data, listFileName, status]

        else:
            listFileNameCmd = "-f "
            if readsf:
                listFileNameCmd = "-r "
            listFileNameCmd += listFileName


            # run correct C++ code
            #cmd = '%s/correct %s %s -q %d -b bithash.bh' % (quake_dir, correct_options, listFileNameCmd, quality_scale)
            cmd = '%s/correct %s %s -b bithash.bh -q %d' % (quake_bin, correct_options, listFileNameCmd, quality_scale)

            print "%s :: correcting :: %s" % ( listFileName, cmd )
            p           = subprocess.Popen(cmd, stdout=sys.stdout, stderr=sys.stderr, shell=True)
            pid, status = os.waitpid(p.pid, 0)
            #pid, status = 123, 0

            print "%s :: correcting :: finished %d" % ( listFileName, status )
            resp = [data, listFileName, status]



        data, listFileName, response = resp

        print '%s :: callback   :: response (%d)' % ( listFileName, response )

        if response != 0:
            print "%s :: callback   :: finished with value %d" % (listFileName, response)
            #sys.exit(response)
            return resp


        nfo = data[listFileName]
        if nfo is not None:
            #data[newListFileName][fileTmpPath].append( [ outFile, dstFile ] )
            for fileTmpPath in nfo:
                for outFiles in nfo[fileTmpPath]:
                    tmpFile, dstFile, compulsory = outFiles
                    if compulsory:
                        if not os.path.exists(tmpFile):
                            print "did not create tmp file: %s" % tmpFile
                            print "creating dummy"
                            with open(tmpFile, 'w') as t:
                                t.write('@dummy\n')
                                t.write('N\n')
                                t.write('+dummy\n')
                                t.write('%s\n' % chr(quality_scale))

                    if os.path.exists(tmpFile):
                        print "%s :: callback   :: copying %s to %s" % ( listFileName, tmpFile, dstFile )

                        try:
                            shutil.copy(tmpFile, dstFile)
                            print "%s :: callback   :: copying %s to %s SUCCESSFULL" % ( listFileName, tmpFile, dstFile )

                        except:
                            print "%s :: callback   :: copying %s to %s FAILED" % ( listFileName, tmpFile, dstFile )
                            return [data, listFileName, 333]

                        with open(dstFile + ".ok", "w") as d:
                            d.write("ok")

        return resp


def preexec_function():
    signal.signal(signal.SIGINT, signal.SIG_IGN)

################################################################################
# count_kmers
#
# Count kmers in the reads file using my single-threaded
# programs
################################################################################
def count_kmers(readsf, reads_listf, k, ctsf, quality_scale):
    # find files
    fq_files = []
    if readsf:
        fq_files.append(readsf)
    else:
        for line in open(reads_listf):
            for fqf in line.split():
                fq_files.append(fqf)

    # count zipped fastq files
    fq_zipped = [fqf[-3:] == '.gz' for fqf in fq_files]

    # none zipped
    if sum(fq_zipped) == 0:
        if ctsf[-5:] == '.qcts':
            p = subprocess.Popen('cat %s | %s/count-qmers -k %d -q %d > %s' % (' '.join(fq_files), quake_dir, k, quality_scale, ctsf), shell=True)
        else:
            p = subprocess.Popen('cat %s | %s/count-kmers -k %d > %s' % (' '.join(fq_files), quake_dir, k, ctsf), shell=True)

    # all zipped
    elif sum(fq_zipped) == len(fq_zipped):
        if ctsf[-5:] == '.qcts':
            p = subprocess.Popen('gunzip -c %s | %s/count-qmers -k %d -q %d > %s' % (' '.join(fq_files), quake_dir, k, quality_scale, ctsf), shell=True)
        else:
            p = subprocess.Popen('gunzip -c %s | %s/count-kmers -k %d > %s' % (' '.join(fq_files), quake_dir, k, ctsf), shell=True)

    # mixed- boo
    else:
        print >> sys.stderr, 'Some fastq files are zipped, some are not. Please be consistent.'
        exit(1)

    os.waitpid(p.pid, 0)


################################################################################
# get_cutoff
#
# Read the chosen cutoff from disk and interpret it for the user.
################################################################################
def get_cutoff():
    if not os.path.isfile('cutoff.txt'):
        print >> sys.stderr, 'Optimization of distribution likelihood function to choose k-mer cutoff failed. Very likely you have set the value of k too high or not provided adequate coverage (>15x). Inspect the k-mer counts for a clear separation of the error and true k-mer distributions.'
        exit(1)

    cutoff = open('cutoff.txt').readline().rstrip()

    print 'Cutoff: %s' % cutoff

    if float(cutoff) < 1.0:
        print '(This is a low cutoff value indicating that you have low genome coverage. You may consider also correcting your reads by running the "correct" program directly specifying the cutoff as 1.0)'

    return cutoff


################################################################################
# guess_quality_scale
#
# Guess at ascii scale of quality values by examining
# a bunch of reads and looking for quality values < 64,
# in which case we set it to 33.
################################################################################
def guess_quality_scale(readsf, reads_listf):
    reads_to_check = 1000
    if not readsf:
        readsf = open(reads_listf).readline().split()[0]

    fqf           = open(readsf)
    reads_checked = 0
    header        = fqf.readline()
    while header and reads_checked < reads_to_check:
        seq            = fqf.readline()
        mid            = fqf.readline()
        qual           = fqf.readline().rstrip()
        reads_checked += 1

        for q in qual:
            if ord(q) < 64:
                print 'Guessing quality values are on ascii 33 scale'
                return 33
        header = fqf.readline()

    print 'Guessing quality values are on ascii 64 scale'
    return 64


################################################################################
# jellyfish
#
# Run Jellyfish using integer counts only for now. I take a guess at a good
# hash table size here based on the kmer size chosen.
################################################################################
def jellyfish(readsf, reads_listf, k, ctsf, quality_scale, hash_size, proc):
    # choose parameters
    table_expansion_factor = 20
    if not hash_size:
        # guess at table size
        hash_size = int(table_expansion_factor*math.pow(4,k) / 200)

        # make power of 2
        hash_size = pow(2, ceil(log(hash_size)/log(2)))
    ct_size = 8

    # find files
    fq_files = []
    if readsf:
        output_pre = os.path.splitext(readsf)[0]
        fq_files.append(readsf)
    else:
        output_pre = os.path.splitext(reads_listf)[0]
        for line in open(reads_listf):
            for fqf in line.split():
                fq_files.append(fqf)

    # count zipped fastq files
    fq_zipped = [fqf[-3:] == '.gz' for fqf in fq_files]

    # none zipped
    if sum(fq_zipped) == 0:
        if ctsf[-4:] == 'qcts':
            p = subprocess.Popen('%s/jellyfish count -q --quality-start %d -c %d -o %s.db -m %d -t %d -s %d --both-strands %s' % (jellyfish_dir, quality_scale, ct_size, output_pre, k, proc, hash_size, ' '.join(fq_files)), shell=True)
        else:
            p = subprocess.Popen('%s/jellyfish count -c %d -o %s.db -m %d -t %d -s %d --both-strands %s' % (jellyfish_dir, ct_size, output_pre, k, proc, hash_size, ' '.join(fq_files)), shell=True)

    # all zipped
    elif sum(fq_zipped) == len(fq_zipped):
        if ctsf[-4:] == 'qcts':
            p = subprocess.Popen('gunzip -c %s | %s/jellyfish count -q --quality-start %d -c %d -o %s.db -m %d -t %d -s %d --both-strands /dev/fd/0' % (' '.join(fq_files), jellyfish_dir, quality_scale, ct_size, output_pre, k, proc, hash_size), shell=True)
        else:
            p = subprocess.Popen('gunzip -c %s | %s/jellyfish count -c %d -o %s.db -m %d -t %d -s %d --both-strands /dev/fd/0' % (' '.join(fq_files), jellyfish_dir, ct_size, output_pre, k, proc, hash_size), shell=True)

    # mixed- boo
    else:
        print >> sys.stderr, 'Some fastq files are zipped, some are not. Please be consistent.'
        exit(1)

    os.waitpid(p.pid, 0)

    # merge
    max_db = 0
    while os.path.isfile('%s.db_%d' % (output_pre,max_db)):
        max_db += 1
    if max_db == 1:
        os.rename('%s.db_0' % output_pre, '%s.dbm' % output_pre)
    else:
        if ctsf[-4:] == 'qcts':
            # expand hash table
            hash_size *= max_db

            # merge db
            p = subprocess.Popen('%s/jellyfish qmerge -s %d -m %d -o %s.dbm %s' % (jellyfish_dir, hash_size, k, output_pre, ' '.join(['%s.db_%d' % (output_pre,i) for i in range(max_db)])), shell=True)
            os.waitpid(p.pid, 0)

            # rename file
            os.rename('%s.dbm_0' % output_pre, '%s.dbm' % output_pre)

        else:
            # merge db
            p = subprocess.Popen('%s/jellyfish merge -o %s.dbm %s' % (jellyfish_dir, output_pre, ' '.join(['%s.db_%d' % (output_pre,i) for i in range(max_db)])), shell=True)
            os.waitpid(p.pid, 0)

    # produce actual counts
    if ctsf[-4:] == 'qcts':
        p = subprocess.Popen('%s/jellyfish qdump -c %s.dbm > %s' % (jellyfish_dir, output_pre, ctsf), shell=True)
    else:
        p = subprocess.Popen('%s/jellyfish dump -c %s.dbm > %s' % (jellyfish_dir, output_pre, ctsf), shell=True)
    os.waitpid(p.pid, 0)


################################################################################
# make_cor_opts
#
# Interpret the python command line options to pass along to the correct program
################################################################################
def make_cor_opts(options):
    correct_options = '-k %d -p %d -l %d -t %d' % (options.k, options.proc, options.min_read, options.trim_par)
    if options.out_err:
        correct_options += ' -u'
    if options.log:
        correct_options += ' --log'
    if options.headers:
        correct_options += ' --headers'
    return correct_options

############################################################
# __main__
############################################################
if __name__ == '__main__':
    main()
    #pdb.runcall(main)
