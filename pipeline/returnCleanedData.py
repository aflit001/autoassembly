import os,sys

import atexit
from glob   import glob
from pprint import pprint
import shutil
import multiprocessing
from   multiprocessing import Process, Queue, Pool
import signal
import gzip
import time

import techs
import lib.config  as config
import lib.runners as runners
import lib.hasher  as hasher
runners.config = config
foreal         = True
numThreads     = 10

def init_worker():
    """
    tels the child process to ignore signals so that only the mais
    program can accept a ctrl+c. avoiding zumbi processes
    """
    signal.signal(signal.SIGINT, signal.SIG_IGN)


def compress(src, dst):
    print "%-11s:\n  %s\n  %s\n" % ('compressing', src, dst)
    if foreal:
        dstTmp = dst + '.tmp'

        if os.path.exists(dstTmp): os.remove(dstTmp)

        f_in  = open(     src,    'rb')
        f_out = gzip.open(dstTmp, 'wb', 1)
        f_out.writelines(f_in)
        f_out.close()
        f_in.close()

        try:
            if not os.path.exists(dst):
                shutil.move(dstTmp, dst)
            else:
                print "file %s exists. skipping linking %s" % ( dst, src )
        except:
            raise
    else:
        pass


def copy(src, dst):
    print "%-11s:\n  %s\n  %s\n" % ('copying', src, dst)
    if foreal:
        try:
            if not os.path.exists(dst):
                shutil.copy(src, dst)
            else:
                print "file %s exists. skipping linking %s" % ( dst, src )
        except:
            raise
    else:
        pass


def ln(src, dst):
    print "%-11s:\n  %s\n  %s\n" % ('linking', src, dst)
    if foreal:
        try:
            if not os.path.exists(dst):
                os.symlink(src, dst)
            else:
                print "file %s exists. skipping linking %s" % ( dst, src )
        except:
            raise
    else:
        pass


def touch(src, dst):
    print "%-11s:\n  %s\n" % ('touching', dst)
    if foreal:
        if not os.path.exists(dst):
            with open(dst, 'w') as d:
                d.write('ok')
        else:
            print "file %s exists. skipping linking %s" % ( dst, src )
    else:
        pass


def merge(src, dst):
    runners.rmdup(target=[dst], source=src)


def parsework(towork):
    pool       = Pool(processes=numThreads, initializer=init_worker)
    procstodo  = []
    procs      = []

    for funcName, pairs in towork:
        func = workmapping[funcName]
        for src, dst in pairs:
            if not os.path.exists(src):
                print "Src %s does not exists" % src
                sys.exit(1)

            if os.path.exists(dst):
                print "Dst %s already exists" % dst
                sys.exit(1)

            dstDir = os.path.dirname(dst)
            if not os.path.exists(dstDir):
                print "creating dir   : %s" % dstDir
                if foreal:
                    os.makedirs(dstDir)
                else:
                    pass

            print "appending %-5s: %s\n%s%s\n" % ( funcName, src, " "*17, dst )
            procstodo.append([func, src, dst])

    sys.stdout.flush()
    for func, src, dst in procstodo:
        procs.append( [ src, dst, pool.apply_async( func, [src, dst] ) ] )


    finished = []
    for trio in procs:
        src, dst, proc = trio
        if not proc.ready():
            #print "not ready"
            finished.append(trio)
        else:
            #print "ready"
            if not proc.successful():
                print "not successfull"
                raise proc.get()
                sys.exit(1)


    while len(finished) > 0:
        try:
            finished = []
            for trio in procs:
                src, dst, proc = trio
                if not proc.ready():
                    finished.append(trio)
                else:
                    #print "%s ready"%poolNu
                    proc.wait(1)
                    try:
                        res = proc.get(1)
                    except multiprocessing.TimeoutError:
                        pass
                    except:
                        print "%s ready. got. error" % src
            print "waiting %04d/%04d" % (len(finished), len(procs))
            time.sleep(5)

        except KeyboardInterrupt:
            sys.stderr.write("Ctrl+C pressed main. quitting\n")
            pool.terminate()
            sys.stderr.write("Bye\n")
            sys.exit(1)



    print "  FINISHED. JOINING"
    pool.close()
    pool.join()


    print "  MERGING RESULT"

    if len(procs) > 0:
        #time.sleep(5)
        while len(procs) != 0:
            toDel = []

            for trio in procs:
                src, dst, proc = trio
                if proc.ready():
                    proc.wait(.5)
                    if proc.successful():
                        print " %s FINISHED SUCCESSFULLY"      % ( src )
                        toDel.append(trio)

                    else:
                        print "UNSUCESSFUL RUNNING. %s" % src
                else:
                    print " %s STILL RUNNING" % ( src )

            if len(toDel) > 0:
                for trio in toDel:
                    procs.remove(trio)


def parsegroup(togroup):
    towork = []
    for group in togroup:
        for libDsc in group:
            libName, libPath = libDsc
            files = group[libName]

            src   = files
            dst   = os.path.join(os.path.abspath(libPath), libName)

            towork.append( ['merge', [ src, dst ]] )


def main():
    print "MAIN"
    towork = []

    for dataset in config.allSppsDatasets:
        datasetName = dataset.getName()

        print "      RUNNING DATASET '" + datasetName + "'"
        baseDir = dataset.dstFolder
        ##str(dataset)

        print "        PROJECT",dataset.getName()

        datasetOut = []
        for sample in dataset.getSamples():
            sampleName = sample.getName()
            print "          SAMPLE '" + sampleName + "'"

            techs    = sample.getTechnologies()
            techsOut = []

            for tech in techs:
                techName = tech.name
                if techName in techMapper:
                    print "            TECH '"+tech.getName()+"'"
                    techRes = techMapper[techName](datasetName, tech)
                    towork.extend(  techRes['individual'] )
                    togroup.append( techRes['grouping'  ] )
                else:
                    print 'unknown tech',techName
                    sys.exit(1)

    parsework(  towork  )
    parsegroup( togroup )



def doSpeciesCleaningIllumina(datasetName, tech):
    libs        = tech.getLibraries()
    libsOut     = []
    finalOut    = []
    finalQuake  = []
    outs        = []
    globalPairs = []
    toZip       = []
    toCopy      = []
    toTouch     = []
    toLink      = []
    toMerge     = {}

    for libObj in libs:
        libName                     = libObj.getName()
        libPath                     = libObj.dstFolder
        toMerge[(libName, libPath)] = []
        print '              ILL - LIB ' + libName

        pairs    = libObj.getPairs()
        pairsOut = []
        for pair in pairs:
            pairName  = pair.getName()
            print '                ILL - PAIR ' + pairName + ' TYPE ' + pair.getType()

            runFilesPair        = []
            runFilesSingle      = []
            runRawFilesIn       = []
            runRawFilesOut      = []
            runCleanFilesPair   = []
            runCleanFilesSingle = []
            runs                = pair.getRuns()

            for run in runs:
                #pairFiles.append(run.getAbsolutePath())
                data                = run.data
                runShortName        = run.getShortName()
                runFileName         = run.getFileName()
                runFileNameIn       = run.getAbsolutePath()
                runFileNameOut      = runFileNameIn


                if run.data is not None:
                    if run.data.hasPlugin('compression'):
                        runFileNameOut = run.data.getPlugin('compression').getDecompressedFilePath() #TODO. GET FULL PATH

                runFolders          = data.getDstFolders('name')

                trimmedPair         = runFileNameOut + '.trimmed.paired.fastq'
                trimmedSingle       = runFileNameOut + '.trimmed.single.fastq'
                trimmedPairContam   = trimmedPair    + '.contamCleaned.fastq'
                trimmedSingleContam = trimmedSingle  + '.contamCleaned.fastq'


                if not os.path.exists(trimmedPairContam):
                    print "filtered file %s for %s does not exists" % (trimmedPairContam, runFileNameIn)
                    sys.exit(1)

                if not os.path.exists(trimmedSingleContam):
                    print "filtered file %s for %s does not exists" % (trimmedSingleContam, runFileNameIn)
                    sys.exit(1)



                tocheck = {}
                for status   in ["PREFILTER", "FILTERED"]:
                    newRunFileNamePair     = runFileName
                    newRunFileNameSingle   = runFileName

                    for end in [ '.fastq.gz', '.fq.gz', '.fastq']:
                        if runFileName.endswith(end):
                            newRunFileNamePair   = runFileName.replace(end, '.cleaned.fastq.gz'  )
                            newRunFileNamePair   = os.path.join(os.path.dirname(newRunFileNamePair)  , 'paired_' + os.path.basename(newRunFileNamePair  ))

                            newRunFileNameSingle = runFileName.replace(end, '.cleaned.fastq.gz')
                            newRunFileNameSingle = os.path.join(os.path.dirname(newRunFileNameSingle), 'single_' + os.path.basename(newRunFileNameSingle))


                    toMerge[(libName, libPath)].append(newRunFileNamePair)


                    data.projectStatusName = status
                    data.infile            = newRunFileNamePair
                    runFilePathPair        = data.getFileNamePath()
                    runFilePathPairTmp     = data.getDstFolders('seq.fastq')[5]

                    data.infile            = newRunFileNameSingle
                    runFilePathSingle      = data.getFileNamePath()
                    runFilePathSingleTmp   = data.getDstFolders('seq.fastq')[5]



                    tocheck[status] = []
                    stheader = open(trimmedPairContam).read(6)
                    if stheader != '@dummy':
                        tocheck[status].append( [ trimmedPairContam,   runFilePathPair  ] )

                        if not os.path.exists(runFilePathPairTmp):
                            toLink.append( [trimmedPairContam, runFilePathPairTmp          ])
                            toTouch.append([trimmedPairContam, runFilePathPairTmp + '.ok'  ])
                            toTouch.append([trimmedPairContam, runFilePathPairTmp + '.t.ok'])

                    else:
                        print "dummy"


                    stheader = open(trimmedSingleContam).read(6)
                    if stheader != '@dummy':
                        tocheck[status].append( [trimmedSingleContam, runFilePathSingle ] )

                        if not os.path.exists(runFilePathSingleTmp):
                            toLink.append( [trimmedSingleContam, runFilePathSingleTmp          ])
                            toTouch.append([trimmedSingleContam, runFilePathSingleTmp + '.ok'  ])
                            toTouch.append([trimmedSingleContam, runFilePathSingleTmp + '.t.ok'])

                    else:
                        print "dummy"


                if "FILTERED" not in tocheck:
                    if "PREFILTER" in tocheck:
                        toZip.extend(tocheck["PREFILTER"])

                        print '                  ILL - RUN - NAME        : %s' % runShortName
                        print '                              FN          : %s' % runFileName
                        print '                              PATH IN     : %s' % runFileNameIn
                        print '                              PATH OUT    : %s' % runFileNameOut
                        print '                              TRIM PAIR   : %s' % trimmedPair
                        print '                              TRIM SING   : %s' % trimmedSingle
                        print '                              CONTAM PAIR : %s' % trimmedPairContam
                        print '                              CONTAM SING : %s' % trimmedSingleContam
                        #print '                              DST FOLDER  : %s' % str(runFolders)
                        print '                              DST PAIR TMP: %s' % runFilePathPairTmp
                        for pair in tocheck:
                            print '                              TO ADD      : %s > %s' % (pair[0], pair[1])
                        print


    return  {
                'individual':   [
                                    ['zip'  , toZip  ],
                                    ['ln'   , toLink ],
                                    ['touch', toTouch]
                                ],
                'grouping'  :   [
                                    ['merge', toMerge]
                                ]
            }


def doSpeciesCleaning454(datasetName, tech):
    techName                = tech.getName()
    baseFolder              = config.setup.getProjectRoot(datasetName)
    projPath                = os.path.join(baseFolder, datasetName)
    statusFolderFiltered    = config.setup.getStatusFolder("FILTERED")
    statusFolderPreFiltered = config.setup.getStatusFolder("PREFILTER")
    statusPathFiltered      = os.path.join(projPath, statusFolderFiltered   )
    statusPathPreFiltered   = os.path.join(projPath, statusFolderPreFiltered)
    techPathFiltered        = os.path.join(statusPathFiltered   , techName)
    techPathPreFiltered     = os.path.join(statusPathPreFiltered, techName)

    print "        BASE PATH               : %s" % baseFolder
    print "        PROJECT FOLDER          : %s" % projPath
    print "        FILTERED FOLDER         : %s" % statusFolderFiltered
    print "        PRE FILTERED FOLDER     : %s" % statusFolderPreFiltered
    print "        FILTERED STATUS PATH    : %s" % statusPathFiltered
    print "        PRE FILTERED STATUS PATH: %s" % statusPathPreFiltered
    print "        FILTERED TECH PATH      : %s" % techPathFiltered
    print "        PRE FILTERED TECH PATH  : %s" % techPathPreFiltered

    if baseFolder is None:
        print "no base folder"
        sys.exit(1)

    if not os.path.exists(projPath):
        print "no project folder"
        sys.exit(1)

    if not os.path.exists(statusPathFiltered):
        print 'no status path filtered'
        sys.exit(1)

    if not os.path.exists(statusPathPreFiltered):
        print 'no status path pre filtered'
        sys.exit(1)

    libs               = tech.getLibraries()
    libsOut            = []
    libsOutClean       = []
    libsOutCleanFilter = []
    finalOut           = []
    outs               = []
    towork             = []

    for libObj in libs:
        libName                = libObj.getName()
        print '          454 - LIB ' + libName

        baseDir = libObj.dstFolder
        outSff  = os.path.abspath(os.path.join(baseDir, '..', libName + '.dedup.sff'     ))
        outGoo  = os.path.abspath(os.path.join(baseDir, '..', libName + '.dedup.good.ids'))
        outBad  = os.path.abspath(os.path.join(baseDir, '..', libName + '.dedup.bad.ids' ))
        outOks  = os.path.abspath(os.path.join(baseDir, '..', libName + '.dedup.ok'      ))
        outOk2  = os.path.abspath(os.path.join(baseDir, '..', libName + '.dedup.ok.ok'   ))


        if not os.path.exists(outSff):
            print "could not find sff output: %s" % outSff
            sys.exit(1)

        sffFiltered         = os.path.join(techPathFiltered   , libName, os.path.basename(outSff))
        sffPreFiltered      = os.path.join(techPathPreFiltered, libName, os.path.basename(outSff))

        if os.path.exists(sffFiltered):
            continue

        if os.path.exists(sffPreFiltered):
            continue

        print '            sff          %s' % outSff
        print '            good         %s' % outGoo
        print '            bad          %s' % outBad
        print '            ok           %s' % outOks
        print '            okok         %s' % outOk2
        print '            filtered     %s' % sffFiltered
        print '            pre filtered %s' % sffPreFiltered

        print "            appending %s > %s" % (outSff, sffPreFiltered)
        towork.append([outSff, sffPreFiltered])

    return  {
                'individual':   [
                                    ['copy', towork]
                                ]
            }


def doSpeciesCleaningPacBio(datasetName, tech):
    towork = []
    return  {
                'individual':   [
                                    ['zip', towork]
                                ]
            }


workmapping =   {
                    'zip'  : compress,
                    'copy' : copy,
                    'ln'   : ln,
                    'touch': touch,
                    'merge': merge
                }


techMapper =    {
                    '454'     : doSpeciesCleaning454,
                    'illumina': doSpeciesCleaningIllumina,
                    'pacbio'  : doSpeciesCleaningPacBio,
                }


if __name__ == '__main__': main()
