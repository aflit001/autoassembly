#!/usr/bin/python
import os,sys
import types
sys.path.append(os.path.abspath('lib'))

import dataplugins

class run(object):
    """describes a illumina run"""
    def __init__ (self, *args, **kwargs ):
        data = args[0]
        if type(data) is str:
            filename = data
            if os.path.exists(filename):

                print "INITIALIZING RUN FILE " + filename
                self.fileName     = filename
                self.strand       = self.getStrand()
                self.absPath      = self.getAbsolutePath()
                self.baseName     = self.getBaseName()
                self.shortName    = self.getShortName()
                self.fileSize     = self.getFileSize()
                self.numSeqs      = self.getNumSeqs()
                self.version      = self.getVersion()

                self.modTime      = self.getModTime()
                self.zip          = self.getIsCompressed()
                self.outputs      = {}
                self.data         = None
                self.dsfFolder    = None

            else:
                raise os.error('File '+filename+' does not exists')
        elif isinstance(data, dataplugins.GenData):
            print "            LOADING FROM GENDATA"

            self.fileName     = data.getFileName()
            #(base, outPath, outFileBase, outPathTmp, outPathFull, outFile) = dataplugins.getDstFolders()

            self.strand       = self.getStrand()
            self.absPath      = data.getFileNamePath()
            self.baseName     = os.path.basename(self.fileName)
            self.shortName    = self.getShortName()
            self.fileSize     = data.getPluginResult('info' , 'size' )
            self.modTime      = data.getPluginResult('info' , 'mtime')
            qualsData = data.getPlugin('quals')
            #print qualsData
            self.numSeqs      = qualsData['numSeqs'   ]
            self.version      = qualsData['formatType']

            self.zip          = data.getPluginResult('compression', 'isCompressed')
            self.outputs      = {}
            self.data         = data

            self.dstFolder     = None
            if self.dstFolder is None:
                sampleId = data.getSampleId()
                if sampleId == '':
                    sampleId = 'none'
                    #sampleId = data.getprojectName()
                folderEpitope  = os.path.join('assembly', sampleId, data.getSequenceTech(), data.getSampleLibrary(), data.getPairName(), os.path.basename(data.getFileName()))
                #                             assembly/   illumina/                                   MP_5000/                 3018DAAXX_4/        illumina/MP_5000/3018DAAXX_4_f_fastq
                folderEpitope  = folderEpitope.replace('-','_').replace('.','_').replace('__','_')
                dstFolders     = data.getDstFolders(os.path.join('..', folderEpitope))
                dstFolder      = os.path.abspath(dstFolders[5])
                self.dstFolder = dstFolder

        else:
            print "unknown initializer:",type(args[0])
            sys.exit(0)

    def addOutput(self, program, file):
        self.outputs[program] = file

    def check(self):
        fileExists = False
        sameSize   = False
        sameDate   = False

        fn = self.getFileName()
        if os.path.exists( self.absPath ):
            fileExists = True
            if os.path.getsize( self.absPath )  == self.fileSize:
                sameSize = True
            if os.path.getmtime( self.absPath ) == self.modTime:
                #print "CURR MOD TIME " + str(os.path.getmtime( self.absPath ))
                #print "REC  MOD TIME " + str(self.modTime)
                sameDate = True

        result =    "RUN "+fn+" = EXISTS       : " + str(fileExists) +"\n"+\
                    "RUN "+fn+" = SAME SIZE    : " + str(sameSize)   +"\n"+\
                    "RUN "+fn+" = SAME DATE    : " + str(sameDate)   +"\n"+\
                    "RUN "+fn+" = DST FOLDER   : " + self.dstFolder  +"\n";

        veredict = fileExists and sameSize and sameDate

        return ( veredict , result )

    def export(self):
        #EXPORT SELF TO basename.XML
        #TODO
        pass

    def getNumSeqs(self):
        #TODO
        return 100

    def getVersion(self):
        #TODO
        return '1.3'

    def getStrand(self):
        #TODO
        return 'fwd'

    def getAbsolutePath(self):
        absPath = getattr(self, 'absPath', None)
        if absPath:
            return self.absPath
        else:
            self.absPath = os.path.abspath(self.fileName)
            return self.absPath

    def getBaseName(self):
        baseName = getattr(self, 'baseName', None)
        if baseName:
            return self.baseName
        else:
            self.baseName = os.path.basename(self.getAbsolutePath())
            return self.baseName

    def getShortName(self):
        shortName = getattr(self, 'shortName', None)
        if shortName:
            return self.shortName
        else:
            shortName      = self.getBaseName()
            shortName      = shortName.replace(".fastq.gz", "", 1)
            shortName      = shortName.replace(".fastq",    "", 1)
            shortName      = shortName.replace(".fq",       "", 1)
            self.shortName = shortName
            return self.shortName

    def getFileSize(self):
        fileSize = getattr(self, 'fileSize', None)
        if fileSize:
            if ( fileSize == 0 ):
                raise os.error('File '+fileName+' has size 0')
            return self.fileSize
        else:
            self.fileSize = os.path.getsize(self.fileName)
            return self.fileSize

    def getFileName(self):
        return self.fileName

    def getModTime(self):
        modTime = getattr(self, 'modTime', None)
        if modTime:
            return self.modTime
        else:
            self.modTime = os.path.getmtime(self.absPath)
            return self.modTime

    def getIsCompressed(self):
        zip = getattr(self, 'zip', None)
        if zip != None:
            #print "ZIP ALREADY SETUP. RETRIEVING" + str(zip)
            return self.zip
        else:
            #print "ZIP NOT SETUP. ACQUIRING"
            zip = checkCompression(self.fileName)
            self.zip = zip
            return self.zip

    def __str__(self):
        ver, res = self.check()
        fn = self.getFileName()
        return  "RUN "+fn+"\n"+\
                "RUN "+fn+" = File Name    : " + self.getFileName()          +"\n"+\
                "RUN "+fn+" = Abs Path     : " + self.getAbsolutePath()      +"\n"+\
                "RUN "+fn+" = Base Name    : " + self.getBaseName()          +"\n"+\
                "RUN "+fn+" = Short Name   : " + self.getShortName()         +"\n"+\
                "RUN "+fn+" = File Size    : " + str(self.getFileSize())     +"\n"+\
                "RUN "+fn+" = Modif Time   : " + str(self.getModTime())      +"\n"+\
                "RUN "+fn+" = Strand       : " + self.getStrand()            +"\n"+\
                "RUN "+fn+" = Zip          : " + str(self.getIsCompressed()) +"\n"+\
                "RUN "+fn+" = Num Seqs     : " + str(self.getNumSeqs())      +"\n"+\
                "RUN "+fn+" = Version      : " + str(self.getVersion())      +"\n"+\
                "RUN "+fn+" = Dst Folder   : " + str(self.dstFolder)         +"\n"+\
                "RUN "+fn+" = Check        : " + str(ver)                    +"\n"+\
                res                         +"\n"


class pair(object):
    """describes a illumina pairend/matepair"""

    def __init__ (self, **kwargs):
        self.runs          = kwargs.get('fastqs'    )

        if self.runs[0].data is not None:
            data = self.runs[0].data
            self.insertSize = data.librarySize
            self.type       = data.libraryType

            self.name = data.pairName

            if self.name is None:
                self.name = self.getStem()

            self.dstFolder     = None
            if self.dstFolder is None:
                self.dstFolder = os.path.abspath(os.path.join(self.runs[0].dstFolder, '..'))

        else:
            self.insertSize    = kwargs.get('insertSize')
            self.type          = kwargs.get('type'      ) #wgs/mp/pe

            if kwargs.has_key('name'):
                self.name = kwargs.get('name')
            else:
                self.name = self.getStem()

            self.dstFolder     = kwargs.get('dstFolder')

            #print "INITIALIZING PAIR NAME "+self.name+" WITH " + str(len(self.runs)) + " FILES TYPE " + self.type + " INSERT SIZE " + str(self.insertSize)

        for run in self.runs:
            run.pair = self

        self.numRuns       = self.getNumRuns()
        self.totalFileSize = self.getTotalFileSize()
        self.totalNumSeqs  = self.getTotalNumSeqs()
        self.outputs       = {}

    def addOutput(self, program, file):
        self.outputs[program] = file

    def check(self):
        veredict = True

        for run in self.runs:
            vere, res = run.check()
            veredict  = veredict and vere
            #resl      = ["  RUN "+x for x in res.split("\n")]
            #result   += "\n".join(resl) + "\n"

        status = "PAIR '" + self.getName() + "' = STATUS         : "
        if veredict:
            status += "OK"
        else:
            status += "FAILED"

        result = status + "\n"

        return (veredict, result)

    def export(self):
        #export SELF TO stem.xml
        pass

    def getName(self):
        name = getattr(self, 'name', None)
        if name:
            return self.name
        else:
            return none

    def getType(self):
        return self.type

    def getInsertSize(self):
        return self.insertSize

    def getStem(self):
        names = []
        for run in self.runs:
            names.append(run.getShortName())

        stem = getStem(names)
        if stem:
            return stem
            #print "  FOUND STEM : "+ self.stem
        else:
            #print "  NO STEM FOUND : " + str(pos)
            stem = ""
            for run in self.runs:
                stem += run.getShortName()
            return stem

    def getNumRuns(self):
        return len(self.runs)

    def getTotalFileSize(self):
        totalFileSize = getattr(self, 'totalFileSize', None)
        if totalFileSize:
            return self.totalFileSize
        else:
            totalFileSize = 0
            for run in self.runs:
                totalFileSize += run.getFileSize()
            self.totalFileSize = totalFileSize
            return self.totalFileSize

    def getTotalNumSeqs(self):
        totalNumSeqs = getattr(self, 'totalNumSeqs', None)
        if totalNumSeqs:
            return self.totalNumSeqs
        else:
            totalNumSeqs = 0
            for run in self.runs:
                totalNumSeqs += run.getNumSeqs()
            self.totalNumSeqs = totalNumSeqs
            return self.totalNumSeqs

    def getRuns(self):
        return self.runs

    def __iter__(self):
        return self.runs.__iter__()

    def __str__(self):
        lns = ""

        ver, res = self.check()
        lns +=  "PAIR '" + self.getName() + "' = Name           : "  + self.getName()               +"\n"+\
                "PAIR '" + self.getName() + "' = Type           : "  + self.getType()               +"\n"+\
                "PAIR '" + self.getName() + "' = Insert Size    : "  + str(self.getInsertSize())    +"\n"+\
                "PAIR '" + self.getName() + "' = Num Runs       : "  + str(self.getNumRuns())       +"\n"+\
                "PAIR '" + self.getName() + "' = Total File Size: "  + str(self.getTotalFileSize()) +"\n"+\
                "PAIR '" + self.getName() + "' = Total Num Seqs : "  + str(self.getTotalNumSeqs())  +"\n"+\
                "PAIR '" + self.getName() + "' = Dst Folder     : "  + str(self.dstFolder)          +"\n"+\
                "PAIR '" + self.getName() + "' = Check          : "  + str(ver)                     +"\n"+\
                                       res
        for run in self.runs:
            for ln in str(run).split('\n'):
                lns += "PAIR '" + self.getName() + "' " + ln + "\n"

        return lns


class library(object):
    """describes a illumina library containing illumina unities (PE/MP or singleton)"""

    def __init__(self, **kwargs):
        self.pairs = kwargs.get('pairs')
        self.dstFolder     = None

        if self.pairs[0].runs[0].data is not None:
            data = self.pairs[0].runs[0].data
            self.name = data.sampleLibrary
            if self.dstFolder is None:
                self.dstFolder = os.path.abspath(os.path.join(self.pairs[0].dstFolder, '..'))
        else:
            self.name      = kwargs.get('name'     )
            self.dstFolder = kwargs.get('dstFolder')

        print "INITIALIZING LIBRARY NAME " + self.name + " WITH " + str(len(self.pairs)) + " PAIRS"

        for pair in self.pairs:
            pair.library = self

        self.numPairs      = self.getNumPairs()
        self.totalNumRuns  = self.getTotalNumRuns()
        self.totalFileSize = self.getTotalFileSize()
        self.totalNumSeqs  = self.getTotalNumSeqs()
        self.outputs       = {}

    def addOutput(self, program, file):
        self.outputs[program] = file

    def check(self):
        veredict = True
        result   = ""

        for pair in self.pairs:
            vere, res = pair.check()
            veredict  = veredict and vere
            #resl    = ["  PAIR "+x for x in res.split("\n")]
            #result += "\n".join(resl) + "\n"

        status = "LIB " + self.getName() + " = STATUS         : "
        if veredict:
            status += "OK"
        else:
            status += "FAILED"

        result = status + "\n"

        return (veredict, result)

    def export(self):
        pass

    def getNumPairs(self):
        return len(self.pairs)

    def getTotalNumRuns(self):
        totalNumRuns = getattr(self, 'totalNumRuns', None)
        if totalNumRuns:
            return self.totalNumRuns
        else:
            totalNumRuns = 0
            for pair in self.pairs:
                totalNumRuns += pair.getNumRuns()

            self.totalNumRuns = totalNumRuns
            return self.totalNumRuns

    def getTotalFileSize(self):
        totalFileSize = getattr(self, 'totalFileSize', None)
        if totalFileSize:
            return self.totalFileSize
        else:
            totalFileSize = 0
            for pair in self.pairs:
                totalFileSize += pair.getTotalFileSize()
            self.totalFileSize = totalFileSize
            return self.totalFileSize

    def getTotalNumSeqs(self):
        totalNumSeqs = getattr(self, 'totalNumSeqs', None)
        if totalNumSeqs:
            return self.totalNumSeqs
        else:
            totalNumSeqs = 0
            for pair in self.pairs:
                totalNumSeqs += pair.getTotalNumSeqs()
            self.totalNumSeqs = totalNumSeqs
            return self.totalNumSeqs

    def getName(self):
        return self.name

    def getPairs(self):
        return self.pairs

    def __iter__(self):
        return self.pairs.__iter__()

    def __str__(self):
        lns = ""

        ver, res = self.check()
        lns +=  "LIB " + self.getName() +"\n"+\
                "LIB " + self.getName() + " = Name           : "  + self.getName()               +"\n"+\
                "LIB " + self.getName() + " = Num Pairs      : "  + str(self.getNumPairs())      +"\n"+\
                "LIB " + self.getName() + " = Total Num Runs : "  + str(self.getTotalNumRuns())  +"\n"+\
                "LIB " + self.getName() + " = Total File Size: "  + str(self.getTotalFileSize()) +"\n"+\
                "LIB " + self.getName() + " = Total Num Seqs : "  + str(self.getTotalNumSeqs())  +"\n"+\
                "LIB " + self.getName() + " = Dst Folder     : "  + str(self.dstFolder)          +"\n"+\
                "LIB " + self.getName() + " = Check          : "  + str(ver)                     +"\n"+\
                res

        for pair in self.pairs:
            for ln in str(pair).split('\n'):
                lns += "LIB " + self.getName() + "  " + ln + "\n"

        return lns


class technology(object):
    def __init__(self, **kwargs):
        self.libraries      = None
        self.libraries      = kwargs['libraries']
        self.name           = None

        data = self.libraries[0].pairs[0].runs[0].data
        if data is not None:
            self.name = data.getSequenceTech()
        else:
            self.name = kwargs.get('name')

        if self.name is None:
            print "technology has no name"
            sys.exit(1)

        for lib in self.libraries:
            lib.tech = self

        #self.name          = kwargs['name']
        self.numLibraries  = self.getNumLibraries()
        self.totalNumPairs = self.getTotalNumPairs()
        self.totalNumRuns  = self.getTotalNumRuns()
        self.totalFileSize = self.getTotalFileSize()
        self.totalNumSeqs  = self.getTotalNumSeqs()
        self.outputs       = {}
        self.dstFolder     = None
        if self.dstFolder is None:
            self.dstFolder = os.path.abspath(os.path.join(self.libraries[0].dstFolder, '..'))

        print "INITIALIZING TECHNOLOGY NAME " + self.name + " WITH " + str(self.numLibraries) + " LIBRARIES"


    def addOutput(self, program, file):
        self.outputs[program] = file

    def check(self):
        veredict = True
        result   = ""

        for lib in self.libraries:
           vere, res = lib.check()
           veredict  = veredict and vere
           #resl      = ["SAMPLE '"+self.name+"' = "+x for x in res.split("\n")]
           #result   += "\n".join(resl) + "\n"

        status = "TECH '"+self.name+"' = STATUS         : "
        if veredict:
           status += "OK"
        else:
           status += "FAILED"

        result = status + "\n"

        return (veredict, result)

    def export(self):
        pass

    def getLibraries(self):
        return self.libraries

    def getName(self):
        return self.name

    def getNumLibraries(self):
        return len(self.libraries)

    def getTotalNumPairs(self):
        totalNumPairs = getattr(self, 'totalNumPairs', None)
        if totalNumPairs:
            return self.totalNumPairs
        else:
            totalNumPairs = 0
            for lib in self.libraries:
                totalNumPairs += lib.getNumPairs()
            self.totalNumPairs = totalNumPairs
            return self.totalNumPairs

    def getTotalNumRuns(self):
        totalNumRuns = getattr(self, 'totalNumRuns', None)
        if totalNumRuns:
            return self.totalNumRuns
        else:
            totalNumRuns = 0
            for lib in self.libraries:
                totalNumRuns += lib.getTotalNumRuns()

            self.totalNumRuns = totalNumRuns
            return self.totalNumRuns

    def getTotalFileSize(self):
        totalFileSize = getattr(self, 'totalFileSize', None)
        if totalFileSize:
            return self.totalFileSize
        else:
            totalFileSize = 0
            for lib in self.libraries:
                totalFileSize += lib.getTotalFileSize()
            self.totalFileSize = totalFileSize
            return self.totalFileSize

    def getTotalNumSeqs(self):
        totalNumSeqs = getattr(self, 'totalNumSeqs', None)
        if totalNumSeqs:
            return self.totalNumSeqs
        else:
            totalNumSeqs = 0
            for lib in self.libraries:
                    totalNumSeqs += lib.getTotalNumSeqs()
            self.totalNumSeqs = totalNumSeqs
            return self.totalNumSeqs

    def __iter__(self):
        return self.libraries.__iter__()

    def __str__(self):
        lns = ""

        ver, res = self.check()
        lns =   "TECH '" + self.getName() + "'\n"+\
                "TECH '" + self.getName() + "' = Name           : "  + self.name                    +"\n"+\
                "TECH '" + self.getName() + "' = Num Libs       : "  + str(self.getNumLibraries())  +"\n"+\
                "TECH '" + self.getName() + "' = Total Num Pairs: "  + str(self.getTotalNumPairs()) +"\n"+\
                "TECH '" + self.getName() + "' = Total Num Runs : "  + str(self.getTotalNumRuns())  +"\n"+\
                "TECH '" + self.getName() + "' = Total File Size: "  + str(self.getTotalFileSize()) +"\n"+\
                "TECH '" + self.getName() + "' = Total Num Seqs : "  + str(self.getTotalNumSeqs())  +"\n"+\
                "TECH '" + self.getName() + "' = Dst Folder     : "  + str(self.dstFolder)          +"\n"+\
                "TECH '" + self.getName() + "' = Check          : "  + str(ver)                     +"\n"+\
                res

        for lib in self.libraries:
            for ln in str(lib).split('\n'):
                lns += "TECH '" + self.getName() + "'  " + ln + "\n"

        return lns


class sample(object):
    """describes a illumina dataset containing one or more technologies"""

    def __init__(self, **kwargs):
        self.technologies     = kwargs['technologies']

        data = self.technologies[0].libraries[0].pairs[0].runs[0].data
        if data is not None:
            self.name = data.getSampleId()
            if self.name == '':
                self.name = 'none'
        else:
            self.name = kwargs.get('name')

        for tech in self.technologies:
            tech.technology = self

        #self.name          = kwargs['name']
        self.numTechnologies   = self.getNumTechnologies()
        self.totalNumLibraries = self.getTotalNumLibraries()
        self.totalNumRuns      = self.getTotalNumRuns()
        self.totalNumPairs     = self.getTotalNumPairs()
        self.totalFileSize     = self.getTotalFileSize()
        self.totalNumSeqs      = self.getTotalNumSeqs()
        self.outputs           = {}
        self.dstFolder         = None
        if self.dstFolder is None:
            self.dstFolder = os.path.abspath(os.path.join(self.technologies[0].dstFolder, '..'))

        print "INITIALIZING SAMPLE NAME '" + self.name + "' WITH " + str(self.numTechnologies) + " TECHNOLOGIES"


    def addOutput(self, program, file):
        self.outputs[program] = file

    def check(self):
        veredict = True
        result   = ""

        for tech in self.technologies:
           vere, res = tech.check()
           veredict  = veredict and vere
           #resl      = ["SAMPLE '"+self.name+"' = "+x for x in res.split("\n")]
           #result   += "\n".join(resl) + "\n"

        status = "SAMPLE '"+self.name+"' = STATUS         : "
        if veredict:
           status += "OK"
        else:
           status += "FAILED"

        result = status + "\n"

        return (veredict, result)

    def export(self):
        pass

    def getTechnologies(self):
        return self.technologies

    def getName(self):
        return self.name

    def getNumTechnologies(self):
        return len(self.technologies)

    def getTotalNumLibraries(self):
        totalNumLibraries = getattr(self, 'totalNumLibraries', None)
        if totalNumLibraries:
            return self.totalNumLibraries
        else:
            totalNumLibraries = 0
            for tech in self.technologies:
                totalNumLibraries += tech.getNumLibraries()
            self.totalNumLibraries = totalNumLibraries
            return self.totalNumLibraries

    def getTotalNumPairs(self):
        totalNumPairs = getattr(self, 'totalNumPairs', None)
        if totalNumPairs:
            return self.totalNumPairs
        else:
            totalNumPairs = 0
            for tech in self.technologies:
                totalNumPairs += tech.getTotalNumPairs()
            self.totalNumPairs = totalNumPairs
            return self.totalNumPairs

    def getTotalNumRuns(self):
        totalNumRuns = getattr(self, 'totalNumRuns', None)
        if totalNumRuns:
            return self.totalNumRuns
        else:
            totalNumRuns = 0
            for tech in self.technologies:
                totalNumRuns += tech.getTotalNumRuns()

            self.totalNumRuns = totalNumRuns
            return self.totalNumRuns

    def getTotalFileSize(self):
        totalFileSize = getattr(self, 'totalFileSize', None)
        if totalFileSize:
            return self.totalFileSize
        else:
            totalFileSize = 0
            for tech in self.technologies:
                totalFileSize += tech.getTotalFileSize()
            self.totalFileSize = totalFileSize
            return self.totalFileSize

    def getTotalNumSeqs(self):
        totalNumSeqs = getattr(self, 'totalNumSeqs', None)
        if totalNumSeqs:
            return self.totalNumSeqs
        else:
            totalNumSeqs = 0
            for tech in self.technologies:
                    totalNumSeqs += tech.getTotalNumSeqs()
            self.totalNumSeqs = totalNumSeqs
            return self.totalNumSeqs

    def __iter__(self):
        return self.technologies.__iter__()

    def __str__(self):
        lns = ""

        ver, res = self.check()
        lns =   "SAMPLE '" + self.getName() + "'\n"+\
                "SAMPLE '" + self.getName() + "' = Name           : "  + self.name                        +"\n"+\
                "SAMPLE '" + self.getName() + "' = Num Techs      : "  + str(self.getNumTechnologies())   +"\n"+\
                "SAMPLE '" + self.getName() + "' = Total Num Libs : "  + str(self.getTotalNumLibraries()) +"\n"+\
                "SAMPLE '" + self.getName() + "' = Total Num Pairs: "  + str(self.getTotalNumPairs())     +"\n"+\
                "SAMPLE '" + self.getName() + "' = Total Num Runs : "  + str(self.getTotalNumRuns())      +"\n"+\
                "SAMPLE '" + self.getName() + "' = Total File Size: "  + str(self.getTotalFileSize())     +"\n"+\
                "SAMPLE '" + self.getName() + "' = Total Num Seqs : "  + str(self.getTotalNumSeqs())      +"\n"+\
                "SAMPLE '" + self.getName() + "' = Dst Folder     : "  + str(self.dstFolder)              +"\n"+\
                "SAMPLE '" + self.getName() + "' = Check          : "  + str(ver)                         +"\n"+\
                res

        for tech in self.technologies:
            for ln in str(tech).split('\n'):
                lns += "SAMPLE '" + self.getName() + "'  " + ln + "\n"

        return lns


class project(object):
    """describes a illumina dataset containing one or more samples"""

    def __init__(self, **kwargs):
        self.samples     = kwargs['samples']

        data = self.samples[0].technologies[0].libraries[0].pairs[0].runs[0].data
        if data is not None:
            self.name = data.getprojectName()
        else:
            self.name = kwargs.get('name')

        for sample in self.samples:
            sample.project = self

        #self.name          = kwargs['name']
        self.numSamples           = self.getNumSamples()
        self.totalNumTechnologies = self.getTotalNumTechnologies()
        self.totalNumLibraries    = self.getTotalNumLibraries()
        self.totalNumRuns         = self.getTotalNumRuns()
        self.totalNumPairs        = self.getTotalNumPairs()
        self.totalFileSize        = self.getTotalFileSize()
        self.totalNumSeqs         = self.getTotalNumSeqs()
        self.outputs              = {}
        self.dstFolder            = None
        if self.dstFolder is None:
            self.dstFolder = os.path.abspath(os.path.join(self.samples[0].dstFolder, '..'))

        print "INITIALIZING SAMPLE NAME " + self.name + " WITH " + str(self.numSamples) + " TECHNOLOGIES"


    def addOutput(self, program, file):
        self.outputs[program] = file

    def check(self):
        veredict = True
        result   = ""

        for sample in self.samples:
           vere, res = sample.check()
           veredict  = veredict and vere
           #resl      = ["SAMPLE '"+self.name+"' = "+x for x in res.split("\n")]
           #result   += "\n".join(resl) + "\n"

        status = "PROJECT '"+self.name+"' = STATUS         : "
        if veredict:
           status += "OK"
        else:
           status += "FAILED"

        result = status + "\n"

        return (veredict, result)

    def export(self):
        pass

    def getSamples(self):
        return self.samples

    def getName(self):
        return self.name

    def getNumSamples(self):
        return len(self.samples)

    def getTotalNumTechnologies(self):
        totalNumTechnologies = getattr(self, 'totalNumTechnologies', None)
        if totalNumTechnologies:
            return self.totalNumTechnologies
        else:
            totalNumTechnologies = 0
            for sample in self.samples:
                totalNumTechnologies += sample.getNumTechnologies()
            self.totalNumTechnologies = totalNumTechnologies
            return self.totalNumTechnologies


    def getTotalNumLibraries(self):
        totalNumLibraries = getattr(self, 'totalNumLibraries', None)
        if totalNumLibraries:
            return self.totalNumLibraries
        else:
            totalNumLibraries = 0
            for sample in self.samples:
                totalNumLibraries += sample.getTotalNumLibraries()
            self.totalNumLibraries = totalNumLibraries
            return self.totalNumLibraries

    def getTotalNumPairs(self):
        totalNumPairs = getattr(self, 'totalNumPairs', None)
        if totalNumPairs:
            return self.totalNumPairs
        else:
            totalNumPairs = 0
            for sample in self.samples:
                totalNumPairs += sample.getTotalNumPairs()
            self.totalNumPairs = totalNumPairs
            return self.totalNumPairs

    def getTotalNumRuns(self):
        totalNumRuns = getattr(self, 'totalNumRuns', None)
        if totalNumRuns:
            return self.totalNumRuns
        else:
            totalNumRuns = 0
            for sample in self.samples:
                totalNumRuns += sample.getTotalNumRuns()

            self.totalNumRuns = totalNumRuns
            return self.totalNumRuns

    def getTotalFileSize(self):
        totalFileSize = getattr(self, 'totalFileSize', None)
        if totalFileSize:
            return self.totalFileSize
        else:
            totalFileSize = 0
            for sample in self.samples:
                totalFileSize += sample.getTotalFileSize()
            self.totalFileSize = totalFileSize
            return self.totalFileSize

    def getTotalNumSeqs(self):
        totalNumSeqs = getattr(self, 'totalNumSeqs', None)
        if totalNumSeqs:
            return self.totalNumSeqs
        else:
            totalNumSeqs = 0
            for sample in self.samples:
                    totalNumSeqs += sample.getTotalNumSeqs()
            self.totalNumSeqs = totalNumSeqs
            return self.totalNumSeqs

    def __iter__(self):
        return self.samples.__iter__()

    def __str__(self):
        lns = ""

        ver, res = self.check()
        lns =   "PROJECT '" + self.getName() + "'\n"+\
                "PROJECT '" + self.getName() + "' = Name           : "  + self.name                           +"\n"+\
                "PROJECT '" + self.getName() + "' = Num Samples    : "  + str(self.getNumSamples())           +"\n"+\
                "PROJECT '" + self.getName() + "' = Total Num Techs: "  + str(self.getTotalNumTechnologies()) +"\n"+\
                "PROJECT '" + self.getName() + "' = Total Num Libs : "  + str(self.getTotalNumLibraries())    +"\n"+\
                "PROJECT '" + self.getName() + "' = Total Num Pairs: "  + str(self.getTotalNumPairs())        +"\n"+\
                "PROJECT '" + self.getName() + "' = Total Num Runs : "  + str(self.getTotalNumRuns())         +"\n"+\
                "PROJECT '" + self.getName() + "' = Total File Size: "  + str(self.getTotalFileSize())        +"\n"+\
                "PROJECT '" + self.getName() + "' = Total Num Seqs : "  + str(self.getTotalNumSeqs())         +"\n"+\
                "PROJECT '" + self.getName() + "' = Dst Folder     : "  + str(self.dstFolder)                 +"\n"+\
                "PROJECT '" + self.getName() + "' = Check          : "  + str(ver)                            +"\n"+\
                res

        for sample in self.samples:
            for ln in str(sample).split('\n'):
                lns += "PROJECT '" + self.getName() + "'  " + ln + "\n"

        return lns

