#!/usr/bin/python

# import required modules
import sys

# import custom modules
from library import _getFastaData
from library import _createSequenceDictionary
from library import _getCompressedFastaData
from library import _createCompressedSequenceDictionary
from library import _groupSequences
from library import _createBaseCountDictionary
from library import _countBaseFrequenciesInSequence
from library import _invertDictionaryOfLists



def startAnalysis(fasta_file, max_polymer = 1, seed_length = 0, trim_length = 0):

    # create a compressed sequence dictionary
    sequence_data = _createCompressedSequenceDictionary(fasta_file,
                                                        max_polymer)

    # if sequences should be grouped
    if seed_length:

        # group the sequences
        sequence_groups = _groupSequences(sequence_data,
                                          seed_length,
                                          trim_length)

        # link each sequence in a group to its master sequence
        sequence_masters = _invertDictionaryOfLists(sequence_groups,
                                                    duplicate_values = False)

    # else
    else:

        # there are no groups
        sequence_groups  = None
        sequence_masters = None

    # create the print data
    print_data = _createPrintData(sequence_data,
                                  sequence_groups,
                                  sequence_masters)

    # return the result
    return print_data

# end of startAnalysis


def _createPrintData(sequence_data, sequence_groups, sequence_masters):

    # create an initial print data list
    print_data = ["\t".join(("identifier",
                             "compressed_length",
                             "a_count",
                             "t_count",
                             "c_count",
                             "g_count",
                             "n_count",
                             "duplicate_count",
                             "duplicate_master"))]

    # create a sorted list of sequence ids
    seq_ids = sequence_data.keys()
    seq_ids.sort()

    # loop over the sequences
    for seq_id in seq_ids:

        # create an empty base counts dictionary
        base_counts = _createBaseCountDictionary(alphabet = "ACGT",
                                                 wildcard = "N")

        # count base frequencies for this sequence
        _countBaseFrequenciesInSequence(sequence_data[seq_id],
                                        base_counts,
                                        alphabet = "ACGT",
                                        wildcard = "N")

        # get the number of duplicates and the master sequence id
        try:
            duplicate_master = sequence_masters[seq_id]
            duplicate_count  = len(sequence_groups[duplicate_master])
        except TypeError:
            duplicate_master = seq_id
            duplicate_count  = 0

        # compose a print string
        print_string = "\t".join((seq_id.split(" ")[0],
                                  str(sum(base_counts.values())),
                                  str(base_counts["A"]),
                                  str(base_counts["T"]),
                                  str(base_counts["C"]),
                                  str(base_counts["G"]),
                                  str(base_counts["N"]),
                                  str(duplicate_count),
                                  duplicate_master.split(" ")[0]))

        # add this to the print data
        print_data.append(print_string)

    # convert the print data list to a string
    print_data = "\n".join(print_data)

    # return the result
    return print_data

# end of _createPrintData



# MAIN

# check argument length
if len(sys.argv[1:]) < 1:

    # print an error message and exit
    print "Usage: python analyze454Reads.py <454 reads fasta file or dir> <maximum homopolymer length (default 1)> <seed length for clustering (default 0 = off)> <trimming length for clustering (default 0 = off, 0..1 for percentage based, >1 is absolute)>"
    print "NOTE: output headers will be shortened to the first word only!"
    print "BUG:  setting seed length to 0 does not work!"
    sys.exit()

# set defaults
max_polymer = 1
seed_length = 0
trim_length = 0

# get parameters
fasta_file = sys.argv[1]

# get optional parameters
if len(sys.argv[1:]) > 1:
    max_polymer = int(sys.argv[2])
if len(sys.argv[1:]) > 2:
    seed_length = int(sys.argv[3])
if len(sys.argv[1:]) > 3:
    trim_length = float(sys.argv[4])

# start the analysis
print_data = startAnalysis(fasta_file,
                           max_polymer,
                           seed_length,
                           trim_length)

# print the data
print print_data
