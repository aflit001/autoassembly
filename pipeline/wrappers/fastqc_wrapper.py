#!/usr/bin/env python
"""
FastQC - A high throughput sequence QC analysis tool

SYNOPSIS

	fastqc seqfile1 seqfile2 .. seqfileN

    fastqc [-o output dir] [--(no)extract] [-f fastq|bam|sam]
           [-c contaminant file] seqfile1 .. seqfileN

DESCRIPTION

    FastQC reads a set of sequence files and produces from each one a quality
    control report consisting of a number of different modules, each one of
    which will help to identify a different potential type of problem in your
    data.

    If no files to process are specified on the command line then the program
    will start as an interactive graphical application.  If files are provided
    on the commmand line then the program will run with no user interaction
    required.  In this mode it is suitable for inclusion into a standardised
    analysis pipeline.

    The options for the program as as follows:

    -h --help       Print this help file and exit

    -v --version    Print the version of the program and exit

    -o --outdir     Create all output files in the specified output directory.
                    Please note that this directory must exist as the program
                    will not create it.  If this option is not set then the
                    output file for each sequence file is created in the same
                    directory as the sequence file which was processed.

    --casava        Files come from raw casava output. Files in the same sample
                    group (differing only by the group number) will be analysed
                    as a set rather than individually. Sequences with the filter
                    flag set in the header will be excluded from the analysis.
                    Files must have the same names given to them by casava
                    (including being gzipped and ending with .gz) otherwise they
                    won't be grouped together correctly.

    --extract       If set then the zipped output file will be uncomressed in
                    the same directory after it has been created.  By default
                    this option will be set if fastqc is run in non-interactive
                    mode.

    --noextract     Do not uncompress the output file after creating it.  You
                    should set this option if you do not wish to uncompress
                    the output when running in non-interactive mode.

    --nogroup       Disable grouping of bases for reads >50bp. All reports will
                    show data for every base in the read.  WARNING: Using this
                    option will cause fastqc to crash and burn if you use it on
                    really long reads, and your plots may end up a ridiculous size.
                    You have been warned!

    -f --format     Bypasses the normal sequence file format detection and
                    forces the program to use the specified format.  Valid
                    formats are bam,sam,bam_mapped,sam_mapped and fastq

    -t --threads    Specifies the number of files which can be processed
                    simultaneously.  Each thread will be allocated 250MB of
                    memory so you shouldn't run more threads than your
                    available memory will cope with, and not more than
                    6 threads on a 32 bit machine

    -c              Specifies a non-default file which contains the list of
    --contaminants  contaminants to screen overrepresented sequences against.
                    The file must contain sets of named contaminants in the
                    form name[tab]sequence.  Lines prefixed with a hash will
                    be ignored.

   -q --quiet       Supress all progress messages on stdout and only report errors.
"""


import os, shutil, subprocess, sys, tempfile, glob
import traceback
if __name__=="__main__":
    import os
    fullpath=os.getcwd()

    # add parent folder to path

    #print "CURRENT PATH " + fullpath
    fullpath=os.path.abspath(fullpath)
    #print "PREVIOUS PATH " + fullpath
    sys.path.insert(0, fullpath)
    sys.path.insert(0, os.path.abspath(os.path.join(fullpath, '..')))
    #print "PATH " + str(sys.path)

import tools.parameters
import tools.run
import lib.setup      as setup

setupExe = setup.constants.programs['fastqc']['exe']


def stop_err( msg, errNo=1 ):
    sys.stderr.write( '%s\n' % msg )
    sys.exit(errNo)

def loadParams():
    parameter = tools.parameters.parameters()

    params = {
#        'help'           : { 'optType':'bool',                 'dashes': 2, 'default': False,       'data': { 'help':'[Bool             ] print help     (false)' }},
#        'quiet'          : { 'optType':'bool',                 'dashes': 2, 'default': False,       'data': { 'help':'[Bool             ] not verbose    (false)' }},
        'nogroup'        : { 'optType':'bool',                 'dashes': 2, 'default': False,       'data': { 'help':'[Bool             ] not grouping   (false)' }},
        'extract'        : { 'optType':'bool',                 'dashes': 2, 'default': True,        'data': { 'help':'[Bool             ] extract        (true)'  }},
        'noextract'      : { 'optType':'bool',                 'dashes': 2, 'default': False,       'data': { 'help':'[Bool             ] do not extract (true)'  }},
        'casava'         : { 'optType':'bool',                 'dashes': 2, 'default': False,       'data': { 'help':'[Bool             ] casava format  (false)' }},
        'outdir'         : {                                   'dashes': 2,                         'data': { 'help':'[output folder    ] folder to output'       }},
        'rename'         : {                                   'dashes': 2,                         'data': { 'help':'[rename output    ] output name'            }},
        'prefix'         : {                                   'dashes': 2,                         'data': { 'help':'[prefix output    ] prefix output '         }},
        'format'         : {                                   'dashes': 2,                         'data': { 'help':'[input format     ] bam,sam,bam_mapped,sam_mapped and fastq' }},
        'threads'        : {                    'type':'int',  'dashes': 2, 'default': 4,           'data': { 'help':'[uint32           ] Number of threads (4)'  }},
        'contaminants'   : {                                   'dashes': 2,                         'data': { 'help':'[contaminants file] contanimants file'      }},
    }


    parameter.parseList(params)
    parameter.compile(sys.argv[1:])
    #parameter.parser.print_usage()
    #parameter.parser.print_help()
    #print sys.argv
    #print parameter.parser.values

    validateParams(parameter)

    return parameter


def validateParams(parameter):
    args      = parameter.getArgs()

    if len(args) == 0:
        stop_err("NO INPUT FILES DEFINED", 2)

    basenames = []

    for arg in args:
        files = glob.glob(arg)
        if len(files) == 0:
            stop_err("no globable input file given in '" + arg + "'", 3)

        for file in files:
            basenames.append(os.path.basename(file))
            if ( not os.path.exists(file) ):
                stop_err("file '" + arg + "' does not exists", 4)


    if parameter.hasParam('format'):
        format = parameter.getValue('format')
        if format not in ['bam','sam','bam_mapped','sam_mapped', 'fastq']:
            stop_err('INVALID FORMAT '+format, 5)

    if parameter.hasParam('contaminants'):
        contaminants = parameter.getValue('contaminants')
        if not os.path.exists(contaminants):
            stop_err('CONTAMINANT FILE '+contaminants+' DOES NOT EXISTS', 5)
        if not os.path.isfile(contaminants):
            stop_err('CONTAMINANT FILE '+contaminants+' IS NOT A FILE', 6)

    if parameter.hasParam('outdir'):
        outdir = parameter.getValue('outdir')
        if not os.path.exists(outdir):
            stop_err('OUTPUT DIR '+outdir+' DOES NOT EXISTS', 7)
        if not os.path.isdir(outdir):
            stop_err('OUTPUT DIR '+outdir+' IS NOT A DIRECTORY', 8)

    #if not parameter.hasParam('outdir'):
    #    commomPrefix = os.path.commonprefix(basenames)
    #    commomPrefix = commomPrefix.rstrip("_.-")
    #    print "COMMOM PREFIX " + commomPrefix
    #    if commomPrefix == "":
    #        commomPrefix = "fastqcout"
    #
    #    commomPrefix += '_fastqc'
    #
    #    #parameter.addValue('outdir', 'value', commomPrefix)
    #
    #    print "NEW OUTPUT "+commomPrefix



def localRun(name, id, cmd, errNo):
    try:
        res = tools.run.runString(name + " " + id, cmd)
        if res != 0:
            return 100+res
        return res
    except Exception, e:
        sys.stderr.write(name + " " + id + " :: Exception (Job__launch_out): " + str(e) + "\n")
        sys.stderr.write(name + " " + id + " :: FAILED TO RUN "                + cmd + " EXCEPTION " + str(e) + "\n")
        sys.stderr.write(name + " " + id + " :: TRACEBACK :: "                 + " ".join(traceback.format_stack()) + "\n")
        return 200+errNo



def __main__():
    #Parse Command Line
    parameter = loadParams()
    cmd       = parameter.getCmd()

    #perl $HOME/bin/FastQC/fastqc -t 16 $@
    fastqc = setupExe  + ' ' + cmd + ' ' + " ".join(parameter.getArgs())
    name   = 'FASTQC ' + fastqc
    id     = os.path.commonprefix([os.path.basename(x) for x in parameter.getArgs()])
    if len(id) < 3:
        id = "".join(parameter.getArgs()[0][:10])

    id = id.rstrip('_')
    print "CMD",fastqc,"NAME",name,"ID",id

    #/home/aflit001/filter/Data/AllRound/AllRound_Illumina/AllRound_Illumina_GOG17L2_pairedend_300/110126_SN132_B_s_2_1_seq_GOG-17.fastq
    #/home/aflit001/filter/Data/AllRound/AllRound_Illumina/AllRound_Illumina_GOG17L2_pairedend_300/110126_SN132_B_s_2_1_seq_GOG-17_fastqc.zip
    #/home/aflit001/filter/Data/AllRound/AllRound_Illumina/AllRound_Illumina_GOG17L2_pairedend_300/110126_SN132_B_s_2_1_seq_GOG-17_fastqc:

    #./fastqc_wrapper.py --outdir . ~/filter/Data/AllRound/AllRound_Illumina/AllRound_Illumina_GOG17L2_pairedend_300/110126_SN132_B_s_2_1_seq_GOG-17.fastq
    #110126_SN132_B_s_2_1_seq_GOG-17_fastqc
    #110126_SN132_B_s_2_1_seq_GOG-17_fastqc.zip

    #./fastqc_wrapper.py --outdir . --threads 40 ~/filter/Data/AllRound/AllRound_Illumina/AllRound_Illumina_GOG17L2_pairedend_300/110126_SN132_B_s_2_*_seq_GOG-17.fastq
    #110126_SN132_B_s_2_1_seq_GOG-17_fastqc
    #110126_SN132_B_s_2_1_seq_GOG-17_fastqc.zip
    #110126_SN132_B_s_2_2_seq_GOG-17_fastqc
    #110126_SN132_B_s_2_2_seq_GOG-17_fastqc.zip
    #fastqc_data.txt  fastqc_report.html  Icons  Images  summary.txt


    res = localRun(name, id, fastqc, 13)
    if res != 0:
        stop_err(name + " " + id + " :: ERROR RUNNING. ERROR " + str(res) + " CMD " + cmd, res)

    for file in parameter.getArgs():
        dir,ext  = os.path.splitext(file)
        name     = os.path.basename(dir)
        dir      = os.path.dirname(dir)

        if parameter.hasParam('outdir'):
            dir = parameter.getValue('outdir')

        outDir = os.path.join(dir, name + '_fastqc')
        outZip = outDir + '.zip'

        if not os.path.exists(outDir) or not os.path.isdir(outDir):
            try:
                os.remove(outDir)
                os.remove(outZip)
            except:
                pass
            stop_err(name + " " + id + " :: ERROR CREATING OUTPUT DIR " + outDir,  14)
        else:
            for internalFile in ['fastqc_data.txt', 'fastqc_report.html', 'fastqc_report.html']:
                ifile = os.path.join(outDir, internalFile)
                if not os.path.exists(ifile):
                    try:
                        os.remove(outDir)
                        os.remove(outZip)
                    except:
                        pass
                    stop_err(name + " " + id + " :: ERROR CREATING OUTPUT DIR " + outDir + "INTERNAL FILES. "+ifile+" NOT CREATED",  15)

                elif os.path.getsize(ifile) == 0:
                    try:
                        os.remove(outDir)
                        os.remove(outZip)
                    except:
                        pass
                    stop_err(name + " " + id + " :: ERROR CREATING OUTPUT DIR " + outDir + "INTERNAL FILES. "+ifile+" HAS SIZE 0",  16)


        if not os.path.exists(outZip):
            try:
                os.remove(outDir)
                os.remove(outZip)
            except:
                pass
            stop_err(name + " " + id + " :: ERROR CREATING OUTPUT ZIP " + outZip + ". FILE NOT CREATED",  17)

        elif not os.path.isfile(outZip):
            try:
                os.remove(outDir)
                os.remove(outZip)
            except:
                pass
            stop_err(name + " " + id + " :: ERROR CREATING OUTPUT ZIP " + outZip + ". FILE NOT FILE",  18)

        elif os.path.getsize(outZip) == 0:
            try:
                os.remove(outDir)
                os.remove(outZip)
            except:
                pass
            stop_err(name + " " + id + " :: ERROR CREATING OUTPUT ZIP " + outZip + ". FILE HAS SIZE 0",  19)

        if parameter.hasParam('rename'):
            rename = parameter.getValue('rename')
            print "RENAMING",outDir,"TO",rename
            try:
                os.rename(outDir, rename)
                os.rename(outZip, rename + '.zip')
            except:
                try:
                    os.remove(outDir)
                    os.remove(outZip)
                except:
                    pass
                stop_err(name + " " + id + " :: ERROR RENAMING FILE",  20)

            outDir = rename
            outZip = rename + '.zip'

        if parameter.hasParam('prefix'):
            prefix = parameter.getValue('prefix')
            nameZip  = os.path.basename(outZip)
            nameDir  = os.path.basename(outDir)
            dir      = os.path.dirname( outZip)
            tgtZip   = os.path.join(dir, prefix + nameZip)
            tgtDir   = os.path.join(dir, prefix + nameDir)
            print "ADDING PREFIX",prefix,"FROM",outZip,"TO",tgtZip
            print "ADDING PREFIX",prefix,"FROM",outDir,"TO",tgtDir
            try:
                os.rename(outZip, tgtZip)
                os.rename(outDir, tgtDir)
            except:
                try:
                    os.remove(outDir)
                    os.remove(outZip)
                except:
                    pass
                stop_err(name + " " + id + " :: ERROR RENAMING FILE TO ADD PREFIX",  21)






if __name__=="__main__": __main__()
