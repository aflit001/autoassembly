#!/usr/bin/env python
import os, shutil, subprocess, sys, tempfile, glob
import traceback
import atexit
if __name__=="__main__":
    import os
    fullpath=os.getcwd()

    # add parent folder to path

    #print "CURRENT PATH " + fullpath
    fullpath=os.path.abspath(fullpath)
    #print "PREVIOUS PATH " + fullpath
    sys.path.insert(0, fullpath)
    sys.path.insert(0, os.path.abspath(os.path.join(fullpath, '..')))
    #print "PATH " + str(sys.path)

import lib.parameters as parameters
import lib.run        as run
import lib.setup      as setup

setupExe = setup.constants.programs['fastqCount']['exe']
setupTmp = setup.constants.programs['fastqCount']['tmp']


def stop_err( msg, errNo=1 ):
    sys.stderr.write( '%s\n' % msg )
    sys.stderr.flush()
    sys.exit(errNo)


def localRun(name, id, cmd, errNo):
    try:
        res = run.runString(name + " " + id, cmd)
        if res != 0:
            return 100+res
        return res
    except Exception, e:
        sys.stderr.write(name + " " + id + " :: Exception (Job__launch_out): " + str(e) + "\n")
        sys.stderr.write(name + " " + id + " :: FAILED TO RUN "                + cmd + " EXCEPTION " + str(e) + "\n")
        sys.stderr.write(name + " " + id + " :: TRACEBACK :: "                 + " ".join(traceback.format_stack()) + "\n")
        return 200+errNo



def __main__():
    #Parse Command Line

    #--input %s --output %s
    fileIn        = None
    fileOut       = None
    inStr         = None
    tmp_index_dir = None
    outputTmp     = None


    for p in range(1, len(sys.argv)):
        arg = sys.argv[p]
        if   arg == '--input':
            fileIn  = sys.argv[p+1]
            inStr  += '--input ' + fileIn

        elif arg == '--output':
            fileout = sys.argv[p+1]
            tmp_index_dir, outputTmp = makeTmp(fileOut)
            inStr  += '--output ' + outputTmp

        elif arg == '--kmerlen':
            kmerLen = sys.argv[p+1]
            inStr  += '--kmerlen ' + kmerLen


    if fileIn is None:
        print "no input file defined"
        sys.exit(1)

    if fileOut is None:
        print "no output file defined"
        sys.exit(1)

    for var in [fileIn, fileOut, inStr, tmp_index_dir, outputTmp ]:
        if var is None:
            print "variable is None"
            sys.exit(1)


    atexit.register(cleanTmp, tmp_index_dir)

    cmd   = ' %s' % (setupExe, inStr)

    res = localRun('fastqCount', fileOut, cmd, 1)

    if res != 0:
        stop_err('fastqCount' + " " + fileOut + " :: ERROR RUNNING fastqCount. ERROR " + str(res) + " CMD " + cmd, res)

    if not os.path.exists(outputTmp) or os.path.getsize(outputTmp) == 0:
        stop_err("fastqCount :: ERROR RUNNING fastqCount. ERROR CREATING OUTPUT FILE "       + outputTmp,     3)

    try:
        shutil.move(outputTmp, fileOut)
    except (IOError, os.error), why:
        stop_err(name + " " + mid + " :: ERROR RUNNING " + name + ". ERROR " + str(2) + " NOT ABLE TO MOVE INTERMEDIATE FILE :: " + str(why), 21)

    if not os.path.exists(fileOut) or os.path.getsize(fileOut) == 0:
        stop_err("fastqCount :: ERROR RUNNING fastqCount. ERROR CREATING OUTPUT FILE "       + fileOut,     3)

    outFiles = checkTmp(tmp_index_dir, outputTmp, 'fastqCount', fileOut)

    cleanTmp(tmp_index_dir)


def cleanTmp(tmp_index_dir):
    if os.path.exists( tmp_index_dir ):
        shutil.rmtree( tmp_index_dir )

def checkTmp(index_tmp_dir, outputTmp, name, id):
    outFiles = glob.glob(outputTmp + '*')
    if len(outFiles) == 0:
        if os.path.exists( tmp_index_dir ):
            shutil.rmtree( tmp_index_dir )
        stop_err(name + id + " :: ERROR CREATING OUTPUT TEMPORARY DB " + outputTmp + " NO FILES", 27)

    for file in outFiles:
        if os.path.exists(file):
            size = os.path.getsize(file)
            if ( size == 0 ):
                if os.path.exists( tmp_index_dir ):
                    shutil.rmtree( tmp_index_dir )
                stop_err(name + id + " :: ERROR CREATING OUTPUT TEMPORARY DB " + outputTmp + ". OUTPUT FILE " + file + " IS EMPTY" , 28)
        else:
            if os.path.exists( tmp_index_dir ):
                shutil.rmtree( tmp_index_dir )
            stop_err(name + id + " :: ERROR CREATING OUTPUT TEMPORARY DB " + outputTmp + ". NOT ABLE TO ACCESS " + file , 29)
    return outFiles

def makeTmp(output):
    tmp_index_dir   = tempfile.mkdtemp(prefix='fastqcCount_', dir=setupTmp)
    outputTmp  = output
    outputTmp  = os.path.basename(outputTmp)
    outputTmp  = os.path.join(tmp_index_dir, outputTmp)
    return (tmp_index_dir, outputTmp)



if __name__=="__main__": __main__()



"""
count, stats, histo, dump, merge, query, cite, qhisto, qdump, qmerge, jf



COUNT
Usage: jellyfish count [options] file:path+

Count k-mers or qmers in fasta or fastq files

Options (default value in (), *required):
 -m, --mer-len=uint32                    *Length of mer
 -s, --size=uint64                       *Hash size
 -t, --threads=uint32                     Number of threads (1)
 -o, --output=string                      Output prefix (mer_counts)
 -c, --counter-len=Length in bits         Length of counting field (7)
     --out-counter-len=Length in bytes    Length of counter field in output (4)
 -C, --both-strands                       Count both strand, canonical representation (false)
 -p, --reprobes=uint32                    Maximum number of reprobes (62)
 -r, --raw                                Write raw database (false)
 -q, --quake                              Quake compatibility mode (false)
     --quality-start=uint32               Starting ASCII for quality values (64)
     --min-quality=uint32                 Minimum quality. A base with lesser quality becomes an N (0)
 -L, --lower-count=uint64                 Don't output k-mer with count < lower-count
 -U, --upper-count=uint64                 Don't output k-mer with count > upper-count
     --matrix=Matrix file                 Hash function binary matrix
     --timing=Timing file                 Print timing information
     --stats=Stats file                   Print stats
     --usage                              Usage
 -h, --help                               This message
     --full-help                          Detailed help
 -V, --version                            Version





STATS
Usage: jellyfish stats [options] db:path

Statistics

Display some statistics about the k-mers in the hash:

Unique:    Number of k-mers which occur only once.
Distinct:  Number of k-mers, not counting multiplicity.
Total:     Number of k-mers, including multiplicity.
Max_count: Maximum number of occurrence of a k-mer.

Options (default value in (), *required):
 -L, --lower-count=uint64                 Don't consider k-mer with count < lower-count
 -U, --upper-count=uint64                 Don't consider k-mer with count > upper-count
 -v, --verbose                            Verbose (false)
 -o, --output=c_string                    Output file
     --usage                              Usage
 -h, --help                               This message
     --full-help                          Detailed help
 -V, --version                            Version




HISTO
Usage: jellyfish histo [options] db:path

Create an histogram of k-mer occurrences

Create an histogram with the number of k-mers having a given
count. In bucket 'i' are tallied the k-mers which have a count 'c'
satisfying 'low+i*inc <= c < low+(i+1)*inc'. Buckets in the output are
labeled by the low end point (low+i*inc).

The last bucket in the output behaves as a catchall: it tallies all
k-mers with a count greater or equal to the low end point of this
bucket.

Options (default value in (), *required):
 -l, --low=uint64                         Low count value of histogram (1)
 -h, --high=uint64                        High count value of histogram (10000)
 -i, --increment=uint64                   Increment value for buckets (1)
 -t, --threads=uint32                     Number of threads (1)
 -f, --full                               Full histo. Don't skip count 0. (false)
 -o, --output=c_string                    Output file
 -v, --verbose                            Output information (false)
     --usage                              Usage
     --help                               This message
     --full-help                          Detailed help
 -V, --version                            Version




DUMP
Usage: jellyfish stats [options] db:path

Dump k-mer counts

By default, dump in a fasta format where the header is the count and
the sequence is the sequence of the k-mer. The column format is a 2
column output: k-mer count.

Options (default value in (), *required):
 -c, --column                             Column format (false)
 -t, --tab                                Tab separator (false)
 -L, --lower-count=uint64                 Don't output k-mer with count < lower-count
 -U, --upper-count=uint64                 Don't output k-mer with count > upper-count
 -o, --output=c_string                    Output file
     --usage                              Usage
 -h, --help                               This message
 -V, --version                            Version



MERGE
Usage: jellyfish merge [options] input:c_string+

Merge jellyfish databases

Options (default value in (), *required):
 -s, --buffer-size=Buffer length          Length in bytes of input buffer (10000000)
 -o, --output=string                      Output file (mer_counts_merged.jf)
     --out-counter-len=uint32             Length (in bytes) of counting field in output (4)
     --out-buffer-size=uint64             Size of output buffer per thread (10000000)
 -v, --verbose                            Be verbose (false)
     --usage                              Usage
 -h, --help                               This message
 -V, --version                            Version


"""
