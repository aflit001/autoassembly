#!/usr/bin/env python

import os, shutil, subprocess, sys, tempfile, glob
import traceback
import atexit

if __name__=="__main__":
    import os
    fullpath=os.getcwd()

    # add parent folder to path

    #print "CURRENT PATH " + fullpath
    fullpath=os.path.abspath(fullpath)
    #print "PREVIOUS PATH " + fullpath
    sys.path.insert(0, fullpath)
    sys.path.insert(0, os.path.abspath(os.path.join(fullpath, '..')))
    #print "PATH " + str(sys.path)

import lib.parameters as parameters
import lib.run        as run
import lib.setup      as setup

setupExe = setup.constants.programs['quake']['exe']
setupTmp = setup.constants.programs['quake']['tmp']

def localRun(name, id, cmd, errNo):
    try:
        res = run.runString(name + " " + id, cmd)
        if res != 0:
            return 100+res
        return res
    except Exception, e:
        sys.stderr.write(name + " " + id + " :: Exception (Job__launch_out): " + str(e) + "\n")
        sys.stderr.write(name + " " + id + " :: FAILED TO RUN "                + cmd + " EXCEPTION " + str(e) + "\n")
        sys.stderr.write(name + " " + id + " :: TRACEBACK :: "                 + " ".join(traceback.format_stack()) + "\n")
        return 200+errNo

def cleanTmp(tmp_index_dir):
    if os.path.exists( tmp_index_dir ):
        shutil.rmtree( tmp_index_dir )
        pass

def makeTmp(output):
    #tmp_index_dir = tempfile.mkdtemp(prefix='quake_', dir='/run/shm')
    tmp_index_dir = tempfile.mkdtemp(prefix='quake_', dir=setupTmp)
    #tmp_index_dir = '/run/shm/quake_tmp'
    if not os.path.exists(tmp_index_dir):
        os.makedirs(tmp_index_dir)

    outputTmp     = output
    outputTmp     = os.path.basename(outputTmp)
    outputTmp     = os.path.join(tmp_index_dir, outputTmp)
    return (tmp_index_dir, outputTmp)



if __name__=="__main__":
    if len(sys.argv) < 2:
        print "no params given"
        sys.exit(1)

    dstPath = sys.argv[1]
    if not os.path.exists(dstPath):
        print "dst path %s does not exists" % dstPath
        sys.exit(1)

    #TODO: GET LST FILE AND PARSE IT, REGENERATING IT WITH ONLY 2
    #      FILES AT A TIME IN ORDER TO REDUCE THE DISK FOOTPRINT
    #       - READ LST FILE
    #       - MAKE SYMBOLIC LINK OF THE FILE IN THE TMP FOLDER
    #       - CREATE TMP LST FILE WITH NEW NAMES
    #       - RUN QUAKE
    #       - MOVE FINAL FILES (AND LOGS) TO DESTINY (CHECK QUAKE.PY)
    #         - MERGE LOGS
    #         - RE-USE CUTOFF
    #       - CREATE NEW TMP LST FILE
    #       - REDO
    dstPath = os.path.abspath(dstPath)

    (tmp_index_dir, outputTmp) = makeTmp('none')

    atexit.register(cleanTmp, tmp_index_dir)

    #os.chdir(tmp_index_dir)
    cmd = '%s --tmp %s --dst %s %s' % ( setupExe, tmp_index_dir, dstPath, " ".join(sys.argv[2:]) )
    res = localRun('quake', dstPath, cmd, 1)

    if res != 0:
        print "RESULT NOT ZERO"
        cleanTmp(tmp_index_dir)
        sys.exit(res)

    #TODO: MOVE RESULTS. STILL NEED TO KNOW WHICH FILES ARE THOSE

    sys.exit(res)
