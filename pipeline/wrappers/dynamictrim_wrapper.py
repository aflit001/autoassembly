#!/usr/bin/env python

import os, shutil, subprocess, sys, tempfile, glob
import traceback
import atexit

if __name__=="__main__":
    import os
    fullpath=os.getcwd()

    # add parent folder to path

    #print "CURRENT PATH " + fullpath
    fullpath=os.path.abspath(fullpath)
    #print "PREVIOUS PATH " + fullpath
    sys.path.insert(0, fullpath)
    sys.path.insert(0, os.path.abspath(os.path.join(fullpath, '..')))
    #print "PATH " + str(sys.path)

import lib.parameters as parameters
import lib.run        as run
import lib.setup      as setup

setupExeDytrim = setup.constants.programs['dymTrim']['exeDynamicTrim']
setupExeLenSor = setup.constants.programs['dymTrim']['exeLengthSort' ]
setupTmp       = setup.constants.programs['dymTrim']['tmp']

def localRun(name, id, cmd, errNo):
    try:
        res = run.runString(name + " " + id, cmd)
        if res != 0:
            return 100+res
        return res
    except Exception, e:
        sys.stderr.write(name + " " + id + " :: Exception (Job__launch_out): " + str(e) + "\n")
        sys.stderr.write(name + " " + id + " :: FAILED TO RUN "                + cmd + " EXCEPTION " + str(e) + "\n")
        sys.stderr.write(name + " " + id + " :: TRACEBACK :: "                 + " ".join(traceback.format_stack()) + "\n")
        return 200+errNo


def cleanTmp(tmp_index_dir):
    if os.path.exists( tmp_index_dir ):
        shutil.rmtree( tmp_index_dir )
        #pass


def makeTmp(output):
    #tmp_index_dir = tempfile.mkdtemp(prefix='dytrim_', dir='/run/shm')
    tmp_index_dir = tempfile.mkdtemp(prefix='dytrim_', dir=setupTmp)
    #tmp_index_dir = '/run/shm/trim_tmp'
    outputTmp     = output
    outputTmp     = os.path.basename(outputTmp)
    outputTmp     = os.path.join(tmp_index_dir, outputTmp)
    return (tmp_index_dir, outputTmp)



if __name__=="__main__":
    (tmp_index_dir, outputTmp) = makeTmp('none')

    atexit.register(cleanTmp, tmp_index_dir)

    count   = 0
    params1 = sys.argv[1:]
    params2 = []
    files   = []

    for fileName in params1:
        if fileName.endswith('.fastq'):
            count  += 1
            fnPath  = os.path.abspath(fileName)
            fnName  = fnPath.replace('/', '_').replace('\\', '_').replace('-', '_').replace('__', '_').replace('__', '_')
            fnDst   = os.path.join(tmp_index_dir, fnName)
            fnOut1  = fnDst + '.trimmed'

            if not os.path.exists(fileName):
                print "source file %s does not exists" % fileName
                sys.exit(1)

            if not os.path.exists(fnDst):
                print "linking %s to %s" % ( fileName, fnDst )
                os.symlink(fileName, fnDst)

            if os.path.exists(fnOut1):
                os.remove(fnOut1)

            params2.append(fnOut1)

            files.append( [ fileName, fnDst, fnOut1 ] )


    toCopy  = []
    outBase = params2[0]
    lcount  = 1
    report  = os.path.join(tmp_index_dir, 'res.txt')

    for fileData in files:
        fileName, fileDst, fileOut1 = fileData

        fileFilteredSingle = outBase  + '.single'
        fileFilteredDiscar = outBase  + '.discard'

        fileFinalSingle    = fileName + '.trimmed.single.fastq'
        fileFinalDiscar    = fileName + '.trimmed.discard.fastq'

        fileFinalReport    = fileName + '.trimmed.txt'

        toCopy.append( [ fileFilteredSingle, fileFinalSingle ] )
        toCopy.append( [ fileFilteredDiscar, fileFinalDiscar ] )
        toCopy.append( [ report            , fileFinalReport ] )

        if count > 1:
            fileFilteredPaired  = outBase  + '.paired%s' % lcount
            fileFinalPaired     = fileName + '.trimmed.paired.fastq'
            lcount             += 1
            toCopy.append( [ fileFilteredPaired, fileFinalPaired ] )


    for fileToCopy in toCopy:
        src, dst = fileToCopy
        if os.path.exists(src):
            print 'removing %s' % src
            os.remove(src)


    #(*.single) and reads that are smaller than the threshold (*.discard)
    #(*.paired1 and *.paired2), usable single reads (*.single) and non-usable reads (*.discard)


    if count == 0:
        print "no fastq given: %s" % ( " ".join(sys.argv[1:] ) )
        sys.exit(1)

    minLen = 25

    for fileData in files:
        fileName, fileDst, fileOut1 = fileData
        for index, param in enumerate(params1):
            if param == fileName:
                params1[index] = fileDst


    minLenPar = None
    if '-l' in params1:
        minLenPar = '-l'
    elif '-length' in params1:
        minLenPar = '-length'


    if minLenPar is None:
        print "length parameter not given, using default"
    else:
        pos    = params1.index(minLenPar)
        minLen = params1.pop(pos+1)
        params1.pop(pos)
        if not minLen.isdigit():
            print "length parameter is not a number:",minLen
            sys.exit(1)
        else:
            minLen = int(minLen)



    print "new args: %s" % " ".join(params1)

    exe1   = setupExeDytrim
    exe2   = setupExeLenSor
    cmd1   = '%s -d %s       %s 2>&1 | tee    %s' % ( exe1, tmp_index_dir,          " ".join(params1), report )
    cmd2   = '%s -d %s -l %d %s 2>&1 | tee -a %s' % ( exe2, tmp_index_dir, minLen,  " ".join(params2), report )


    print "cmd 1 %s" % cmd1
    res1   = localRun('dyntrim trim', tmp_index_dir, cmd1, 1)
    if res1 != 0:
        print "RESULT NOT ZERO"
        cleanTmp(tmp_index_dir)
        sys.exit(res1)


    for fileData in files:
        fileName, fileDst, fileOut1 = fileData

        print "for file name %s from %s to %s" % (fileName, fileDst, fileOut1)

        if not os.path.exists(fnOut1):
            print "output %s for %s does not exists" % (fnOut1, fileName)
            sys.exit(1)



    print "cmd 2 %s" % cmd2
    res2   = localRun('dyntrim length', tmp_index_dir, cmd2, 2)
    if res2 != 0:
        print "RESULT NOT ZERO"
        cleanTmp(tmp_index_dir)
        sys.exit(res2)



    for fileToCopy in toCopy:
        src, dst = fileToCopy
        if not os.path.exists(src):
            print "tmp file %s does not exists" % src
            sys.exit(1)



    for fileToCopy in toCopy:
        src, dst = fileToCopy
        print "copying %s to %s" % ( src, dst )
        shutil.copy( src, dst )


    for fileData in files:
        fileName, fileDst, fileOut1 = fileData
        fnOk   = fileName  + '.trimmed.ok'
        with open(fnOk, 'w') as f:
            f.write('ok')

    sys.exit(0)
