#!/usr/bin/python

import sys,os,datetime,time
import tempfile
import subprocess
import shutil
import re
#import zipfile
#http://www.linuxjournal.com/article/9347

#myfile      = zipfile.ZipFile(filein)
#listoffiles = myfile.infolist()
#
#for s in listoffiles:
#    print   s.orig_filename, \
#            s.date_time, \
#            s.filename, \
#            s.file_size, \
#            s.compress_size
#
#    if s.orig_filename in [ 'content.xml']:
#            fd = open(sys.argv[2],'w')
#            bh = myfile.read(s.orig_filename)
#            fd.write(bh)
#            fd.close()

variables = {
    'projectName'                    : "project name is experiment",
    'projectStatusName'              : "project status is experiment",
    'projectType'                    : "project type is denovo",
    'sampleId'                       : "sample id is experiment",
    'sequenceTech'                   : "sequence techology is experimental",
    'librarySize'                    : "library size is 100",
    'libraryType'                    : "library type is 454",
    'infile'                         : "input file is temp",
    'pair_name'                      : "pair name is couple",
    'data'                           : {
        'info'                          : {
                                                'mtime': "creation time",
                                                'size' : "size is big"
                                            },
        'quals'                          : {
                                                'COV'          : "coverage is low",
                                                'Ns'           : "Ns is big",
                                                'ns%'          : "Ns > 100%",
                                                'Q30'          : "Q30 is 40",
                                                'adaptamerSum%': "% of adaptamers id weird",
                                                'numSeqs'      : "number of sequences is ok",
                                                'seqAvgLen'    : "average length is huge",
                                            },
        'contamination'                    : {
                                                'contaminationData': "contamination table",
        }
    }
}


#############
# default values when running standalone
#############
base     = '/home/assembly/tomato150/denovo/arcanum/_tmp/454_20000_HP1TBXB01_sff/'
imagesG  = {
    #fastqc:fastqcGraphs:
    #contamination:contaminationGraph:
    "Per sequence quality scores"  : os.path.join(base, 'seq_fastqc/Images/', 'per_sequence_quality.png'),
    "Sequence Length Distribution" : os.path.join(base, 'seq_fastqc/Images/', 'sequence_length_distribution.png'),
    "Sequence Duplication Levels"  : os.path.join(base, 'seq_fastqc/Images/', 'duplication_levels.png'),
    "Per base N content"           : os.path.join(base, 'seq_fastqc/Images/', 'per_base_n_content.png'),
    "Per base sequence content"    : os.path.join(base, 'seq_fastqc/Images/', 'per_base_sequence_content.png'),
    "Per base sequence quality"    : os.path.join(base, 'seq_fastqc/Images/', 'per_base_quality.png'),
    "Per base GC content"          : os.path.join(base, 'seq_fastqc/Images/', 'per_base_gc_content.png'),
    "Kmer Content"                 : os.path.join(base, 'seq_fastqc/Images/', 'kmer_profiles.png'),
    "Per sequence GC content"      : os.path.join(base, 'seq_fastqc/Images/', 'per_sequence_gc_content.png'),
    "contaminationGraph"           : os.path.join(base, 'seq_screen.png')
}


imageMap = {
    '10000000000003200000025819F3BC90.png': "Per sequence quality scores",  #os.path.join(base, 'seq_fastqc/Images/', 'per_sequence_quality.png'),
    '100000000000032000000258734797CA.png': "Sequence Length Distribution", #os.path.join(base, 'seq_fastqc/Images/', 'sequence_length_distribution.png'),
    '100000000000032000000258AA71FC40.png': "Sequence Duplication Levels",  #os.path.join(base, 'seq_fastqc/Images/', 'duplication_levels.png'),
    '100000000000032000000258B26CDF1A.png': "Per base N content",           #os.path.join(base, 'seq_fastqc/Images/', 'per_base_n_content.png'),
    '100000000000032000000258EA525EDC.png': "Per base sequence content",    #os.path.join(base, 'seq_fastqc/Images/', 'per_base_sequence_content.png'),
    '100000000000032000000258F4EF71C5.png': "Per base sequence quality",    #os.path.join(base, 'seq_fastqc/Images/', 'per_base_quality.png'),
    '1000000000000320000002587F06D941.png': "Per base GC content",          #os.path.join(base, 'seq_fastqc/Images/', 'per_base_gc_content.png'),
    '1000000000000320000002588A7D864A.png': "Kmer Content",                 #os.path.join(base, 'seq_fastqc/Images/', 'kmer_profiles.png'),
    '100000000000032000000258399636BA.png': "Per sequence GC content",      #os.path.join(base, 'seq_fastqc/Images/', 'per_sequence_gc_content.png'),
    '10000000000003840000015ED4FF220C.png': "contaminationGraph",           #os.path.join(base, 'seq_screen.png')
}



def getTime():
    t = time.strftime('%Y/%m/%d - %H:%M %Z', time.localtime(time.time()))
    return t

commands =  {
                'now'  : getTime
            }

def runCommand(cmd):
    if cmd in commands:
        return commands[cmd]()
    else:
        return None



class odfConverter:
    def __init__(self, filein, fileou, variab, images):
        self.filein = filein
        self.fileou = fileou
        self.variab = variab
        self.images = images
        self.main()

    def getVal(self, var):
        pieces    = var.split(':')
        lenPieces = len(pieces)

        if lenPieces == 1:
            if pieces[0] in self.variab:
                return str(self.variab[pieces[0]])
            elif pieces[0] in commands:
                return runCommand(pieces[0])
            else:
                return None
        else:
            if pieces[0] in self.variab:
                var2 = self.variab[pieces[0]]
                if pieces[1] in var2:
                    var3 = var2[pieces[1]]
                    if lenPieces == 2:
                        return str(var3)
                    else:
                        if pieces[2] in var3:
                            var4 = var3[pieces[2]]
                            if lenPieces == 3:
                                return str(var4)
                            else:
                                return None
                        else:
                            return None
                else:
                    return None
            else:
                return None

    def getImage(self, srcImg):
        if srcImg in imageMap:
            dstName = imageMap[srcImg]
            if dstName in self.images:
                dstFile = self.images[dstName]
                return dstFile
            else:
                return False
        else:
            return None

    def main(self):
        if os.path.exists(self.fileou):
            os.remove(self.fileou)
        fileou = os.path.abspath(self.fileou)

        tmpFolder = tempfile.mkdtemp()
        print "TMP     ",tmpFolder
        print "IN  FILE",self.filein
        print "OUT FILE",fileou

        mre   = re.compile('(\{[0-9a-zA-Z\_\:\%]+\})')

        if not os.path.exists(self.filein):
            print "INPUT FILE %s DOES NOT EXISTS" % self.filein

        res = subprocess.call('unzip %s -d %s' % (self.filein, tmpFolder), shell=True, stdout=open(os.devnull, 'wb'))
        if res != 0:
            print "error running unzip. %d" % res
            if os.path.exists(fileou):
                os.remove(fileou)
            shutil.rmtree(tmpFolder)
            sys.exit(1)

        for xmlfile in ['content.xml', 'styles.xml']:
            content = os.path.join(tmpFolder, xmlfile)
            if not os.path.exists(content):
                print "NO CONTENT FILE %s" % content
                if os.path.exists(fileou):
                    os.remove(fileou)
                shutil.rmtree(tmpFolder)
                sys.exit(1)

            data = []
            with open(content, 'r') as cont:
                data = cont.readlines()

            #print data
            pieces = []
            for line in data:
                m = mre.finditer(line)
                if m is None:
                    print "ERROR ON RE"
                    sys.exit(1)

                for item in m:
                    var = item.group(0)
                    pieces.append(var[1:-1])
                    #print "  FOUND FIELD %s"%var

            piecesRes = {}
            for piece in pieces:
                pres = self.getVal(piece)
                if pres is None:
                    print "  COULD NOT MAP FIELD {%s}"%piece
                    pieces.remove(piece)
                else:
                    piecesRes[piece] = pres

            for piece in piecesRes.keys():
                pieceRes = piecesRes[piece]
                #print "    REPLACING {%s} BY <%s>" % (piece, pieceRes)

                for linePos in range(0, len(data)):
                    line             = data[linePos]
                    lineNew, reCount = re.subn('{%s}'%piece, pieceRes, line)
                    data[linePos]    = lineNew
                    #print "      LINE %d REPLACED %d TIMES" % (linePos,reCount)

            with open(content, 'w') as cont:
                cont.writelines(data)

        imgDir = os.path.join(tmpFolder, 'Pictures')
        for srcFile in os.listdir(imgDir):
            imgName = os.path.join(imgDir, srcFile)
            dstFile = self.getImage(srcFile)

            if dstFile is not None:
                os.remove(imgName)
                if dstFile is not False:
                    #print "  REPLACING %s BY %s" % (srcFile, dstFile)
                    if os.path.exists(dstFile):
                        shutil.copy(dstFile, imgName)
                    else:
                        #print "  DELETING %s. INPUT FILE %s DOES NOT EXISTS" % (srcFile, dstFile)
                        pass
                else:
                    #print "  DELETING %s" % (srcFile)
                    pass

        fileout = fileou
        if fileou.endswith('.pdf'):
            fileout = fileou + '.odt'

        if os.path.exists(fileout):
                os.remove(fileout)

        currPwd = os.getcwd()
        os.chdir(tmpFolder)

        print "  SAVING %s" % fileout
        res = subprocess.call('zip -0 -X %s mimetype' % (fileout), shell=True, stdout=open(os.devnull, 'wb'))
        if res != 0:
            print "error running zip. %d" % res
            if os.path.exists(fileout):
                os.remove(fileout)
            shutil.rmtree(tmpFolder)
            sys.exit(1)

        res = subprocess.call('zip -ur %s * -x mimetype' % (fileout), shell=True, stdout=open(os.devnull, 'wb'))
        if res != 0:
            print "error running zip. %d" % res
            if os.path.exists(fileout):
                os.remove(fileout)
            shutil.rmtree(tmpFolder)
            sys.exit(1)
        else:
            print "success compressing to odf"


        if not os.path.exists(fileout):
            print "error creating odf. no file %s" % fileout
            if os.path.exists(fileout):
                os.remove(fileout)
            shutil.rmtree(tmpFolder)
            sys.exit(1)
        else:
            print "success creating odf %s (%d)" % (fileout, os.path.getsize(fileout))


        maxRetries = 3
        if fileou.endswith('.pdf'):
            retries = 0
            success = False
            while retries < maxRetries:
                res     = subprocess.call('unoconv -f pdf -o %s.pdf %s 2>&1' % (fileout, fileout), shell=True) #, stdout=open(os.devnull, 'wb')
                if res != 0:
                    print "error converting to pdf. %d (%d)" % (res, retries)
                    #if os.path.exists(fileout):
                    #    os.remove(fileout)
                    if os.path.exists(fileout):
                        os.remove(fileout)
                    if os.path.exists(fileout + '.pdf'):
                        os.remove(fileout + '.pdf')
                    #shutil.rmtree(tmpFolder)
                    #sys.exit(1)
                    retries += 1
                else:
                    success = True
                    break

            if not success:
                print "error converting to pdf. giving up"
                if os.path.exists(fileout):
                    os.remove(fileout)
                if os.path.exists(fileout + '.pdf'):
                    os.remove(fileout + '.pdf')
                #if os.path.exists(fileou):
                    #os.remove(fileou)
                shutil.rmtree(tmpFolder)
                sys.exit(1)

            if not os.path.exists(fileout + '.pdf'):
                print "error creating pdf. no file %s" % (fileout + '.pdf')
                if os.path.exists(fileout):
                    os.remove(fileout)
                if os.path.exists(fileout + '.pdf'):
                    os.remove(fileout + '.pdf')
                #if os.path.exists(fileou):
                    #os.remove(fileou)
                shutil.rmtree(tmpFolder)
                sys.exit(1)

            res = subprocess.call('pdfopt %s.pdf %s' % (fileout, fileou), shell=True, stdout=open(os.devnull, 'wb'))
            if res != 0:
                print "error optimizing pdf. %d" % res
                if os.path.exists(fileout):
                    os.remove(fileout)
                if os.path.exists(fileout + '.pdf'):
                    os.remove(fileout + '.pdf')
                #if os.path.exists(fileou):
                    #os.remove(fileou)
                shutil.rmtree(tmpFolder)
                sys.exit(1)

            if not os.path.exists(fileou):
                print "error creating pdf. no file %s" % fileou
                if os.path.exists(fileout):
                    os.remove(fileout)
                if os.path.exists(fileout + '.pdf'):
                    os.remove(fileout + '.pdf')
                #if os.path.exists(fileou):
                    #os.remove(fileou)
                shutil.rmtree(tmpFolder)
                sys.exit(1)

            if os.path.exists(fileout + '.pdf'):
                os.remove(fileout + '.pdf')


            #if os.path.exists(fileout):
            #    os.remove(fileout)


        os.chdir(currPwd)
        shutil.rmtree(tmpFolder)
        print "FINISHED"

if __name__ == '__main__':
    if len(sys.argv) < 3:
        print "Usage: %s INFILE OUTFILE" % sys.argv[0]
        sys.exit(0)

    filein = sys.argv[1]
    fileou = sys.argv[2]
    odfConverter(filein, fileou, variables, imagesG)
