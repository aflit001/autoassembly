#!/usr/bin/python
#variables describing the assembly type
DENOVO          = 0
MAPPING         = 1
types           = ['DENOVO', 'MAPPING']

fqFormats = [   #first val  minBorder   max     name                    subtracts
                [33,        59,          74,    "Sanger/Illumina 1.8",  33], #0
                [59,        64,         104,    "Solexa/Illumina 1.0",  64], #1
                [64,        66,         104,    "Illumina 1.3+"      ,  64], #2
                [66,        66,         104,    "Illumina 1.5+"      ,  64], #3
            ]

newblerBorder = 0
ADAPTAMERS = {
    'FLX_LINKER'   : "GTTGGAACCGAAAGGGTTTGAATTCAAACCCTTTCGGTTCCAAC",
    'XLR_LINKER_F' : "TCGTATAACTTCGTATAATGTATGCTATACGAAGTTATTACG",
    'XLR_LINKER_R' : "CGTAATAACTTCGTATAGCATACATTATACGAAGTTATACGA"
}

dbFile      = 'db/filesDB'  # db file prefix to be added .yaml and/or .pickle containing the files information
dumpStruct  = 'db/structDB' # db file prefix to be added .yaml and/or .pickle containing the structure and files information
dumpSetup   = 'db/setupDB'  # db file prefix to be added .yaml and/or .pickle containing the setup of the project

#header of description file
dscHeaders  =   [
                    ['FILE_NAME',      's'],
                    ['PROJECT_NAME',   's'],
                    ['PROJECT_TYPE',   's'],
                    ['SAMPLE_NAME',    's'],
                    ['TECHNOLOGY',     's'],
                    ['LIBRARY_NAME',   's'],
                    ['LIBRARY_TYPE',   's'],
                    ['LIBRARY_SIZE',   's'],
                    ['STATUS_NAME',    's'],
                    ['PAIR_NAME',      's'],
                ]

const_pv           = ' | pv --buffer-size 16M -q '
#const_pv           = ''
const_base         = '/home/assembly/tomato150'
const_tmpl         = '/home/assembly/tmp'
const_tmpr         = '/mnt/nexenta/assembly/nobackup/tmp'
const_shm          = '/run/shm'
const_newbler      = '/opt/454/2.6_1'
const_zip_exe      = 'gzip'
const_zip_parallel = False
#const_zip_exe      = 'pigz'
#const_zip_parallel = True


#TODO: 
#  fastq_screen in path
#  fastq_to_fasta in path
#  fastqCount in path
#  newbler in path
#  pv installed

programs =  {
                'fastqc'                  : {
                    'exe'                 : 'perl %s/scripts/pipeline/progs/FastQC/fastqc' % const_base,
                    'threads'             : 4
                },

                'fastqScreen'             :{
                    'exe'                 : 'fastq_screen',
                    'threads'             : 8,
                    'subset'              : 5000
                },

                'solexaqa'                : {
                    'exe'                 : 'perl %s/scripts/pipeline/progs/solexaqa/SolexaQA.pl' % const_base
                },

                'quake'                   : {
                    'exe'                 : 'python %s/scripts/pipeline/progs/Quake/bin/quake.py' % const_base,
                    'tmp'                 : '%s' % const_tmpl
                    #'tmp'                 : '/mnt/nexenta/assembly/nobackup/tmp'
                },

                'mkplot'                  : {
                    'q'                   : 80,
                    'miY'                 : 3
                },

                'jellyfish'               : {
                    'exe'                 : 'jellyfish',
                    'tmp'                 : '%s' % const_shm,
                    'pv'                  : const_pv
                },

                'filter454'               : {
                    'exeAnalyze'          : 'python %s/scripts/pipeline/progs/filter454/analyze454Reads.py' % const_home,
                    'exeFilter'           : 'python %s/scripts/pipeline/progs/filter454/filter454Reads.py'  % const_home,
                    'exeFq2Fa'            : '%s/scripts/pipeline/progs/fastq_to_fasta' % const_home,
                    'exeFq2Fa'            : '/home/assembly/bin/fastq_to_fasta',
                    'exeSffFile'          : '%s/bin/sfffile' % const_newbler,
                    'exeSffInfo'          : '%s/bin/sffinfo' % const_newbler,
                    'tmp'                 : '%s'             % const_shm,
                },


                'sffExtract'              : {
                    'exeSffFile'          : '%s/bin/sfffile' % const_newbler,
                    'exeSffInfo'          : '%s/bin/sffinfo' % const_newbler,
                    'exeFastaAndQualMerge': 'python %s/scripts/pipeline/progs/fastqmergefastaandqual.py' % const_home,
                },

                'fastqCount'              : {
                    #'exe'                 : '/home/assembly/tomato150/scripts/pipeline/progs/fastqCount',
                    'exe'                 : '/home/aflit001/bin/fastqCount',
                    'tmp'                 : '%s' % const_shm
                },

                'dymTrim'                 : {
                    'exeDynamicTrim'      : 'perl %s/scripts/pipeline/progs/solexaqa/DynamicTrim.pl' % const_home,
                    'exeLengthSort'       : 'perl %s/scripts/pipeline/progs/solexaqa/LengthSort.pl'  % const_home,
                    'tmp'                 : '%s' % const_tmpr
                },

                'zip'                     : {
                    'exe'                 : const_zip_exe
                }
            }


#how to identify if a file is compressed and commands to uncompress them

pigzd  = [programs['zip']['exe'] + ' -d -k -c %(in)s ' + pv + ' > %(out)s']
pigzt  = [programs['zip']['exe'] + ' -t %(in)s'                           ]
if const_zip_parallel:
	pigzd  = [programs['zip']['exe'] + ' -d -p 3 -k -c %(in)s ' + pv + ' > %(out)s']

#sffExt = ['sff_extract --fastq --min_left_clip=16 --out_basename=%(out)s %(in)s']
sffExt = [
            programs['sffExtract']['exeSffInfo'          ] + ' -s %(in)s ' + pv + ' > %(out)s.fasta',
            programs['sffExtract']['exeSffInfo'          ] + ' -q %(in)s ' + pv + ' > %(out)s.fasta.qual',
            programs['sffExtract']['exeFastaAndQualMerge'] + ' %(out)s.fasta %(out)s.fasta.qual %(out)s',
            'rm -f %(out)s.fasta %(out)s.fasta.qual'
        ]

extensions = [
                #extension      compression rate    uncompression   compression test    technology name compressed
                ['*.fastq.gz'  , 2.70,              pigzd ,         pigzt,              'illumina',     True ],
                ['*.fq.gz'     , 2.70,              pigzd ,         pigzt,              'illumina',     True ],
                ['*.fq.gz.*.gz', 2.70,              pigzd ,         pigzt,              'illumina',     True ],
                ['*.fastq'     , 1.00,              None,           None ,              'illumina',     False],
                ['*.fq'        , 1.00,              None,           None ,              'illumina',     False],
                ['*.sff'       , 0.33,              sffExt,         None ,              '454'     ,     True ],
            ]
