import run
import os,sys
import shutil

if __name__=="__main__":
    import os
    fullpath=os.getcwd()

    # add parent folder to path

    #print "CURRENT PATH " + fullpath
    fullpath=os.path.abspath(fullpath)
    #print "PREVIOUS PATH " + fullpath
    sys.path.insert(0, fullpath)
    #print "PATH " + str(sys.path)
import run


def checkFile(file):
    print "    CHECKING '",file,

    lres = 0
    if ( not os.path.exists(file) ):
        lres += 1
        print "' :: EXISTS False",
    else:
        print "' :: EXISTS True",

    size      = os.path.getsize(file)
    print "SIZE",size,

    if ( size != 0 and size == 0 ):
        lres += 2
        print "VALID SIZE False",
    else:
        print "VALID SIZE True",

    print "RES",str(lres)

    return lres


def checkFiles(files):
    lres = 0
    for file in files:
        lres += checkFile(file)
    return lres


def touch(fname, times = None):
    with file(fname, 'a'):
        os.utime(fname, times)




def fastq2jf(target=None, source=None, env=None):
    sources    = map(lambda x: str(x), source)
    targets    = map(lambda x: str(x), target)

    print '  FQ2JF '+" ".join(sources)+' > '+" ".join(targets)

    resin = checkFiles(sources)
    if resin:  return 100 + resin

    #for fileName in targets:
        #touch(fileName)

    #jellyfish count --mer-len=$JELLYMERLEN --threads=$JELLYTHREADS --counter-len=$JELLYCOUNTERLEN --size=$JELLYHASHSIZE --both-strands -o $OUTF/"$fastqBase"_mer_counts $IN"
    #cmd  = 'jellyfish count --mer-len=%(mer-len)d --threads=%(threads)d --counter-len=%(counter-len)d --size=%(size)d %(extra)s"' % config.jellyfishParams
    #cmd += '-o %s_mer_counts %s' % (targets[0], " ".join(sources))
    #-o $OUTF/"$fastqBase"_mer_counts $IN

    infile   = sources[0]
    stheader = open(infile).read(6)
    if stheader == '@dummy':
        for target in targets:
            with open(target, 'w') as t:
                t.write('dummy')
        return 0

    cmd  = 'python wrappers/jellyfish_wrapper.py --program all'
    for key in config.jellyfishParams:
        #--mer-len=%(mer-len)d --threads=%(threads)d --counter-len=%(counter-len)d --size=%(size)d %(_extra)s
        val = str(config.jellyfishParams[key])
        if key[0] == '_':
            cmd += ' ' + val
        else:
            cmd += ' --' + key + ' ' + val

    cmd += ' --output %s %s' % (targets[0], " ".join(sources))

    res    = run.runString('fastq2jf' , cmd)

    if res != 0 :
        return 150 + res

    resout = checkFiles(targets)

    if resout: return 150 + resout

    return 0



def rmdup(target=None, source=None, env=None):
    sources = map(lambda x: str(x), source)
    targets = map(lambda x: str(x), target)

    bn      = targets[ -1]
    targets = targets[:-1]

    print '  RMDUP'+" ".join(sources)+' > '+" ".join(targets)

    resin = checkFiles(sources)
    if resin:  return 100 + resin

    #for file in targets:
    #    touch(file)

    for infile in sources:
        stheader = open(infile).read(6)
        if stheader == 'dummy':
            sources.remove(infile)

    if len(sources) == 0:
        for target in targets:
            with open(target, 'w') as t:
                t.write('dummy')
        return 0



    #usage: filter_dups.py [-h] [-n [N]] [-d [D]] [-m [M]] [-s] [-c] [-i] [-o] [-f]
    #                      [-z]
    #                      i [i ...]
    #filter fastq files
    #positional arguments:
    #  i                     input files
    #optional arguments:
    #  -h, --help            show this help message and exit
    #  -n [N], --nsize [N]   number of bp to use [INT: default: -1 (all)]
    #  -d [D], --dir [D]     output dir [STR: default: .]
    #  -m [M], --merge [M]   merge output using prefix
    #  -s, --single          treat as single end
    #  -c, --compress        compress in memory using md5
    #  -i, --only-ids        only export id, no sequence
    #  -o, --homo, --homopolymer
    #                        compress homopolymers
    #  -f, --dry-run         dry run
    #  -z, --gzip            compress output to gzip
    fullPath = os.path.abspath( sources[0] )
    path     = os.path.dirname( fullPath   )
    bn       = os.path.basename(fullPath   )

    paramStr = ""
    params   = config.rmdupParams
    if 'compress' in params and params['compress']            : paramStr += " --compress"
    if 'homo'     in params and params['homo'    ]            : paramStr += " --homo"
    if 'zip'      in params and params['zip'     ]            : paramStr += " --gzip"
    if 'nsize'    in params and params['nsize'   ] is not None: paramStr += " --nsize %d" % params['nsize']

    cmd      = 'python wrappers/filter_dups.py --dir %s --merge %s %s %s' % ( path, bn, paramStr, " ".join(sources) )



    res      = run.runString('rmdup' , cmd)



    if res != 0 :
        return 150 + res

    resout = checkFiles(targets)

    if resout: return 150 + resout

    ##run.runString('joinjf' , 'echo '+" ".join(sources)+' > '+" ".join(targets))

    return 0



def joinjf(target=None, source=None, env=None):
    sources = map(lambda x: str(x), source)
    targets = map(lambda x: str(x), target)

    print '  JOINJF '+" ".join(sources)+' > '+" ".join(targets)

    resin = checkFiles(sources)
    if resin:  return 100 + resin

    #for file in targets:
    #    touch(file)

    for infile in sources:
        stheader = open(infile).read(6)
        if stheader == 'dummy':
            sources.remove(infile)

    if len(sources) == 0:
        for target in targets:
            with open(target, 'w') as t:
                t.write('dummy')
        return 0

    cmd  = 'python wrappers/jellyfish_wrapper.py --program merge'
    for key in config.jellyfishParams:
        #--mer-len=%(mer-len)d --threads=%(threads)d --counter-len=%(counter-len)d --size=%(size)d %(_extra)s
        val = str(config.jellyfishParams[key])
        if key[0] == '_':
            cmd += ' ' + val
        else:
            cmd += ' --' + key + ' ' + val

    cmd    += ' --output %s --stats %s --histo %s %s' % (targets[0], targets[1], targets[2], " ".join(sources))

    res     = run.runString('joinjf' , cmd)

    if res != 0 :
        return 150 + res

    resout = checkFiles(targets)

    if resout: return 150 + resout

    ##run.runString('joinjf' , 'echo '+" ".join(sources)+' > '+" ".join(targets))

    return 0


def makeNfo(target=None, source=None, env=None):
    sources = map(lambda x: str(x), source)
    targets = map(lambda x: str(x), target)

    print '  MKNFO '+" ".join(sources)+' > '+" ".join(targets)

    resin = checkFiles(sources)
    if resin:  return 100 + resin

    #for file in targets:
    #    touch(file)

    if 'mer-len' in config.jellyfishParams:
        merlen = config.jellyfishParams[ 'mer-len' ]

        if not os.path.exists(targets[0]) or os.path.getsize(targets[0]) == 0:
            cmd    = '/home/aflit001/bin/fastqCount --input %s --output %s --kmerlen %d' % (sources[0], targets[0], merlen)

            res    = run.runString('makenfo' , cmd)

            if res != 0 :
                return 150 + res

        resout = checkFiles(targets)

        if resout: return 150 + resout
    else:
        print '  MKNFO - NO SETUP.JELLYFISHPARAMTERS.MER-LEN DEFINED'
        return 1

    return 0


def histo2png(target=None, source=None, env=None):
    sources = map(lambda x: str(x), source)
    targets = map(lambda x: str(x), target)

    print '  HISTO2PNG '+" ".join(sources)+' > '+" ".join(targets)

    resin = checkFiles(sources)
    if resin:  return 100 + resin


    for infile in sources:
        stheader = open(infile).read(6)
        if stheader == 'dummy':
            sources.remove(infile)

    if len(sources) == 0:
        for target in targets:
            with open(target, 'w') as t:
                t.write('dummy')
        return 0

    high = 300
    if 'high' in config.jellyfishParams:
        high = config.jellyfishParams['high']

    cmd  = 'python wrappers/mkplot_wrapper.py %s %d' % ( sources[0], high )

    res    = run.runString('histo2png' , cmd)

    if res != 0 :
        return 150 + res

    resout = checkFiles(targets)

    if resout: return 150 + resout

    return 0


def cleanContamination(target=None, source=None, env=None, pairs=None):
    sources = map(lambda x: str(x), source)
    targets = map(lambda x: str(x), target)

    print '  CLEAN DATA '+" ".join(sources)+' > '+" ".join(targets)

    resin = checkFiles(sources)
    if resin:  return 100 + resin

    tech = None
    if   sources[0].endswith('.fastq'):
        tech = 'illumina'

    elif sources[0].endswith('.sff'):
        tech = '454'

    else:
        #TODO: FIGURE OUT HOW TO DIFFERENTIATE ILL FROM PACBIO
        print "UNKNOWN EXTENSION"
        return 1

    #for file in targets:
    #    touch(file)
    #[ okFile, reportName ] + fou, fin

    cleans  = []
    reports = []
    oks     = []

    for i in range(0, len(targets), 3):
        print "I %d" % i
        outClean   = targets[i  ]
        outReport  = targets[i+1]
        outCleanOk = targets[i+2]

        if not os.path.exists(outClean) or not os.path.exists(outCleanOk):
            if os.path.exists(outClean):
                os.remove(outClean)
            if os.path.exists(outCleanOk):
                os.remove(outCleanOk)

        cleans.append(  outClean   )
        reports.append( outReport  )
        oks.append(     outCleanOk )

        #mkdir -p /home/assembly/dev_150/sample_heinz/_tmp/assembly/illumina_MP_2000_3018DAAXX_1_f_fastq
        #mkdir -p /home/assembly/dev_150/sample_heinz/_tmp/assembly/illumina_MP_2000_3018DAAXX_1_r_fastq
        #[ -f /home/assembly/dev_150/sample_heinz/_tmp/assembly/illumina_MP_2000_3018DAAXX_1_r_fastq/seq.fastq.clean.fastq ]
        #    && echo "Skip job" ||
        #    /home/smits065/projects/saulo_filter_tomato/fastq_filter.py
        #    /home/assembly/dev_150/sample_heinz/_tmp/illumina_MP_2000_3018DAAXX_1_f_fastq/seq.fastq
        #    /home/assembly/dev_150/sample_heinz/_tmp/illumina_MP_2000_3018DAAXX_1_r_fastq/seq.fastq
        #    /home/assembly/dev_150/sample_heinz/_tmp/assembly/illumina_MP_2000_3018DAAXX_1_f_fastq/seq.fastq.clean.fastq
        #    /home/assembly/dev_150/sample_heinz/_tmp/assembly/illumina_MP_2000_3018DAAXX_1_r_fastq/seq.fastq.clean.fastq
        #    /home/smits065/projects/saulo_filter_tomato/db/contamination_without_ecolidb_v0.2.fa
        #    0
        #    /home/assembly/dev_150/sample_heinz/_tmp/assembly/illumina_MP_2000_3018DAAXX_1_f_fastq/seq.fastq.clean.fastq.contamination.txt

    print 'SOURCES',sources
    print 'REPORTS',reports
    print 'OKS    ',oks


    cmdC = ""
    if tech in config.contamClean:
        cmdC  =  'python /home/assembly/dev_150/scripts/pipeline/progs/contam/contam_filter.py'
        cmdC += ' %s' % ( config.contamClean[tech]['db'] )
        cmdC += " %d" % config.contamClean[tech]['threshold']
        cmdC += " " + reports[0]
        cmdC += ' ' + " ".join( sources )
        cmdC += ' ' + " ".join( cleans  )

        #./fastq_filter.py <fw_reads.fastq> <rv_reads.fastq> <fw_reads_filtered.fastq> <rv_reads_filtered.fastq> <bwa_db> <max_percentage_contamination> <contamination_file>
        #./sff_filter.py <input_sff_file> <output_sff_file_clean> <bwa_db> <max_percentage_contamination> <contamination_file>

    else:
        print "unknown tech to clean",tech
        sys.exit(1)

    print "CONTAMINATION CLEAN",cmdC

    resC = run.runString('contamination clean' , cmdC)

    if resC != 0 :
        for outFile in cleans + reports + oks:
            if os.path.exists(outFile):
                os.remove(outFile)

        return 150 + resC


    if len(reports) > 1:
        if os.path.exists(reports[0]):
            shutil.copy(reports[0], reports[1])
        else:
            print "NO REPORT %s" % repots[0]
            return 150


    for ok in oks:
        with open(ok, 'w') as f:
            f.write('ok')

    return 0



def filter454(target=None, source=None, env=None, pairs=None):
    sources = map(lambda x: str(x), source)
    targets = map(lambda x: str(x), target)

    print 'FILTER 454 '+" ".join(sources)+' > '+" ".join(targets)

    resin = checkFiles(sources)
    if resin:  return 100 + resin

    outOk  = targets[0]
    outSff = targets[1]
    outGoo = targets[2]
    outBad = targets[3]

    cmdF = "python wrappers/filter454_wrapper.py \
           %(compressHomopolymerSize)d \
           %(seedLength)d \
           %(trim5).f \
           %(minCompressedSize)d \
           %(maxCompressedSize)d \
           %(maxNs)d \
           %(filterDuplicates)d" % config.filter454

    cmdF += ' %s %s %s %s' % (outSff, outGoo, outBad, ' '.join(sources))

    print "FILTER 454",cmdF

    resF = run.runString('FILTER 454' , cmdF)

    if resF != 0 :
        for outFile in targets:
            if os.path.exists(outFile):
                os.remove(outFile)

        return 150 + resF

    with open(outOk, 'w') as f:
        f.write('ok')

    #'%s \
    #<int: compress homopolymer size> \
    #<int: seed length> \
    #<01b: trim 5 prime> \
    #<int: min compressed size> \
    #<int: max compressed size> \
    #<int: max Ns> \
    #<01b: filter duplicates> \
    #<path: dst folder, dst single file (default ./res.sff), - to here multiple files, and + to source path> \
    #<file: out good ids file> \
    #<file: out bad ids file> \
    #[1 dir, fasta files, fastq files, sff files]' % sys.argv[0]
    #        print 'e.g.: ', sys.argv[0], ' 1 50 1 50 900 1 1 res.sff good_f.ids bad_f.ids *.sff'
    return 0







def jellyDump(target=None, source=None, env=None, pairs=None):
    sources = map(lambda x: str(x), source)
    targets = map(lambda x: str(x), target)

    print '  JELLY DUMP '+" ".join(sources)+' > '+" ".join(targets)

    resin = checkFiles(sources)
    if resin:  return 100 + resin

    #for file in targets:
    #    touch(file)

    outDump   = targets[0]
    outDumpOk = outDump + '.ok'
    quakeInJf = [x for x in sources if str(x).endswith('.jf') ]

    if not os.path.exists(outDump) or not os.path.exists(outDumpOk):
        if os.path.exists(outDump):
            os.remove(outDump)

        if os.path.exists(outDumpOk):
            os.remove(outDumpOk)

        #DUMP
        cmdJ  = 'python wrappers/jellyfish_wrapper.py --program dump --tab --column'
        if 'lower-count' in config.jellyfishParams:
            cmdJ += ' --lower-count=%d' % config.jellyfishParams['lower-count']

        cmdJ   += ' --output %s %s' % (outDump, " ".join(quakeInJf))

        print "JELLY DUMP",cmdJ

        resJ    = run.runString('jelly dump' , cmdJ)

        if resJ != 0 :
            if os.path.exists(outDump):
                os.remove(outDump)

            if os.path.exists(outDumpOk):
                os.remove(outDumpOk)
            return 150 + resJ

    with open(outDumpOk, 'w') as f:
        f.write('ok')

    return 0


def runQuake(target, source, env, pairs=None):
    sources = map(lambda x: str(x), source)
    targets = map(lambda x: str(x), target)

    print '  QUAKE '+" ".join(sources)+' > '+" ".join(targets)

    #/mnt/nexenta/assembly/nobackup/dev_150/sample_heinz_nano/_tmp/assembly/none/illumina/sample_heinz_nano.quake.lst.qcts
    #/mnt/nexenta/assembly/nobackup/dev_150/sample_heinz_nano/_tmp/assembly/none/illumina/sample_heinz_nano.quake.lst

    #/mnt/nexenta/assembly/nobackup/dev_150/sample_heinz_nano/_tmp/assembly/none/illumina/cutoff.txt
    #/mnt/nexenta/assembly/nobackup/dev_150/sample_heinz_nano/_tmp/assembly/none/illumina/kmers.txt
    #/mnt/nexenta/assembly/nobackup/dev_150/sample_heinz_nano/_tmp/illumina_PE_500_sample_R1_001_fastq_128th_fastq/seq.fastq.trimmed.paired.fastq.contamCleaned.quake.cor.fastq
    #/mnt/nexenta/assembly/nobackup/dev_150/sample_heinz_nano/_tmp/illumina_PE_500_sample_R1_001_fastq_128th_fastq/seq.fastq.trimmed.paired.fastq.contamCleaned.quake.cor_single.fastq
    #/mnt/nexenta/assembly/nobackup/dev_150/sample_heinz_nano/_tmp/illumina_PE_500_sample_R1_001_fastq_128th_fastq/seq.fastq.trimmed.paired.fastq.contamCleaned.quake.fastq.log
    #/mnt/nexenta/assembly/nobackup/dev_150/sample_heinz_nano/_tmp/illumina_PE_500_sample_R1_001_fastq_128th_fastq/seq.fastq.trimmed.paired.fastq.contamCleaned.quake.stats.txt
    #/mnt/nexenta/assembly/nobackup/dev_150/sample_heinz_nano/_tmp/illumina_PE_500_sample_R1_001_fastq_128th_fastq/seq.fastq.trimmed.paired.fastq.contamCleaned.quake.error_model.txt

    resin = checkFiles(sources)
    if resin:  return 100 + resin

    #for file in targets:
    #    touch(file)
    #TODO: consider copying the files to the tmp folder so that the
    #      writing and readig can be faster if in a faster disk
    #      - run first program from quake or read cutoff.txt
    #      - run second program from quake or read kmers.txt
    #      - create one file for each pair
    #      - run 'correct' to each of those files

    outQCut     = targets[0]
    outQKme     = targets[1]

    quakeInDump = sources[0]
    #quakeInHist = sources[1]
    quakeInList = sources[1]

    cmdQ  = 'python wrappers/quake_wrapper.py %s' % os.path.dirname(outQCut)

    for par in ['-p', '--ratio', '-l', '-t']:
        if par in config.quakeParams:
            cmdQ += ' ' + par + ' ' + str(config.quakeParams[par])

    for par in ['--no_cut', '-u']:
        if par in config.quakeParams:
            if config.quakeParams[par]:
                cmdQ += ' ' + par

    cutoff = None
    if os.path.exists(outQCut + '.tmp'):
        with open(outQCut + '.tmp', 'r') as c:
            for line in c:
                if cutoff is None:
                    cutoff = int(line)
                    break

    if cutoff is not None:
        cmdQ += ' --cutoff %d' % cutoff
    else:
        print kwargs
        print "!"*100
        print "CUTOFF IS NONE"
        print "!"*100
        return 1

    #if not os.path.exists(os.path.join(os.path.dirname(outQLog), 'cutoff.txt')):
    #    shutil.copy('/home/assembly/dev_150/sample_heinz_nano/_tmp/assembly/cutoff.txt', os.path.dirname(outQLog))

    k = config.jellyfishParams['mer-len']

    cmdQ   += ' --no_count --log -f %s -k %d' % ( quakeInList, k )

    print "QUAKE CMD",cmdQ

    resQ   = run.runString('quake' , cmdQ)

    if resQ != 0 :
        return 150 + resQ

    resout = checkFiles(targets)

    if resout: return 150 + resout

    return 0

    #QUAKE
    #-h, --help            show this help message and exit
    #-r READSF             Fastq file of reads
    #-f READS_LISTF        File containing fastq file names, one per line or two
    #                      per line for paired end reads.
    #-k K                  Size of k-mers to correct
    #-p PROC               Number of processes [default: 4]
    #-q QUALITY_SCALE      Quality value ascii scale, generally 64 or 33. If not
    #                      specified, it will guess.


    #'-p'     : 20,      # number of processes
    #'--nocut': True,    # coverage model is optimized and cutoff was printed to
    #                    #   expected file cutoff.txt (default false)
    #'--ratio': 500,     # Likelihood ratio to set trusted/untrusted cutoff.
    #                    #   Generally set between 10-1000 with lower numbers
    #                    #   suggesting a lower threshold. [default: 200]
    #'-l'     :         # MIN_READ         Return only reads corrected and/or
                        #   trimmed to <min_read> bp
    #-u                 # Output error reads even if they can't be corrected,
                        #   maintaing paired end reads
    #-t TRIM_PAR        # Use BWA-like trim parameter <trim_par>
                        #   Parameter for read trimming. BWA trims a read down to
                        #   argmax_x{\sum_{i=x+1}^l(INT-q_i)}
                        #   if q_l<INT where l is the original read length. [0]
    #--headers          # Output only the original read headers without
                        #   correction messages
    #--log              # Output a log of all corrections into *.log as "quality
                        #   position new_nt old_nt"
    pass


def getPairString(pairs):
    outFiles = []
    pairStr  = ''
    for pair in pairs:
        pairLine = "\t".join([str(x) for x in pair]) + "\n"
        print "  PAIR LINE",pairLine
        pairStr += pairLine

        for runPath in pair:
            runPathStr = str(runPath)
            if runPathStr[-6:] == '.fastq':
                runPathStr = runPathStr[:-6]
            else:
                print "NOT FASTQ FILE: %s" % runPathStr
                sys.exit(1)

            for extNfo in [
                ['.cor.fastq'       , True ],
                ['.cor_single.fastq', True ],
                ['.err_single.fastq', False],
                ['.err.fastq'       , False],
                ['.fastq.log'       , True ],
                ['.stats.txt'       , True ],
                ['.error_model.txt' , True ]]:
                ext        = extNfo[0]
                compulsody = extNfo[1]

                if compulsody:
                    outFiles.append(runPathStr + '.quake' + ext       )

                #outFiles.append(runPath + '.cor.fastq'       )
                #outFiles.append(runPath + '.cor_single.fastq')
                #outFiles.append(runPath + '.err_single.fastq')
                #outFiles.append(runPath + '.err.fastq'       )
                #A.cor.fastq            B.cor.fastq         Validated and corrected reads
                #A.cor_single.fastq     B.err_single.fastq  Validated and corrected reads from A, error reads from B
                #A.err_single.fastq     B.cor_single.fastq  Validated and corrected reads from B, error reads from A
                #A.err.fastq            B.err.fastq         Error reads

            #outFiles.append( runPath + 'quake.error_model.txt' )

    return [pairStr, outFiles]


def fastqc(target=None, source=None, env=None):
    sources = map(lambda x: str(x), source)
    targets = map(lambda x: str(x), target)

    print '  FASTQC '+" ".join(sources)+' > '+" ".join(targets)

    resin = checkFiles(sources)
    if resin:  return 100 + resin

    for file in targets:
        touch(file)

    resout = checkFiles(targets)

    if resout: return 150 + resout

    ##run.runString('joinjf' , 'echo '+" ".join(sources)+' > '+" ".join(targets))

    return 0


def solexaqa(target=None, source=None, env=None):
    sources = map(lambda x: str(x), source)
    targets = map(lambda x: str(x), target)

    print '  SOLEXAQA '+" ".join(sources)+' > '+" ".join(targets)

    resin = checkFiles(sources)
    if resin:  return 100 + resin

    for file in targets:
        touch(file)

    resout = checkFiles(targets)

    if resout: return 150 + resout

    ##run.runString('joinjf' , 'echo '+" ".join(sources)+' > '+" ".join(targets))

    return 0


def trimFastq(target=None, source=None, env=None):
    sources = map(lambda x: str(x), source)
    targets = map(lambda x: str(x), target)

    print '  TRIMFQ '+" ".join(sources)+' > '+" ".join(targets)

    resin = checkFiles(sources)
    if resin:  return 100 + resin

    cmd = 'python wrappers/dynamictrim_wrapper.py '

    for par in config.trimFastq:
        cmd += ' ' + par + ' ' + str( config.trimFastq[par] )

    print "  TRIM FQ",cmd

    cmd += " " + " ".join(sources)

    res = run.runString('trimfq' , cmd)

    if res != 0 :
        return 150 + res

    resout = checkFiles(targets)

    if resout: return 150 + resout

    return 0


def checkOk(target=None, source=None, env=None):

    sources = map(lambda x: str(x), source)
    targets = map(lambda x: str(x), target)

    print '  CHECK OK', " ".join(sources) , '>' , " ".join(targets)

    resin = checkFiles(sources)
    if resin:  return 100 + resin

    #TODO: ADD SOME DATA. FILELIST MAYBE
    for file in targets:
        touch(file)

    resout = checkFiles(targets)

    if resout: return 150 + resout

    return 0

#project
#/home/aflit001//public_html_acessories/mkdb.pl $PROJFOLDER 1>/dev/null


#dokmer
#KMERS=`cat $OUTF/$fastqBase.nfo | perl -ne 'if (/(?:KMER:*|KMERCOUNT:*)\s*-*(\d+)/) { print $1 }'`
#JELLYIN="<(gunzip -c $fastq)"
#IN="$JELLYIN"
#CMD="jellyfish count --mer-len=$JELLYMERLEN --threads=$JELLYTHREADS --counter-len=$JELLYCOUNTERLEN --size=$JELLYHASHSIZE --both-strands -o $OUTF/"$fastqBase"_mer_counts $IN"
#FCOUNT=`ls $OUTF/$fastqBase"_mer_counts_"* | wc -l`
#elif [[ $FCOUNT -eq "1" ]]; then
#CMD="cp $FILES $OUTF/$fastqBase.jf"
#runCmd "$SCRIPT_NAME" "$LINENO" "$OUTF/$JOUT" "$OUT" "$SUBTITLE" "$DESC" "$CMD" "$EXITCODE"
#else
#CMD="jellyfish merge --buffer-size=$JELLYBUFFERSIZE --out-buffer-size=$JELLYBUFFERSIZE -o $OUTF/$fastqBase.jf "`echo $FILES`
#CMD="jellyfish stats -o $OUT $IN";
#CMD="jellyfish histo --low=1 --high=$JELLYHISTOHIGH --increment=1 --buffer-size=$JELLYBUFFERSIZE --threads=$JELLYTHREADS -o $OUT $IN";
#IN="$OUTF/$fastqBase.histo"
#OUT="$OUTF/$fastqBase.histo.png"
#CMD="$MKPLOT $IN"


#dokmerdoproject
#CMD="jellyfish merge --buffer-size=$JELLYBUFFERSIZE --out-buffer-size=$JELLYBUFFERSIZE -o $OUT $IN";
#CMD="jellyfish stats -o $OUT $IN";
#CMD="nohup jellyfish histo --low=1 --high=$PROJHISTOHIGH --increment=1 --buffer-size=$JELLYBUFFERSIZE --threads=$JELLYTHREADS -o $OUT $IN";
#IN="$OUTF/$LIBFOLDERSHORT.histo"
#OUT="$OUTF/$LIBFOLDERSHORT.histo.png"
#CMD="$MKPLOT $IN";


#dokmerdoprojectdoparent
#SOURCES=`ls $OUTF/"$PARENT"_*.jf | grep -iv ".fastq.jf" | grep -iv ".fastq.gz.jf" | perl -ne 'BEGIN{ @f = () } END{ print join(" ", @f) } chomp; push(@f, $_);'`
#FCOUNT=`ls $OUTF/"$PARENT"_*.jf  | grep -iv ".fastq.jf" | grep -iv ".fastq.gz.jf" | wc -l`
#if [[ $FCOUNT -eq "0" ]]; then
#echoMsg "$SCRIPT_NAME" "$LINENO" "$TITLE" "NO INPUT FILE ${SOURCES[*]}. ERROR 722"
#exit 722
#elif [[ $FCOUNT -eq "1" ]]; then
#IN="$SOURCES"
#OUT="$OUTF/$PJF"
#CMD="cp $IN $OUT"
#CMD="jellyfish merge --buffer-size=$JELLYBUFFERSIZE --out-buffer-size=$JELLYBUFFERSIZE  -o $OUT $IN"
#CMD="jellyfish stats -o $OUT $IN";
#CMD="jellyfish histo --low=1 --high=$PARENTHISTOHIGH --increment=1 --buffer-size=$JELLYBUFFERSIZE --threads=$JELLYTHREADS -o $OUT $IN";
#IN="$OUTF/$PARENT.histo"
#OUT="$OUTF/$PARENT.histo.png"
#CMD="$MKPLOT  $IN";


#dokmerdonfo
#OUT="$OUTF/$fastqBase.nfo"
#CMD="fastqCount -i $IN -o $OUT";


#doqc
#LISTCMDQCF[COQCF]="./histoProjectDoQcDoFastqc \"$PROJFOLDER\" \"$TECHFOLDER\" \"$LIBFOLDER\" \"$LOGFILE\" \"$fastq\""
#LISTCMDQCS[COQCS]="./histoProjectDoQcDoSolexaqa \"$PROJFOLDER\" \"$TECHFOLDER\" \"$LIBFOLDER\" \"$LOGFILE\" \"$fastq\""


#doqcdofastqc
#OUT="$OUTQCFQ.zip"
#CMD="$QCP -o '$OUTQCDS' $fastq";
#SUBTITLE="$TITLE :: $fastq"
#DESC="$CMD"
#EXITCODE=31
#echoMsg "$SCRIPT_NAME" "$LINENO" "$SUBTITLE"
#if [[ ! -d "$OUTQCFQ/" && ! -f $OUTQCFQ.broken || $REDOQC -gt 0 ]]; then
#if [[ -d "$OUTQCFQ/" ]]; then
#rm -rf "$OUTQCFQ"
#rm $OUTQCFQ.zip
#fi
#if [[ ! -d "$OUTQCFQ" ]]; then
#    if [[ $IGNOREQCERROR -eq 0 ]]; then
#        echoMsg "$SCRIPT_NAME" "$LINENO" "$TITLE" "FAILED TO CREATE '$OUTQCFQ' DIR. ERROR 31"
#        exit 31
#    else
#        echoMsg "$SCRIPT_NAME" "$LINENO" "$TITLE" "FAILED TO CREATE '$OUTQCFQ' DIR BUT SET TO IGNORE. CONTINUING. ERROR 31b"
#        rm $OUTQCFQ.zip
#        touch $OUTQCFQ.broken
#    fi
#else
#    if [[ ! -f "$OUTQCFQ/fastqc_report.html" ]]; then
#        if [[  $IGNOREQCERROR -eq 0 ]]; then
#            echoMsg "$SCRIPT_NAME" "$LINENO" "$TITLE" "FAILED TO CREATE '$OUTQCFQ'. ERROR 31"
#            exit 31
#        else
#            echoMsg "$SCRIPT_NAME" "$LINENO" "$TITLE" "FAILED TO CREATE '$OUTQCFQ' BUT SET TO IGNORE. CONTINUING. ERROR 31a"
#            rm $OUTQCFQ.zip
#            rm -rf $OUTQCFQ
#            touch $OUTQCFQ.broken
#        fi
#    else
#        echoMsg "$SCRIPT_NAME" "$LINENO" "$TITLE" "SUCCESSFULLY CREATED '$OUTQCFQ'"
#    fi
#fi


#doqcdosolexaqa
#IN="$fastq"
#OUT="$OUTQCDS/$fastqName$fastqExt.png"
#CMD="$SOLEXAQA -s $SOLEXAQASAMPLESIZE $JELLYIN -o $OUTQCDS";
#QUALP="$OUTQCDS/$fastqName$fastqExt.quality.pdf"
#QUALI="$OUTQCDS/$fastqName$fastqExt.quality.pdf.png"
#
#
#if [[ -f "$QUALP" ]]; then
#    echoMsg "$SCRIPT_NAME" "$LINENO" "$TITLE" "CONVERTING '$QUALP' TO '$QUALI'"
#    convert  $QUALP $QUALI
#fi
#
#
#
#HISP="$OUTQCDS/$fastqName$fastqExt.segments.hist.pdf"
#HISI="$OUTQCDS/$fastqName$fastqExt.segments.hist.pdf.png"
#
#
#if [[ -f "$HISP" ]]; then
#    echoMsg "$SCRIPT_NAME" "$LINENO" "$TITLE" "CONVERTING '$HISP' TO '$HISI'"
#    convert  $HISP $HISI
#fi


#global
#echo "GLOBAL :: MERGING :: $FOLDER/$OUT.jf"
#GMERGE="jellyfish merge --buffer-size=800000000 -o $FOLDER/$OUT.jf ${NEXT[*]}"
#JSTAT="jellyfish stats -o $FOLDER/$OUT.stats $FOLDER/$OUT.jf"
#GHISTO="jellyfish histo --low=1 --high=$GLOBALHISTOHIGH --increment=1 --buffer-size=800000000 --threads=$THREADS -o $FOLDER/$OUT.histo $FOLDER/$OUT.jf"
#$MKPLOT $FOLDER/$OUT.histo


#setup
#BASE=/home/$USER/filter/Data
#OUTF=/home/$USER/filter/out/Data_0_mer
#MKPLOT=/home/$USER/filter/mkplot
#
#SLEEPEVERY=2
#WAITALL_DELAY=20
#
##THREADS
#MAXNFO=20
#MAXJELLY=5
#MAXQC=10
#MAXPROJ=2
#
#
##JELLY
#    DOKMER=0
#    #NFO
#        DONFO=1
#
#    #JELLY
#        DOJELLY=1
#            REDOCOUNT=0
#            REDOJELLY=0
#            REDOJELLYMERGE=0
#            REDOJELLYSTAT=0
#            REDOJELLYHISTO=0
#            REDOJELLYHISTOGRAPH=0
#
#            JELLYDELETETEMPMER=1
#            JELLYGRAPHLENGTH=50
#            JELLYHISTOHIGH=$(($JELLYGRAPHLENGTH-1))
#            JELLYTHREADS=8
#            JELLYHASHSIZE=800000000
#            JELLYBUFFERSIZE=100000000
#            #HASHSIZE=8000000
#            JELLYMERLEN=31
#            JELLYCOUNTERLEN=7
#
#    #PROJECT
#        DOPROJECT=1
#            REDOPROJMERGE=1
#            REDOPROJSTAT=1
#            REDOPROJHISTO=1
#            REDOPROJHISTOGRAPH=1
#
#            PROJDELETETEMPMER=0
#            PROJGRAPHLENGTH=100
#            PROJHISTOHIGH=$(($PROJGRAPHLENGTH-1))
#
#    #PARENT
#        DOPARENT=1
#            REDOPARENTMERGE=1
#            REDOPARENTSTAT=1
#            REDOPARENTHISTO=1
#            REDOPARENTHISTOGRAPH=1
#
#            PARENTDELETETEMPMER=0
#            PARENTGRAPHLENGTH=200
#            PARENTHISTOHIGH=$(($PARENTGRAPHLENGTH-1))
#
##QC
#     DOQC=1
#        DOFASTQC=1
#            REDOQC=0
#            IGNOREQCERROR=0
#            OUTQC="/home/aflit001/public_html_acessories/src/fastqc"
#            QCP="/home/aflit001/bin/fqc2"
#        DOSOLEXAQA=1
#            REDOSOLQC=0
#            SOLEXAQA="/home/aflit001/progs/solexaqa/SolexaQA.pl"
#            OUTSOLEXAQA="/home/aflit001/public_html_acessories/src/solexaqa"
#            SOLEXAQASAMPLESIZE=250000
#
