import sys,os,time,inspect,traceback
fullPath        = os.path.abspath(os.path.dirname(inspect.getfile(inspect.currentframe())))
sys.path.insert(0, fullPath)
sys.path.insert(0, os.path.abspath(os.path.join(fullPath, '..')))
from shared import *
from tools  import *
import dbdump

setupExe      = setup.constants.programs['fastqScreen']['exe'    ]
setupThreads  = setup.constants.programs['fastqScreen']['threads']
setupSubset   = setup.constants.programs['fastqScreen']['subset' ]

class genContaminationError(generalError):
    def __init__(self, message):
        generalError.__init__(self, message)



# CONTAMINATION
class pluginContamination(plugin):
    className = "contamination"
    outFn     = "seq_screen.txt"
    outFnPic  = "seq_screen.png"
    def __init__(              self, parent):
        self['parent'            ] = parent
        self['contaminationData' ] = None
        self['contaminationOK'   ] = False
        self['contaminationFile' ] = None
        self['contaminationGraph'] = None
        self.printStats('INITIALIZING')

    def valid(                 self):
        """
        Checks whether the contamination analysis has succeeded and whether all
        the data has been loaded properly
        """
        self.printStats('CHECKING VALIDITY')
        if self.redo():
            self.printStats('CHECKING VALIDITY :: REDOING')
            return False

        base, outPath, outFileBase, outPathTmp, outPathFull, outFile    = getDstFolders( self['parent'], self.outFn    )
        base, outPath, outFileBase, outPathTmp, outPathFull, outFilePic = getDstFolders( self['parent'], self.outFnPic )

        if  os.path.exists( outFile    ) and \
            os.path.exists( outFilePic ):

            if  self['contaminationOK'   ] and \
                self['contaminationFile' ] is not None and \
                self['contaminationData' ] is not None and \
                self['contaminationGraph'] is not None:
                self.printStats('CHECKING VALIDITY :: VALID 1')
                return True
            else:
                self.parseContamination()

                if  self['contaminationOK'   ] and \
                    self['contaminationFile' ] is not None and \
                    self['contaminationData' ] is not None and \
                    self['contaminationGraph'] is not None:
                    self.printStats('CHECKING VALIDITY :: VALID 2')
                    self['parent'].saveYourself()
                    return True
                else:
                    self.printStats('CHECKING VALIDITY :: NOT VALID 1')
                    self['parent'].saveYourself()
                    return False
        else:
            self.printStats('CHECKING VALIDITY :: NOT VALID 2')
            return False

    def run(                   self, name=None, force=False):
        if name is None:
            name = self.className

        if not self.valid() or force:
            return self.runContamination()

    def getContanimationString(self):
        """
        returns a list in which the first element is the header of a table containing the name of the
        databases the contamination was tested on and the second element is the values returned
        """
        if  self['contaminationData'] is not None:
            #pp(self.contaminationData)
            #data.contaminationData[lineName] = "%.2f/%.2f/%.2f" % (unMapped, mapped, multi_mapped)
            contaminationData = self['contaminationData']
            keys              = contaminationData.keys()
            keys.sort()
            vals = []

            for sppName in keys:
                val    = contaminationData[sppName]
                valStr = []
                for v in val:
                    valStr.append("%.2f" % v)
                valStr = "/".join(valStr)
                vals.append(valStr)

            return ["\t".join(keys), "\t".join(vals)]
        else:
            self.printStats("NO CONTAMINATION DATA")
            return None

    def runContamination(      self):
        """
        run contamination test in a given file
        """
        try:
            #dstShort    # 601/illumina/pairedend_500/601_CGATGT_L002_R1_001.fastq.gz
            #base        # /home/assembly/tomato150/ril
            #outPath     # 608_illumina_pairedend_500_608_ACAGTG_L002_R2_001_fastq_gz
            #outFqBase   # 608_illumina_pairedend_500_608_ACAGTG_L002_R2_001_fastq_gz/seq.fastq
            #outFqPath   # _tmp/608_illumina_pairedend_500_608_ACAGTG_L002_R2_001_fastq_gz
            #outPathTmp  # /home/assembly/tomato150/ril/_tmp
            #outPathFull # /home/assembly/tomato150/ril/_tmp/608_illumina_pairedend_500_608_ACAGTG_L002_R2_001_fastq_gz
            #outFq       # /home/assembly/tomato150/ril/_tmp/608_illumina_pairedend_500_608_ACAGTG_L002_R2_001_fastq_gz/seq.fastq

            base, outPath, outFileBase, outPathTmp, outPathFull, outFile    = getDstFolders(self['parent'], self.outFn    )
            base, outPath, outFileBase, outPathTmp, outPathFull, outFilePic = getDstFolders(self['parent'], self.outFnPic )

            compression       = self['parent'].getPlugin('compression')
            inFq              = compression.getDecompressedFilePath()

            self.printStats("OUT FILE  %s" % (outFile   ))
            self.printStats("OUT GRAPH %s" % (outFilePic))


            if self.do():
                #TODO: MOVE TO GENDATA CLASS
                #--illumina
                fqScreenCmd   = '%s --threads %d --subset %d %%(in)s' % ( setupExe, setupThreads, setupSubset )
                #fqScreenCmd += ''
                if  not os.path.exists(outFile   ) or \
                    not os.path.exists(outFilePic) or \
                    not self['contaminationOK']    or \
                    self.redo():

                    self.printStats("RUNNING :: EXISTS %s %s ; %s %s OK %s REDO %s" % \
                                    (outFile,    os.path.exists(outFile),
                                     outFilePic, os.path.exists(outFilePic), \
                                     self['contaminationOK'], self.redo()))

                    self['contaminationData' ] = None
                    self['contaminationOK'   ] = False
                    self['contaminationFile' ] = None
                    self['contaminationGraph'] = None

                    if os.path.exists(outFile):
                        try:
                            os.remove(outFile)
                        except OSError:
                            pass

                    if os.path.exists(outFilePic):
                        try:
                            os.remove(outFilePic)
                        except OSError:
                            pass

                    resTest = run.runString(self.getPrintStr()+" RUNNING :: OUT %s" % (outPath),  fqScreenCmd % { 'in': inFq } )

                    if resTest != 0:
                        self.printStats("RUNNING :: OUT PATH %s :: ERROR" % (outCont))
                        self['contaminationOK'] = False

                        try:
                            os.remove(outContPath)
                        except OSError:
                            pass
                    else:
                        self.printStats("RUNNING :: OK")
                        self['contaminationOK'   ] = True

                        #Library	Unmapped	Mapped	Multi_mapped
                        #Hsap	100.00	0.00	0
                        #Mmus	100.00	0.00	0
                        #Sce	99.98	0.01	0.00
                        #PhX174	99.42	0.58	0
                        #IllCont	100.00	0.00	0
                        #UniVec	100.00	0.00	0
                        #Slyc	15.74	68.05	16.21
                        #EcoliM	100.00	0.00	0.00
                        #Viruses	99.98	0.01	0.01
                        #Viruses2	100.00	0.00	0
                        #Insects	99.92	0.04	0.04

                        if  os.path.exists(outFile) and os.path.exists(outFilePic):
                            self.parseContamination()

                        else:
                            self['contaminationOK'   ] = False
                            self['contaminationFile' ] = None
                            self.printStats("RUNNING :: OUT PATH %s :: ERROR :: PATH %s EXISTS %s RES %s" % \
                            (outPath, outFile, os.path.exists(outFile), self['contaminationOK']))

                            traceback.print_stack()
                            traceback.print_exc()
                            msg    = self.getPrintStr()+" RUNNING :: OUT PATH %s :: ERROR :: PATH %s EXISTS %s RES %s" % \
                            (outPath, outFile, os.path.exists(outFile), self['contaminationOK'])
                            reason = "PATH %s DOES NOT EXISTS" % str(outFile)
                            exc_type, exc_value, exc_traceback = sys.exc_info()
                            raise genContaminationError(genErrorMsg(2, msg, reason, traceback.format_stack(), traceback.format_exception(exc_type, exc_value, exc_traceback)))
            else:
                pass
        except Exception, e:
            traceback.print_stack()
            traceback.print_exc()
            msg    = self.getPrintStr()+" UNKNOWN EXCEPTION ON CONTAMINATION TO %s" % outFile
            reason = "%s" % str(e)
            exc_type, exc_value, exc_traceback = sys.exc_info()
            raise genContaminationError(genErrorMsg(3, msg, reason, traceback.format_stack(), traceback.format_exception(exc_type, exc_value, exc_traceback)))

    def parseContamination(self):
        base, outPath, outFileBase, outPathTmp, outPathFull, outFile    = getDstFolders(self['parent'], self.outFn    )
        base, outPath, outFileBase, outPathTmp, outPathFull, outFilePic = getDstFolders(self['parent'], self.outFnPic )
        contaminationData = {}

        self.printStats("PARSING")

        with open(outFile, 'r') as f:
            count    = 0
            colNames = []
            for line in f.readlines():
                count += 1
                if count < 3: #header
                    #for vName in line.split()[1:]:
                    #    colNames.append(vName)
                    pass
                else:
                    vals         = line.split()
                    if len(vals) > 2:
                        lineName     = vals[0]
                        #nums         = [ float(x) for x in vals[1:] ]
                        nums = []
                        for x in vals[1:]:
                            #print "FLOATING X "+x
                            nums.append(float(x))
                        #colName      = colNames[colNum - 1]
                        contaminationData[lineName] = nums
                    else:
                        #print "LINE '%s' IS NOT VALID. SKIPPING" % line
                        #sys.exit(1)
                        pass
                #print "        CONTAMINATION :: %s :: %s" % (dstShort, line),

        contGraphStr = image2string(outFilePic)
        self['contaminationOK'   ] = True
        self['contaminationFile' ] = os.path.basename(outFile)
        self['contaminationData' ] = contaminationData
        self['contaminationGraph'] = contGraphStr
