import os,sys,time, inspect
fullPath        = os.path.abspath(os.path.dirname(inspect.getfile(inspect.currentframe())))
sys.path.insert(0, fullPath)

sys.path.insert(0, os.path.abspath(os.path.join(fullPath, '..')))
import setup
import dataplugins

sys.path.insert(0, os.path.abspath(os.path.join(fullPath, '..', '..')))

class generalError(Exception):
    def __init__(self, message):
        Exception.__init__(self, message)

def getDstFolders(dataObj, fileName):
    return dataplugins.getDstFolders(dataObj, fileName)

#def getDstFolders(dataObj, fileName):
#    base        = dataObj.getProjectBase()                 # /home/assembly/tomato150/ril
#    outPath     = dataObj.getTmpFolder()                   # 608_illumina_pairedend_500_608_ACAGTG_L002_R2_001_fastq_gz
#    outFileBase = os.path.join(outPath   , fileName)  # 608_illumina_pairedend_500_608_ACAGTG_L002_R2_001_fastq_gz/seq.fastq
#    outPathTmp  = os.path.join(base      , setup.tmpFolder)    # /home/assembly/tomato150/ril/_tmp
#    outPathFull = os.path.join(outPathTmp, outPath)     # /home/assembly/tomato150/ril/_tmp/608_illumina_pairedend_500_608_ACAGTG_L002_R2_001_fastq_gz
#    outFile     = os.path.join(outPathTmp, outFileBase)   # /home/assembly/tomato150/ril/_tmp/608_illumina_pairedend_500_608_ACAGTG_L002_R2_001_fastq_gz/seq.fastq
#    return (base, outPath, outFileBase, outPathTmp, outPathFull, outFile)

setup.nameLength = 12
setup.projLength =  6

class plugin(dict):
    className  = 'plugin'
    def printStats(self, msg):
        print self.getPrintStr() + " " + msg

    def redo(        self):
        if self.className in setup.redo and setup.redo[self.className]:
            return True
        else:
            return False

    def do(         self):
        if self.className not in setup.does or setup.does[self.className]:
            return True
        else:
            return False

    def getPrintStr(self):
        projName = self['parent'].projectName
        name     = self['parent'].infile

        if len(projName) > setup.projLength:
            setup.projLength = len(projName)

        if len(name) > setup.nameLength:
            setup.nameLength = len(name)

        fmt = "      %-"+str(setup.projLength)+"s :: %-"+str(setup.nameLength)+"s :: %s"

        return fmt % (projName, name, self.className.upper())
