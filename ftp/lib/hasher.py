from hashlib import sha256
from tools   import *


class hasher(object):
    def __init__(self, indata, isfile, runId="", verbose=False):
        self.runId                = runId
        self.file_sha256_checksum = sha256()
        self.sumSoFar             = 0
        self.hashStartTime        = time.time()
        self.lastPerc             = 0
        self.perc                 = 0
        self.currByte             = 0

        if isfile: # is file
            self.infile           = indata
            self.hashFileSize     = os.path.getsize(indata)
            self.runFile(verbose=verbose)
        else: #is string
            self.instring         = indata
            self.hashStringSize   = len(indata)
            self.runString(verbose=verbose)

    def runString(self, verbose=False):
        self.update(self.instring, self.hashStringSize, verbose=verbose)

    def runFile(self, verbose=False):
        chunk_size   = 16 * 1024 * 1024

        with open(self.infile, "rb") as f:
            byte                 = f.read(chunk_size)
            while byte:
                self.update(byte, self.hashFileSize, verbose=verbose)
                byte                 = f.read(chunk_size)

    def update(self, byte, maxSize, verbose=False):
        self.file_sha256_checksum.update(byte)
        self.currByte        += len(byte)
        perc                 = self.currByte * 1.0 / maxSize * 100.0

        if int(perc) != self.lastPerc and verbose:
            timerRes  = timer(start=self.hashStartTime, target=maxSize, curr=self.currByte )
            ela       = timerRes['elaStr']
            etc       = timerRes['etaStr']
            speed     = timerRes['speed' ]
            #perc     = timerRes['perc'  ]
            self.lastPerc = int(perc)
            print "        ID %s CREATING HASH %12d / %12d [%3d%%] ELA %s ETC %s SPEED %dMB/s" % (self.runId, maxSize, self.currByte, perc, ela, etc, speed)

    def getHash(self):
        return self.file_sha256_checksum.hexdigest()



def sha256_for_file(infile, runId="", verbose=False):
    try:
        if os.path.exists(infile):
            sha256hasher = hasher(infile, True, runId=runId)
            return sha256hasher.getHash()
        else:
            return None
    except:
        return None

def sha256_for_string(indata, runId="", verbose=False):
    sha256hasher = hasher(indata, False, runId=runId, verbose=verbose)
    return sha256hasher.getHash()

