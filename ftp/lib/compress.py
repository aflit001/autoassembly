#!/usr/bin/python
import os,sys
import magic
mt = magic.Magic()
mm = magic.Magic(mime=True)

def checkCompression(fileName):
    zip  = None
    prog = None
    fileType = mt.from_file(fileName)
    fileMime = mm.from_file(fileName)

    if   fileMime == 'application/x-gzip':
        #print "  FILE ENDS IN GZ: "+str(self.fileName.rfind(".gz", len(self.fileName) - 3))
        #../../fastq_screen/data/homo_sapiens/Homo_sapiens.GRCh37.65.dna_rm.chromosome.18.fa.gz: gzip compressed data, was "Homo_sapiens.GRCh37.65.dna_rm.chromosome.18.fa", from Unix, last modified: Fri Nov 25 10:15:00 2011
        #TYPE gzip compressed data, max speed
        #MIME application/x-gzip

        zip      = 'gz'
        prog     = 'pigz --decompress --keep --processes 8 --stdout %(fn)s'
    elif fileMime == 'application/x-bzip2':
        #print "  FILE ENDS IN BZ2: "+str(self.fileName.rfind(".bz2", len(self.fileName) - 4))
        #../../sortmerge/sampleinput.sort.bz2: bzip2 compressed data, block size = 900k
        #TYPE bzip2 compressed data, block size = 900k
        #MIME application/x-bzip2

        zip      = 'bz2'
        prog     = 'pbzip2 --decompress --keep -p8 -m10000 --stdout %(fn)s'
    elif fileMime == 'application/x-xz':
        #print "  FILE ENDS IN XZ: "+str(self.fileName.rfind(".xz", len(self.fileName) - 3))
        #run.py.xz: XZ compressed data
        #TYPE XZ compressed data
        #MIME application/x-xz

        zip      = 'xz'
        prog     = 'xz --decompress --keep --stdout %(fn)s'

    elif fileMime == 'application/zip':
        #print "  FILE ENDS IN XZ: "+str(self.fileName.rfind(".xz", len(self.fileName) - 3))
        #../../bmp/old/easybmp/EasyBMP_1.06.zip: Zip archive data, at least v2.0 to extract
        #TYPE Zip archive data, at least v2.0 to extract
        #MIME application/zip

        zip      = 'zip'
        prog     = 'unzip -d %(out)s %(fn)s'

    else:
        #print "  FILE DOES NOT ENDS IN GZ"
        zip      = None
        prog     = None

    if zip is not None:
        ext1   = ( len(zip) * -1 )
        tarpos = ext1 - 5
        tarStr = fileName[tarpos:ext1]
        print "EXT1",ext1,"TARPOS",tarpos,"TARSTR",tarStr
        if tarStr == '.tar.':
            zip      = 'tar.'+zip
            prog    += ' | tar xv --directory %(out)s '
        elif zip == 'zip':
            pass
        else:
            prog    += ' > %(out)s '


    return [zip, prog, fileType, fileMime]


def decompress(fileName):
    finalName = fileName
    while os.path.islink(finalName):
        print "RESOLVING SYMBOLIC LINK:",finalName,">",
        finalName = os.path.realpath(finalName)
        print finalName

    fileFormat, program, fileType, fileMime = checkCompression(finalName)
    print "FILE"   ,fileName
    print "FORMAT" ,fileFormat
    print "PROGRAM",program
    print "TYPE"   ,fileType
    print "MIME"   ,fileMime
    if format is not None:
        print "CMD", program % { 'fn': fileName, 'out': '.'}
    return {'filename': fileName, 'filepath': finalName, 'format': fileFormat, 'fileType': fileType, 'fileMime': fileMime }

if __name__ == '__main__':
    if len(sys.argv) > 1:
        fileName = sys.argv[1]
        if os.path.exists(fileName):
            decompress(fileName)





def checkCompressionOrig(fileName):
    zip  = None
    prog = None
    type = mt.from_file(fileName)
    mime = mm.from_file(fileName)

    if   fileName.rfind(".gz", len(fileName) - 3) != -1:
        #print "  FILE ENDS IN GZ: "+str(self.fileName.rfind(".gz", len(self.fileName) - 3))
        #TYPE gzip compressed data, max speed
        #MIME application/x-gzip
        zip      = 'gz'
        prog     = 'pigz --decompress --keep --processes 8 --stdout %s > %s'
    elif fileName.rfind(".bz2", len(fileName) - 4) != -1:
        #print "  FILE ENDS IN BZ2: "+str(self.fileName.rfind(".bz2", len(self.fileName) - 4))
        zip      = 'bz2'
        prog     = 'pbzip2 --decompress --keep -p8 -m10000 --stdout %s > %s'
    elif fileName.rfind(".xz", len(fileName) - 3) != -1:
        #print "  FILE ENDS IN XZ: "+str(self.fileName.rfind(".xz", len(self.fileName) - 3))
        zip      = 'xz'
        prog     = 'xz --decompress --keep --stdout %s > %s'
    else:
        #print "  FILE DOES NOT ENDS IN GZ"
        zip      = None
        prog     = None
    return [zip, prog, type, mime]
