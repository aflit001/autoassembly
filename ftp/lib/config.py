#!/usr/bin/python
"""
reads the structure database dump (as defined in setup.py) (struct.json) and
converts it to the techs.general and techs.dataset data structure to be run
by scons
"""
import os,sys
import inspect
fullPath        = os.path.dirname(inspect.getfile(inspect.currentframe()))
prevPath        = os.path.abspath(os.path.join(fullPath, ".."))
sys.path.insert(0,fullPath)
sys.path.insert(0,prevPath)

import tools
from techs import general,dataset
import setup
import dbdump
import constants
import dataplugins

configFile = os.path.abspath(os.path.join('..', 'setup'))
setupLocal = dbdump.loadDump(configFile)
if setupLocal is None:
    print "NO SETUP TO LOAD"
    sys.exit(1)


for key in setupLocal:
    globals()[key] = setupLocal[key]







#-------------------------------------------------------------------------------------------


print 'loading structure'
structureFile = os.path.abspath(os.path.join('..', 'ftp', constants.dumpStruct))
print "  %s" % structureFile
structure  = dbdump.loadDump(structureFile)
print 'structure loaded'
import pprint
pprint.pprint(structure)

structKeys = structure.keys()
structKeys.sort()

allSppsDatasets = dataset.datasets('all', setup)


projectList      = {}
for projectName in structKeys:
    print "PROJECT NAME %s" % projectName,
    if projectName in projectsToIgnore:
        print
        print "  PROJECT %s SET TO BE IGNORED" % projectName
        continue
    projectList[projectName] = []

    projStatuses = structure[projectName]
    projectBase  = os.path.join(setup.getProjectRoot(projectName), projectName)


    print " BASE %s" % ( projectBase )
    projStatusesKeys = projStatuses.keys()
    projStatusesKeys.sort()

    for  projectStatus in projStatusesKeys:
        print "  STATUS %s"%projectStatus

        if projectStatus not in setup.statusesToClean:
            print "    SKIPPING STATUS %s. NOT TO BE CLEANED\n\n" % projectStatus
            continue

        samples       = projStatuses[projectStatus]

        samplesKeys   = samples.keys()
        samplesKeys.sort()
        sampleList    = {}

        for sampleId in samplesKeys:
            sampleList[sampleId] = []
            #sampleIdName = ""
            #if sampleId != '':
            #    sampleIdName = '_' + sampleId

            if sampleId in samplesToIgnore: continue

            technologies = samples[sampleId]

            print "   SAMPLE '%s'"%sampleId
            if len(technologies) == 0: continue

            techsKeys = technologies.keys()
            techsKeys.sort()
            techList  = {}

            for sequenceTech in techsKeys:
                libs = technologies[sequenceTech]
                techList[sequenceTech] = []

                print "      TECH %s"%sequenceTech
                if len(libs) == 0:
                    print "        TECH HAS NO LIB. SKIPPING"
                    continue

                libsKeys = libs.keys()
                libsKeys.sort()
                libList = {}

                for libraryName in libsKeys:
                    fileNames = libs[libraryName]
                    libList[libraryName] = []

                    print "        LIB %s" % libraryName
                    if len(fileNames) == 0: continue

                    fileNamesKeys = fileNames.keys()
                    fileNamesKeys.sort()
                    pairList   = {}

                    for fileName in fileNamesKeys:
                        print "          FILE NAME %s" % fileName#,"MTIME",fileMTime,"SIZE",fileSize#,"ID",fileId#,"DATA",data.pp()
                        data                = fileNames[fileName]
                        #fileNames[fileName] = data
                        runObj   = general.run(data)
                        pairName = data.pairName

                        if pairName is None:
                            pairName = '__none__'
                        if pairName not in pairList:
                            pairList[pairName] = []
                        pairList[pairName].append(runObj)

                    for pairName in pairList:
                        print "          PAIR %s" % pairName
                        pairFiles = pairList[pairName]
                        #print "            ",
                        pairObj   = general.pair(fastqs=pairFiles)
                        libList[libraryName].append(pairObj)

                for libName in libList:
                    libFiles = libList[ libName ]
                    print "            ",
                    libObj   = general.library( pairs=libFiles )
                    techList[sequenceTech].append(libObj)

            for techName in techList:
                techFiles = techList[techName]
                print "            ",
                techObj   = general.technology( libraries=techFiles, techName=sequenceTech )
                sampleList[sampleId].append(techObj)

        for sampleId in sampleList:
            sampleFiles = sampleList[sampleId]
            print "            ",
            sampleObj   = general.sample( technologies=sampleFiles )
            projectList[projectName].append(sampleObj)


for projectName in projectList:
    projFiles = projectList[projectName]
    print "            ",
    projObj   = general.project(samples=projFiles)
    allSppsDatasets.add(projObj)



print "FINISHED IMPORTING"

#print allSppsDatasets
#for dataset in allSppsDatasets:
#    print "dataset %s" % ( dataset )
