import os,sys
import pprint
"""
base of the project, this script summarizes the ftp structure and constants
which will be naively used by prepareFtpFiles.py and mkfolders.py
"""

import constants
import dbdump
sys.path.insert(0, os.path.abspath('..'))
#from folderStruct import *

folderStructure = dbdump.loadDump(os.path.abspath(os.path.join('..', 'setup')))
for key in folderStructure:
    globals()[key] = folderStructure[key]

# file to which the project description will be writen and read
dscFileName = os.path.join(base, "project_description.csv")
debug       = False
# expected genome size in order to calculate coverage



tmpFolder   = "_tmp_nobackup"      # folder for decompression, etc
structs     =   ['docs']  # extra folders to be created
statuses    =   [         # folders describing status of the data
                    #origin folder          | name         | dst summary   | dst sha
                    ['_prelim_nobackup'     , "PRELIMINARY", '_prelim'     , '_prelim'     ],
                    ['_prefiltered_nobackup', "PREFILTER"  , '_prefiltered', '_prefiltered'],
                    ['raw'                  , "CHECKED"    , 'docs'        , 'raw'         ],
                    ['filtered'             , "FILTERED"   , 'docs'        , 'filtered'    ]
                ]



#functions to easily retrieve statuses folders
def getProjectRoot(projectName):
    pairs = {}
    for reg in folders:
        pairs[reg[1]] = reg[0]
    #print "FOLDERS %s => %s" % ( projectName, pairs )

    if pairs.has_key(projectName):
        return pairs[projectName]
    else:
        return None

def getStatusInfo(statusName, pos):
    sdir = None
    for dst in statuses:
        if dst[1] == statusName:
            sdir = dst[pos]
    return sdir

def getStatusFolder(statusName):
    return getStatusInfo(statusName, 0)

def getStatusSummary(statusName):
    return getStatusInfo(statusName, 2)

def getStatusSha(statusName):
    return getStatusInfo(statusName, 3)



#add status folders to structure
for status in statuses:
    statusFolder = status[0]
    try:
        if structs.index(statusFolder):
            pass
    except ValueError:
        structs.append(statusFolder)

try:
    if structs.index(tmpFolder):
        pass
except ValueError:
    structs.append(tmpFolder)

for folder in folders:
    folderType = folder[2]
    folder.append([])
    folder[6].extend(structs)
    if folderType   == constants.DENOVO:
        folder[6].append('assembled')
    elif folderType == constants.MAPPING:
        folder[6].append('mapped')


def getsetup():
    globalVariables = globals()
    resVariables    = {}
    for key in globalVariables.keys():
        if key not in [ 'os'             , 'sys'             , 'pprint'        , 'copy'         ,
                        'dbdump'         , 'folderStructure' , 'getProjectRoot', 'getStatusInfo',
                        'getStatusFolder', 'getStatusSummary', 'getStatusSha'  , 'getsetup',
                        'constants']:
            if not key.startswith('__'):
                resVariables[key] = globalVariables[key]

    resVariables['constants'] = {}
    for k,v in vars(constants).iteritems():
        if not k.startswith('__'):
            resVariables['constants'][k] = v

    return resVariables
