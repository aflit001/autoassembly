import logging
import os,sys

class StreamToLogger(object):
    """
    Fake file-like stream object that redirects writes to a logger instance.
    """
    def __init__(self, logger, port, log_level=logging.INFO):
        self.logger    = logger
        self.log_level = log_level
        self.linebuf   = ''
        self.port      = port

    def write(self, buf):
        for line in buf.rstrip().splitlines(True):
            self.logger.log(self.log_level, line)
            self.port.write(line)
            self.port.write("\n")

    def flush(self):
        self.port.flush()


class logger(object):
    def __init__(self, name=None, level=None, port=None, fileName='log.log', fileMode='a'):
        if name is not None and level is not None and port is not None:
            print "FORWARDING %s LEVEL %s PORT %s FILE NAME %s" % (name, level, port, fileName)
            logging.basicConfig(
                level    = level,
                format   = '%(asctime)s:%(name)s: %(message)s',
                #format   = '%(asctime)s:%(levelname)-7s:%(name)s: %(message)s',
                filename = fileName,
                filemode = fileMode
            )

            logAbsPath  = os.path.abspath(fileName)
            logPath     = os.path.dirname(logAbsPath)
            if not os.path.exists(logPath):
                os.makedirs(logPath)

            logger     = logging.getLogger(name)
            slo        = StreamToLogger(logger, level)
            if   port == "out":
                slo        = StreamToLogger(logger, sys.stdout, level)
                sys.stdout = slo
            elif port == "err":
                slo        = StreamToLogger(logger, sys.stderr, level)
                sys.stderr = slo

            testStr = "TEST FORWARDING %s LEVEL %s PORT %s FILE NAME %s" % (name, level, port, fileName)
            print "OUT "+testStr
            sys.stderr.write("ERR "+testStr + "\n")


if __name__ == '__main__':
    logout = logger(name='STDOUT', level=logging.INFO,  port='out', fileName="out.log", fileMode='w')
    logerr = logger(name='STDERR', level=logging.ERROR, port='err', fileName="out.log", fileMode='w')

    #logging.basicConfig(
    #   level=logging.DEBUG,
    #   format='%(asctime)s:%(levelname)s:%(name)s:%(message)s',
    #   filename="out.log",
    #   filemode='a'
    #)
    #
    #stdout_logger = logging.getLogger('STDOUT')
    #slo           = StreamToLogger(stdout_logger, logging.INFO)
    #sys.stdout    = slo
    #
    #stderr_logger = logging.getLogger('STDERR')
    #sle           = StreamToLogger(stderr_logger, logging.ERROR)
    #sys.stderr    = sle

    print "Test to standard out"
    sys.stderr.write('Test to standard error\n')
    #raise Exception('Test to standard error')
