import sys, os, time, datetime
import cPickle as pickle
import shutil
from hasher import *


from tools   import *

dumperName   = 'JSON'  # opions are YAML PICKLE JSON
availDumpers = {'PICKLE': ['.pickle', 'saveDumpPickle', 'loadDumpPickle']}

try:
    import jsonpickle
    availDumpers['JSON'] = ['.json', 'saveDumpJson', 'loadDumpJson']
    useJson = True
except ImportError:
    useJson = False

try:
    #import yaml
    import yaml
    useYaml = True
    availDumpers['YAML']= ['.yaml', 'saveDumpYaml', 'loadDumpYaml']

    try:
        from yaml import CLoader as Loader, CDumper as Dumper
        #print "USING YAML C VERSION"
    except ImportError:
        from yaml import Loader, Dumper
except ImportError:
    #print "NO YAML INSTALLED"
    useYaml = False



if dumperName not in availDumpers.keys(): #fallback
    dumperName = 'PICKLE'

dumperExt  = availDumpers[dumperName][0]


def getTimestamp():
	return time.strftime('%Y%m%d%H%M%S')

def saveDumpYaml(dumpFilePath, dumpFilePathTmp, bkp, struct, dobackup=True):
    dataDump = str(yaml.dump(struct, Dumper=Dumper))

    shaFile = sha256_for_file(dumpFilePath, verbose=False)
    shaNew  = sha256_for_string(dataDump, verbose=False)

    if shaFile != shaNew:
        #print "  SHAFILE:%s"%shaFile
        #print "  SHANEW :%s"%shaNew

        if dobackup:
            try           : shutil.copy(dumpFilePath, bkp)
            except IOError: pass

        with open(dumpFilePathTmp, 'wb') as dbFileFh:
            dbFileFh.write(dataDump)

        if os.path.exists(dumpFilePath): os.unlink(dumpFilePath)
        os.rename(dumpFilePathTmp, dumpFilePath)
    else:
        #print "  NO NEED TO DUMP\n"
        pass

def saveDumpJson(dumpFilePath, dumpFilePathTmp, bkp, struct, dobackup=True):
    jsonpickle.set_encoder_options('simplejson', sort_keys=True, indent=1) #, separators=(',',':'))##, compactly=True)
    dataDump = str(jsonpickle.encode(struct))
    #print dataDump

    shaFile = sha256_for_file(dumpFilePath, verbose=False)
    shaNew  = sha256_for_string(dataDump, verbose=False)

    if shaFile != shaNew:
        #print "  SHAFILE:%s"%shaFile
        #print "  SHANEW :%s"%shaNew

        if dobackup:
            try           : shutil.copy(dumpFilePath, bkp)
            except IOError: pass

        with open(dumpFilePathTmp, 'wb') as dbFileFh:
            dbFileFh.write(dataDump)

        if os.path.exists(dumpFilePath): os.unlink(dumpFilePath)
        os.rename(dumpFilePathTmp, dumpFilePath)
    else:
        print "  NO NEED TO DUMP\n"
        pass

def saveDumpPickle(dumpFilePath, dumpFilePathTmp, bkp, struct, dobackup=True):
    with open(dumpFilePathTmp, 'wb') as dbFileFh:
        pickle.dump(struct, dbFileFh, pickle.HIGHEST_PROTOCOL)

    shaFile = sha256_for_file(dumpFilePath, verbose=False)
    shaNew  = sha256_for_file(dumpFilePathTmp, verbose=False)

    if shaFile != shaNew:
        if dobackup:
            try           : shutil.copy(dumpFilePath, bkp)
            except IOError: pass

        if os.path.exists(dumpFilePath): os.unlink(dumpFilePath)
        os.rename(dumpFilePathTmp, dumpFilePath)
    else:
        #print "  NO NEED TO DUMP\n"
        pass

def saveDump(dumpFile, struct, startTime=None, verbose=False, dumperNameLocal=dumperName, dobackup=True):
    if startTime is None:
        startTime = getTimestamp()

    #print "SAVING DUMP ",
    ext      = ""
    dumpFunc = None
    if   dumperNameLocal in availDumpers:
        ext          = availDumpers[dumperNameLocal][0]
        dumpFuncName = availDumpers[dumperNameLocal][1]
        try:
            dumpFunc = globals()[dumpFuncName]
        except KeyError:
            sys.stderr.write('UNKNOWN DUMPER FUNCTION %s'%dumpFuncName)
            sys.exit(1)
        except:
            sys.stderr.write('UNKNOWN ERRO')
            sys.exit(1)
    else:
        sys.stderr.write('UNKNOWN DUMPER %s'%dumperNameLocal)
        sys.exit(1)

    dumpFilePath    = dumpFile
    if not dumpFilePath.endswith(ext):
        dumpFilePath    += ext
    dumpFilePathTmp = dumpFilePath + ".tmp"
    #print dumpFilePath

    dbPath   = os.path.dirname(dumpFilePath)
    fileName = os.path.basename(dumpFilePath)
    dbPath   = os.path.join(dbPath, '.dbbkp')
    if not os.path.exists(dbPath):
        os.makedirs(dbPath)
    bkp      = os.path.join(dbPath, fileName)
    bkp     += ".bkp.%s.%s%s" % (startTime, getTimestamp(), ext)

    try:            os.remove(bkp)
    except OSError: pass

    return dumpFunc(dumpFilePath, dumpFilePathTmp, bkp, struct, dobackup=dobackup)

def loadDumpYaml(dumpFilePath):
    with open(dumpFilePath, 'rb') as f:
        return yaml.load(f, Loader=Loader)

def loadDumpJson(dumpFilePath):
    with open(dumpFilePath, 'rb') as f:
        dataStr = f.read()
        return jsonpickle.decode(dataStr)

def loadDumpPickle(dumpFilePath):
    with open(dumpFilePath, 'rb') as f:
        return pickle.load(f)

def loadDump(dumpFile, dumperNameLocal=dumperName):
    #print "LOADING DUMP %s"%dumpFile
    ext      = ""
    dumpFunc = None
    if   dumperNameLocal in availDumpers.keys():
        ext          = availDumpers[dumperNameLocal][0]
        dumpFuncName = availDumpers[dumperNameLocal][2]
        try:
            dumpFunc = globals()[dumpFuncName]
        except KeyError:
            sys.stderr.write('UNKNOWN DUMPER FUNCTION %s'%dumpFuncName)
            sys.exit(1)
        except:
            sys.stderr.write('UNKNOWN ERRO')
            sys.exit(1)


    dumpFilePath    = dumpFile
    if not dumpFilePath.endswith(ext):
        dumpFilePath    += ext
    #print "  DUMP PATH %s"%dumpFilePath

    #bkp = dumpFilePath+".bkp.%s%s" % (getTimestamp(), ext)
    #try:            os.remove(bkp)
    #except OSError: pass
    #if os.path.exists(dumpFilePath):
    #    shutil.copy(dumpFilePath, bkp)

    if os.path.exists(dumpFilePath):
        return dumpFunc(dumpFilePath)

    else:
        return None
