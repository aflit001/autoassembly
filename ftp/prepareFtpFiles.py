#!/usr/bin/python
#prepare files for ftp
import sys, os, time

"""
    By using the values in the setup module, reads the project description
    file and, using this information, analyses the data.

    By checking each file for contamination, quality (fastqc), and phred qualities
    assessment, generates reports in the desired folder.

    This script need the mkfolders.py and setup.py (both gnoistic of the particular
    project) in order to, agnostic to the project structure, create and read the
    proper files and create the reports in the desired folders.
"""

import pprint
import signal
#from subprocess      import call
import multiprocessing
from   multiprocessing import Process, Queue, Pool
import traceback
import logging
#from setup import *

sys.path.insert(0, 'lib')

import logger
import setup
import run
import templater
import dbdump
import constants
from   tools       import *
from   dataplugins import *


#pprint.pprint(globals())

plugins   = [   # class                        Max Threads
                [ Plugins.pluginCompression  ,  10 ],
                [ Plugins.pluginHash         ,  10 ],
                [ Plugins.pluginQuals        ,  10 ],
                [ Plugins.pluginFastQC       ,  10 ],
                [ Plugins.pluginContamination,   3 ],
            ]



setup.startTime = dbdump.getTimestamp()


if setup.debug:
    exportToFile = False
    replaceFiles = False

pf = pprint.pformat
pp = pprint.pprint


class generalError(Exception):
    def __init__(self, message):
        Exception.__init__(self, message)

class poolError(generalError):
    def __init__(self, message):
        generalError.__init__(self, message)

def sendProcess(func, procs2be, db, maxThreads=setup.maxThreads, name=""):
    """
    wrapper to start processes (defined in procs2be) sending them to the FUNC
    returning a list of processes and a pool object.

    wrapper to check which processes are ready and merge them back into the
    database by their keys
    """
    procs = []

    numThreads = len(procs2be)
    if numThreads > maxThreads:
        numThreads = maxThreads

    pool = None
    if numThreads > 0:
        pool   = Pool(processes=numThreads, initializer=init_worker)

        for proc in procs2be:
            #print "proc: ",proc[2]
            #[numDb, seqName, data, pluginName]
            procs.append([proc[0], proc[1], pool.apply_async( func, proc[2] )])
    else:
        pool   = Pool(processes=1, initializer=init_worker)


    #print "  LEN PROC %d" % len(procs)


    finished = []
    for trio in procs:
        poolEl = trio[2]
        if not poolEl.ready():
            #print "not ready"
            finished.append(trio)
        else:
            #print "ready"
            if not poolEl.successful():
                #print "not successfull"
                traceback.print_stack()
                traceback.print_exc()
                msg    = "NOT SUCCESSFULL TO RUN EL \"%s\"" % (str(poolEl))
                reason = "NOT SUCCESSFULL"
                exc_type, exc_value, exc_traceback = sys.exc_info()
                raise poolError(genErrorMsg(2, msg, reason, traceback.format_stack(), traceback.format_exception(exc_type, exc_value, exc_traceback)))

    sStart = time.time()

    if name != "":
        name = "- " + str(name) + " - "

    while len(finished) > 0:
        try:
            timming = timer(start=sStart, target=len(procs), curr=(len(procs)-len(finished)))
            ela     = timming['elaStr']
            etc     = timming['etaStr']
            print "    ACTIVE %s%3d/%3d - ELA %ss ETC %ss" % (name, len(finished), len(procs), ela, etc)
            time.sleep(setup.sleepWhileWaiting)
            finished = []
            for trio in procs:
                poolNu   = trio[1]
                poolEl   = trio[2]
                if not poolEl.ready():
                    finished.append(trio)
                else:
                    #print "%s ready"%poolNu
                    poolEl.wait(1)
                    try:
                        res = poolEl.get(1)
                    except multiprocessing.TimeoutError:
                        pass
                    except:
                        print "%s ready. got. error" % poolNu
                        traceback.print_stack()
                        traceback.print_exc()
                        msg    = "ERROR ON GETTING RESULT FROM THREAD"
                        reason = "ERROR ON THREAD"
                        exc_type, exc_value, exc_traceback = sys.exc_info()
                        raise poolError(genErrorMsg(2, msg, reason, traceback.format_stack(), traceback.format_exception(exc_type, exc_value, exc_traceback)))

        except KeyboardInterrupt:
            sys.stderr.write("Ctrl+C pressed main. quitting\n")
            pool.terminate()
            sys.stderr.write("Bye\n")
            sys.exit(1)



    print "  FINISHED. JOINING"
    pool.close()
    pool.join()


    print "  MERGING RESULT"

    if len(procs) > 0:
        #time.sleep(5)
        while len(procs) != 0:
            toDel = []

            for trio in procs:
                #print "    TRIO",trio
                numDb    = trio[0]
                seqName  = trio[1]
                res      = trio[2]
                #print "NUM %3d FILENAME %s" % (numDb, fileName)
                if res.ready():
                    res.wait(.5)
                    if res.successful():
                        print "      ID %3d :: FILE %s FINISHED SUCCESSFULLY"      % ( numDb, seqName )
                        #print "    res",res.get()
                        data = res.get()
                        if data is None:
                            print "      ID %3d :: FILE %s RETURNED NONE"          % ( numDb, seqName )
                            traceback.print_stack()
                            traceback.print_exc()
                            msg    = "      ID %3d :: FILE %s RETURNED NONE"          % ( numDb, seqName )
                            reason = "DATA RETURNED FROM THREAD IS NONE"
                            exc_type, exc_value, exc_traceback = sys.exc_info()
                            raise poolError(genErrorMsg(2, msg, reason, traceback.format_stack(), traceback.format_exception(exc_type, exc_value, exc_traceback)))
                        dataO = db[seqName[0]][seqName[1]]

                        if not compareData(dataO, data):
                            print "      ID %3d :: FILE %s RETURNED WRONG DATA"    % ( numDb, seqName )
                            traceback.print_stack()
                            traceback.print_exc()
                            msg    = "      ID %3d :: FILE %s RETURNED WRONG DATA"    % ( numDb, seqName )
                            reason = "ID OF DATA RETURNED DOES NOT MATCH ORIGINAL DATA"
                            exc_type, exc_value, exc_traceback = sys.exc_info()
                            raise poolError(genErrorMsg(2, msg, reason, traceback.format_stack(), traceback.format_exception(exc_type, exc_value, exc_traceback)))
                        db[seqName[0]][seqName[1]] = data
                        toDel.append(trio)
                    else:
                        sys.stderr.write("      ID %3d :: FILE %s FAILED TO RUN :: \"%s\"\n" % ( numDb, seqName, res._value ))
                        sys.stderr.write(pf(res.__dict__))
                        traceback.print_stack()
                        traceback.print_exc()
                        msg    = "      ID %3d :: FILE %s FAILED TO RUN :: \"%s\"\n" % ( numDb, seqName, res._value )
                        reason = "UNSUCESSFUL RUNNING. %s" % str(pf(res.__dict__))
                        exc_type, exc_value, exc_traceback = sys.exc_info()
                        raise poolError(genErrorMsg(2, msg, reason, traceback.format_stack(), traceback.format_exception(exc_type, exc_value, exc_traceback)))
                else:
                    print "      ID %3d :: FILE %s STILL RUNNING" % ( numDb, seqName )

            if len(toDel) > 0:
                for trio in toDel:
                    procs.remove(trio)
                #saveDump(dbFile, db, verbose=setup.debug)
    return db

def init_worker():
    """
    tels the child process to ignore signals so that only the mais
    program can accept a ctrl+c. avoiding zumbi processes
    """
    signal.signal(signal.SIGINT, signal.SIG_IGN)

def openLog():
    """
    Replaces stderr and stdout with a tee-ed log file. using this you will not be able to
    use commas to separate print parameters
    """
    logFile = 'log/' + os.path.basename(__file__) + ".%s" % setup.startTime + ".log"
    print "LOGFILE",logFile

    #logerr = logger.logger(name='STDERR', level=logging.ERROR, port='err', fileName=logBase+".err.log", fileMode='a')
    logout  = logger.logger(name='STDOUT', level=logging.INFO,  port='out', fileName=logFile, fileMode='w')

def compareData(data1, data2):
    """
    Compare if two data objects are the same or not through their IDENT function
    """
    ident1 = data1.getPluginResult('info', 'ident')
    ident2 = data2.getPluginResult('info', 'ident')
    if ident1 == ident2:
        return True
    else:
        #print data1.pp()
        #print data2.pp()
        return False

def compareDB(db1, db2):
    """
    Compare whether two data objects dicts are equal, element by element using the
    comparedata function
    """
    for projName in sorted(db1):
        for seqName in db1[projName]:
            data1 = db1[projName][seqName]
            data2 = db2[projName][seqName]
            if not compareData(data1, data2):
                return False
            
    for projName in sorted(db2):
        for seqName in db2[projName]:
            data1 = db1[projName][seqName]
            data2 = db2[projName][seqName]
            if not compareData(data1, data2):
                return False
            
    return True

def addData(fileName, projectName, pairName, projectType, projectStatus, sequenceTech, sampleId, sampleLibrary, libType, libSize, struct, idents, pluginsToAdd):
    """
    initialize a data object and check in the database if it already exists by its identifier
    if file already exists by name and:
        id is the same                    : keep db
        id is different (file has changed): give error
    if file does not exists by name and:
        id already exists (file mas moved): give error
        id does not exists (new file)     : add to db

    """
    data  = GenData(fileName, projectName, pairName, projectType, projectStatus, sequenceTech, sampleId, sampleLibrary, libType, libSize)
    ident = data.getPluginResult('info', 'ident')

    try:
        data2 = struct[projectName][projectStatus][sampleId][sequenceTech][sampleLibrary][fileName]

    except KeyError:
        if ident in idents:
            data2 = idents[ident]
            print "            FILE MOVED FROM %s TO %s UPDATING" % (data2.getFileName(), fileName)
            #sys.exit(1)
            #dataOld = db.pop(oldName)
            data2.update(fileName, projectName, pairName, projectType, projectStatus, sequenceTech, sampleId, sampleLibrary, libType, libSize)
            #db[fileName] = dataOld
            data2.addPlugins(pluginsToAdd)
            return data2
        else:
            print "            NEW FILE !!!!!!!!"
            print "              IDENT %s NOT IN IDENTS" % ident
            #print idents
            #sys.exit(0)
            #db[fileName] = data
            data.addPlugins(pluginsToAdd)
            #return db[fileName]
            return data

    if data.eq(data2):
        print "            KEEPING DB"
        data2.update(fileName, projectName, pairName, projectType, projectStatus, sequenceTech, sampleId, sampleLibrary, libType, libSize)
        #db[fileName] = data2
        #db[fileName].projectStatusName = projectStatus
        data2.addPlugins(pluginsToAdd)
        return data2

    else:
        print "            DATA DIFFERS FROM DB"
        dataDate  = data.getPluginResult( 'info', 'mtime')
        data2Date = data2.getPluginResult('info', 'mtime')
        #data2.update(fileName, projectName, pairName, projectType, projectStatus, sequenceTech, sampleId, sampleLibrary, libType, libSize)
        #data2.addPlugins(pluginsToAdd)
        if dataDate > data2Date:
            print "              FILE ON DISK IS NEWER"
        else:
            print "              FILE ON DB IS NEWER"
        data.addPlugins(pluginsToAdd)
        return data



def genData(numDb, seqName, data, pluginName):
    """
    calls the data.gendata
    """
    #print "NUMDB %d"%numDb
    try:
        #id = seqName
        #print "ID %s :: NUM %3d"                                                 % ( id, numDb )
        #print "  ID %s :: NUM %3d :: SIZE   %12d CREATING TIME %12d"             % ( id, numDb, data.getSize(),       data.getTime())

        #print "ID %s :: NUM %3d :: DECOMPRESSING"                                 % ( numDb, numDb )
        data.runPlugin('compression')
        #print "ID %s :: NUM %3d :: DECOMPRESSED"                                  % ( numDb, numDb )

        #print data.pp()

        if setup.runGenData:
            #data.genData()
            data.runPlugin(pluginName)
        else:
            sys.exit(0)
            print "not running due to setup.rungendata = false"

        #print "  ID %s :: NUM %3d :: HASH   %s"                                             % ( fileName, numDb, data.getHash())
        #print "  ID %s :: NUM %3d :: SEQLEN %12d NUM SEQS      %12d SUM SIZE %12d QUALS %s" % \
        #( fileName, numDb, data.getSeqLen(), data.getNumSeqs(), data.getRealFileSize(), str(data.getQuals()) )
        #print data.pp()
        #saveIndividualEntity(data, setup.startTime)
        return data
    except Exception, e:
        traceback.print_stack()
        traceback.print_exc()
        msg    = 'unknown error while gen data to %d seq %s' % (numDb, seqName)
        reason = "%s" % str(e)
        exc_type, exc_value, exc_traceback = sys.exc_info()
        raise generalError(genErrorMsg(2, msg, reason, traceback.format_stack(), traceback.format_exception(exc_type, exc_value, exc_traceback)))



def processDB(struct, numThreads=setup.maxThreads):
    """
    given a db:
        if file does not exists anymore: delete them from db.
        else:
            call gendata function to generate its hash and quality analysis
    """
    print "PROCESSING DB"
    procs2be = {}
    #q      = Queue()


    db, numDb = genDb(struct)

    for plugin in plugins:
        pluginName    = plugin[0].className
        pluginThreads = plugin[1]
        print "  PROCESSING %s WITH %d THREADS" % (pluginName, pluginThreads)

        processes = []
        for projName in sorted(db):
            for seqName in sorted(db[projName]):
                numDb    -= 1
                data      = db[projName][seqName]
                fileName  = data.getFileNamePath()
                pluginCls = data.getPlugin(pluginName)
    
                #TODO: ADD REDO
                if not pluginCls.valid():
                    if not os.path.exists(fileName):
                        print "  FILENAME %s DOES NOT EXISTS ANYMORE. REMOVING FROM DB :: %s" % (seqName, fileName)
                        #db.pop(seqName)
                        sys.exit(1)
                        continue
                    else:
                        #print "  FILENAME %s EXISTS. PROCESSING. ID %3d" % (fileName, numDb)
                        pass
    
                    processes.append([ numDb, tuple([projName, seqName]), tuple([numDb, seqName, data, pluginName]) ])
                    if setup.debug: break
    
            if len(processes) > 0:
                db2 = sendProcess(genData, processes, db, maxThreads=pluginThreads, name="PROCESSING DB %s" % pluginName)
                if not compareDB(db, db2):
                    return None
                else:
                    db = db2

        print "  PROCESSED  %s" % pluginName


    print "  FINISHED"
    return regenStruct(db)

def genidents(struct):
    idents = {}
    for projectName in sorted(struct):
        statuses = struct[projectName]

        for projectStatus in sorted(statuses):
            samples = statuses[projectStatus]
            
            for sampleId in sorted(samples):
                ids = samples[sampleId]
                
                for sequenceTech in sorted(ids):
                    techs = ids[sequenceTech]
                    
                    for sampleLibrary in sorted(techs):
                        libs = techs[sampleLibrary]
                    
                        for fileName in sorted(libs):
                            data = libs[fileName]
                            ident = data.getPluginResult('info', 'ident')
                            if ident not in idents:
                                idents[ident] = data
                            else:
                                print "duplicated file"
                                data.pp()
                                sys.exit(1)
    return idents

def genDb(struct):
    db    = {}
    numDb = 0
    
    for projectName in sorted(struct):
        statuses = struct[projectName]
        db[projectName] = {}
        
        for projectStatus in sorted(statuses):
            samples = statuses[projectStatus]
            
            for sampleId in sorted(samples):
                ids = samples[sampleId]
                
                for sequenceTech in sorted(ids):
                    techs = ids[sequenceTech]
                    
                    for sampleLibrary in sorted(techs):
                        libs = techs[sampleLibrary]
                    
                        for fileName in sorted(libs):
                            data = libs[fileName]
                            db[projectName][fileName] = data
                            numDb += 1
    return (db, numDb)

def regenStruct(db):
    newStruct = {}
    for projectName in sorted(db):
        for fileName in sorted(db[projectName]):
            data       = db[projectName][fileName]
            projName   = data.getprojectName()
            projStatus = data.getProjectStatus()
            sampleId   = data.getSampleId()
            seqTech    = data.getSequenceTech()
            sampleLib  = data.getSampleLibrary()
            fileName2  = data.getFileName()
    
            print "    %s :: %s :: %s" % (projName, projStatus, fileName)
            #data.saveYourself()
    
            if fileName == fileName2:
                if projName   not in newStruct:
                    newStruct[projName] = {}
    
                if projStatus not in newStruct[projName]:
                    newStruct[projName][projStatus] = {}
    
                if sampleId not in newStruct[projName][projStatus]:
                    newStruct[projName][projStatus][sampleId] = {}
    
                if seqTech not in newStruct[projName][projStatus][sampleId]:
                    newStruct[projName][projStatus][sampleId][seqTech] = {}
    
                if sampleLib not in newStruct[projName][projStatus][sampleId][seqTech]:
                    newStruct[projName][projStatus][sampleId][seqTech][sampleLib] = {}
    
                newStruct[projName][projStatus][sampleId][seqTech][sampleLib][fileName] = data
            else:
                sys.exit(1)

    return newStruct

def updateStruct(struct):
    print "  UPDATING STRUCTURE"

    for projectName in sorted(struct):
        statuses = struct[projectName]
        if len(statuses) == 0:
            print "    nothing under %s" % ( projectName )
            del struct[projectName]
            continue
            
        for projectStatus in sorted(statuses):
            samples = statuses[projectStatus]
            if len(samples) == 0:
                print "    nothing under %s :: %s" % ( projectName, projectStatus )
                del statuses[projectStatus]
                continue
            
            for sampleId in sorted(samples):
                ids = samples[sampleId]
                if len(ids) == 0:
                    print "    nothing under %s :: %s :: %s" % ( projectName, projectStatus, sampleId )
                    del samples[sampleId]
                    continue
                
                for sequenceTech in sorted(ids):
                    techs = ids[sequenceTech]
                    if len(techs) == 0:
                        print "    nothing under %s :: %s :: %s :: %s" % ( projectName, projectStatus, sampleId, sequenceTech )
                        del ids[sequenceTech]
                        continue
                    
                    for sampleLibrary in sorted(techs):
                        libs = techs[sampleLibrary]
                        if len(libs) == 0:
                            print "    nothing under %s :: %s :: %s :: %s :: %s" % ( projectName, projectStatus, sampleId, sequenceTech, sampleLibrary )
                            del techs[sampleLibrary]
                            continue
                    
                        for fileName in sorted(libs):
                            print "      %s :: %s :: %s" % (projectName, projectStatus, fileName)
                            data = libs[fileName]
                            if not os.path.exists( data.getFileNamePath() ):
                                print "        file name %s does not exists"
                                del libs[fileName]
                                continue
                            
                        libs = techs[sampleLibrary]
                        if len(libs) == 0:
                            print "    nothing under %s :: %s :: %s :: %s :: %s" % ( projectName, projectStatus, sampleId, sequenceTech, sampleLibrary )
                            del techs[sampleLibrary]
                            continue
                    techs = ids[sequenceTech]
                    if len(techs) == 0:
                        print "    nothing under %s :: %s :: %s :: %s" % ( projectName, projectStatus, sampleId, sequenceTech )
                        del ids[sequenceTech]
                        continue
                ids = samples[sampleId]
                if len(ids) == 0:
                    print "    nothing under %s :: %s :: %s" % ( projectName, projectStatus, sampleId )
                    del samples[sampleId]
                    continue
            samples = statuses[projectStatus]
            if len(samples) == 0:
                print "    nothing under %s :: %s" % ( projectName, projectStatus )
                del statuses[projectStatus]
                continue
        statuses = struct[projectName]
        if len(statuses) == 0:
            print "    nothing under %s" % ( projectName )
            del struct[projectName]
            continue
    
    print "  UPDATING STRUCTURE COMPLETED"
    
    return struct

def fixFastQC(struct):
    #struct[projectName][projectStatus][sampleId][sequenceTech][sampleLibrary][fileName] = data
    for projectName in struct:
        for projectStatus in struct[projectName]:
            for sampleId in struct[projectName][projectStatus]:
                for sequenceTech in struct[projectName][projectStatus][sampleId]:
                    for sampleLibrary in struct[projectName][projectStatus][sampleId][sequenceTech]:
                        for fileName in struct[projectName][projectStatus][sampleId][sequenceTech][sampleLibrary]:
                            data = struct[projectName][projectStatus][sampleId][sequenceTech][sampleLibrary][fileName]
                            print "NAME %s STATUS %s ID %s TECH %s LIB %s NAME %s" % (projectName, projectStatus, sampleId, sequenceTech, sampleLibrary, fileName)
                            #print data
                            gc = data.getPlugin('fastqc')
                            gc['fastqcGraphs'] = {}
                            #print gc
                            gc.parseResult()
                            #print gc
                            #sys.exit(0)

                            co = data.getPlugin('contamination')
                            co['contaminationGraph'] = None
                            co.parseContamination()

def main():
    dbdump.saveDump(constants.dumpSetup, setup.getsetup(), startTime=setup.startTime, verbose=setup.debug)
    struct = {}
    idents = {}
    openLog()

    if not setup.ignoreDb:
        dbFile = constants.dumpStruct + dbdump.dumperExt
        print "DB FILE %s" % dbFile
        if os.path.exists(dbFile):
            print "  LOADING DB"
            struct = dbdump.loadDump(dbFile)
            idents = genidents(struct)
            print "  DB LOADED"




    tsv_file = CSVFile(setup.dscFileName) # from setup




    for projectName in tsv_file.listCol("PROJECT_NAME"):
        listProject  = tsv_file.listAll("PROJECT_NAME", projectName)
        #print "  LIST PROJ",listProject

        projectType = tsv_file.getCol(listProject[0], "PROJECT_TYPE")
        print "PROJECT NAME %s TYPE %s" % ( projectName, projectType )

        projectRoot = setup.getProjectRoot(projectName)
        print "PROJECT NAME %s ROOT %s" % ( projectName, projectRoot )

        if projectName not in struct:
            struct[projectName] = {}

        projectStatuses = tsv_file.listCol("STATUS_NAME", listProject)
        projectStatuses.sort()

        for projectStatus in projectStatuses:
            print "  STATUS NAME %s"%projectStatus
            listStatuses = tsv_file.listAll("STATUS_NAME", projectStatus, listProject)
            if projectStatus not in struct[projectName]:
                struct[projectName][projectStatus] = {}

            projectSamples = tsv_file.listCol("SAMPLE_NAME", listStatuses)
            projectSamples.sort()
            for sampleId in projectSamples:
                print "    SAMPLE %s"%sampleId
                listSamples = tsv_file.listAll("SAMPLE_NAME", sampleId, listStatuses)
                if sampleId not in struct[projectName][projectStatus]:
                    struct[projectName][projectStatus][sampleId] = {}

                projectTechs = tsv_file.listCol("TECHNOLOGY", listSamples)
                projectTechs.sort()
                for sequenceTech in projectTechs:
                    print "      TECH %s"% sequenceTech
                    listTechs = tsv_file.listAll("TECHNOLOGY", sequenceTech, listSamples)
                    if sequenceTech not in struct[projectName][projectStatus][sampleId]:
                        struct[projectName][projectStatus][sampleId][sequenceTech] = {}

                    projectLibs = tsv_file.listCol("LIBRARY_NAME", listTechs)
                    projectLibs.sort()
                    for sampleLibrary in projectLibs:
                        print "        LIB %s"%sampleLibrary
                        listLibs = tsv_file.listAll("LIBRARY_NAME", sampleLibrary, listTechs )
                        libType  = tsv_file.getCol( listLibs[0], "LIBRARY_TYPE" )
                        libSize  = tsv_file.getCol( listLibs[0], "LIBRARY_SIZE" )
                        if sampleLibrary not in struct[projectName][projectStatus][sampleId][sequenceTech]:
                            struct[projectName][projectStatus][sampleId][sequenceTech][sampleLibrary] = {}

                        #struct[projectName][projectStatus][sampleId][sequenceTech][sampleLibrary] = {}
                        print "        LIB %s TYPE %s SIZE %s" % (sampleLibrary, libType, libSize)
                        #print listLibs

                        for fileNum in listLibs:
                            fileName  = tsv_file.getCol(fileNum, "FILE_NAME"      )
                            pairName  = tsv_file.getCol(fileNum, "PAIR_NAME"      )
                            print "          FILE NAME '%s' PAIR NAME '%s'"% (fileName,pairName )#, "SIZE",fileSize,"MTIME",fileMTime

                            data    = addData(fileName, projectName, pairName, projectType, projectStatus, sequenceTech, sampleId, sampleLibrary, libType, libSize, struct, idents, plugins)

                            struct[projectName][projectStatus][sampleId][sequenceTech][sampleLibrary][fileName] = data

                            print "ADDING DATA"


    struct = updateStruct(struct)
    #fixFastQC(struct)


    print "  SAVING DATA"
    dbdump.saveDump(constants.dumpStruct, struct, startTime=setup.startTime, verbose=setup.debug)
    #dbdump.saveDump(constants.dbFile,     db,     startTime=setup.startTime, verbose=setup.debug)
    #saveIndividualEntities(db, setup.startTime)
    print "  SAVED"
    #sys.exit(0)


    struct = processDB(struct, numThreads=setup.maxThreads)
    
    struct = updateStruct(struct)
    #dbdump.saveDump(constants.dbFile    , db    , startTime=setup.startTime, verbose=setup.debug)
    dbdump.saveDump(constants.dumpStruct, struct, startTime=setup.startTime, verbose=setup.debug)

    exportDB(struct)




    #structKeys      = struct.keys()
    #structFileNames = []
    #structKeys.sort()

    #for proj in structKeys:
    #    dumpStructName = os.path.join('ui', setup.dumpStruct + '_' + proj)
    #    print "EXPORTING PROJECT %s ON %s" % (proj, dumpStructName)
    #    data           = struct[proj]
    #    #dbdump.saveDump(dumpStructName, data, startTime=setup.startTime, verbose=setup.debug, dobackup=False)
    #    structFileNames.append([proj, setup.dumpStruct + '_' + proj])

    #dumpStructNameKeys = os.path.join('ui', setup.dumpStruct + 'Key')
    #dbdump.saveDump(dumpStructNameKeys, structFileNames, startTime=setup.startTime, verbose=setup.debug, dobackup=False)

    #run.runString('running script', \
    #              'sendMessage %s' % \
    #              "Dear Master. I\\'ve finished running "+sys.argv[0]+" which started at "+setup.startTime+". If you may, please check the result")

    #pipelineSetup.genPipelineSetup([setup.getsetup(), struct], '../pipeline/setup')

if __name__ == '__main__': main()
