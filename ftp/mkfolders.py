#!/usr/bin/python
"""
integral part of the project. created the folder structure for the project
and a list of all the files in the expected places as well as their project
informatiom such as library, species, etc. as described in setup.py
saves the information in PROJECT_ROOT/project_description.csv
"""
#creates subfolder structure
import os,sys
import fnmatch
sys.path.insert(0, 'lib')

checkFolders    = True # check if folders are present
for_real_really = False # really create the folders
# check if there are files in the folders and list them
checkFiles      = True
import setup

def parsePairs(nfos):
    #nfo =   {
    #            'FILENAME'     : fileShort,
    #            'PROJECTNAME'  : folderName,
    #            'PROJECTTYPE'  : setup.types[folderType],
    #            'SAMPLENAME'   : sub,
    #            'TECHNOLOGY'   : tech,
    #            'LIBRARYNAME'  : libName,
    #            'LIBRARYTYPE'  : libType,
    #            'LIBRARYSIZE'  : libLength,
    #            'STATUSNAME'   : statusName,
    #            'PAIRNAME'     : pairName
    #        }

    for pos in range(0, len(nfos), 2):
        try:
            prev     = nfos[pos    ]
            curr     = nfos[pos + 1]
        except IndexError:
            print 'index error'
            print nfos
            sys.exit(1)
        #print "PREV %s CURR %s" % (prev['FILENAME'], curr['FILENAME'])
        prevName = os.path.basename(prev['FILENAME'])
        currName = os.path.basename(curr['FILENAME'])
        pref     = os.path.commonprefix([prevName, currName])
        pref     = pref.rstrip('_')
        pref     = pref.rstrip('.')
        nfos[pos    ]['PAIRNAME'] = pref
        nfos[pos + 1]['PAIRNAME'] = pref

    return nfos

def checkLibs(folderRoot, folderBase, statusFolder, folderName, folderType, sub, statusName, folderLibs, subSubPath, seen, dscTemplate, dscFile, debug):
    for lib in folderLibs:
        tech     = lib[0]
        techPath = os.path.join(subSubPath,  tech   )
        print "      TECH",tech,"PATH",techPath
        if not os.path.exists(techPath): continue
        libSizes = lib[1]

        for libSizeData in libSizes:
            libName   = libSizeData[0]
            libType   = libSizeData[1]
            libLength = libSizeData[2]

            libPath    = os.path.join(techPath, libName)
            print "        LIB NAME",libName,"TYPE",libType,"LENGTH",libLength,'PATH',libPath
            if not os.path.exists(libPath): continue

            for dirInfo in os.walk(libPath, topdown=False, onerror=None, followlinks=False):
                dirpath, dirnames, filenames = dirInfo

                if len(filenames) != 0:
                    if debug: print "  DIRPATH  ",dirpath
                    if debug: print "  DIRNAMES ",dirnames
                    if debug: print "  FILENAMES",filenames,"\n"
                    validFiles = []

                    for extension in setup.constants.extensions:
                        extName = extension[0]
                        #extZip  = extension[1]
                        #print "    extension",extName

                        filteredNames = fnmatch.filter(filenames, extName)
                        #print "      filtered",filteredNames
                        for f in filteredNames:
                            if f not in validFiles: validFiles.append(os.path.join(dirpath, f))

                    validFiles.sort()
                    nfos = []
                    for filename in validFiles:
                        mtime = os.path.getmtime(filename)
                        size  = os.path.getsize(filename)
                        fileShort = filename[len(folderBase) + 1 + len(statusFolder) + 1:]

                        try:
                            seen[fileShort] += 1
                            sys.stderr.write("FILE " + fileShort + "ALREADY EXISTS. OOPS")
                            sys.exit(1)
                        except KeyError:
                            seen[fileShort] = 1

                        nfo =   {
                                    'FILENAME'     : fileShort,
                                    'PROJECTNAME'  : folderName,
                                    'PROJECTTYPE'  : setup.constants.types[folderType],
                                    'SAMPLENAME'   : sub,
                                    'TECHNOLOGY'   : tech,
                                    'LIBRARYNAME'  : libName,
                                    'LIBRARYTYPE'  : libType,
                                    'LIBRARYSIZE'  : libLength,
                                    'STATUSNAME'   : statusName,
                                    'PAIRNAME'     : None
                                }
                        nfos.append(nfo)

                    if tech == 'illumina' and libType in ['PE', 'MP']:
                        nfos = parsePairs(nfos)

                    for nfo in nfos:
                        print "            ",
                        print dscTemplate % nfo
                        dscFile.write(dscTemplate % nfo)

def main():
    requestedFolder = None
    if len(sys.argv) > 1:
        requestedFolder = sys.argv[1]

    dscFile     = open(setup.dscFileName, "w")
    dscHeader   = "#"  + "\t".join([x[0] for x in setup.constants.dscHeaders])   + "\n"
    dscTemplate = "\t".join(["%("+x[0].replace("_","") + ")" + x[1] for x in setup.constants.dscHeaders]) + "\n"
    debug       = setup.debug
    print dscHeader
    dscFile.write(dscHeader)

    for folder in setup.folders:
        if requestedFolder is not None and folder != requestedFolder:
            continue

        #print folder
        folderRoot   = folder[0]
        folderName   = folder[1]
        folderType   = folder[2]
        folderSubs   = folder[3]
        folderLibs   = folder[4]
        folderStruct = folder[6]
        folderBase   = os.path.join(folderRoot, folderName)
        print "FOLDER ROOT NAME",folderName,"PATH",folderRoot

        if checkFolders:
            #CREATE ROOT FOLDER (DENOVO, RIL, RESEQ)
            if not os.path.exists(folderRoot):
                print "ROOT FOLDER %s DOES NOT EXISTS. CREATING" % folderRoot
                try:
                    if for_real_really: os.makedirs(folderRoot)
                except OSError, e:
                    raise
            else:
                print "ROOT FOLDER %s ALREADY EXISTS. SKIPPING" % folderRoot

            #CREATE BASE FOLDER FOR TYPE (SPECIES NAME OF MAPPING TYPE)
            if not os.path.exists(folderBase):
                print "  BASE FOLDER %s DOES NOT EXISTS. CREATING" % folderBase
                try:
                    if for_real_really: os.makedirs(folderBase)
                except OSError, e:
                    raise
            else:
                print "  BASE FOLDER %s ALREADY EXISTS. SKIPPING" % folderBase




            ##FOLDER STRUCTURE
            #structs1 = []
            #structs1.extend(setup.structs)
            #if folderType   == DENOVO:
            #    structs1.append('assembled')
            #
            #elif folderType == MAPPING:
            #    structs1.append('mapped')




            for struct in folderStruct:
                structpath = os.path.join(folderBase, struct)

                if not os.path.exists(structpath):
                    print "    BASE STRUCTURE PATH %s DOES NOT EXISTS. CREATING" % structpath

                    try:
                        if for_real_really: os.makedirs(structpath)
                    except OSError, e:
                        raise
                else:
                    print "    BASE STRUCTURESUB PATH %s EXISTS. SKIPPING" % structpath


            # CREATE LIB NAMES FOR _PRELIM FOLDER
            targets = []
            if folderSubs is not None:
                prePath = os.path.join(folderBase, setup.statuses[0][0])
                rawPath = os.path.join(folderBase, setup.statuses[1][0])

                for sub in folderSubs:
                    subPrePath = os.path.join(prePath, sub)
                    subRawPath = os.path.join(rawPath, sub)

                    print "      SUB",subPrePath,
                    if not os.path.exists(subPrePath) and not os.path.exists(subRawPath):
                        targets.append([sub, subPrePath])
                        print " DOES NOT EXISTS. CREATING"

                        try:
                            if for_real_really: os.makedirs(subPrePath)
                        except OSError, e:
                            raise
                    else:
                        print " EXISTS. SKIPPING"
            else:
                prePath = os.path.join(folderBase, setup.statuses[0][0])
                rawPath = os.path.join(folderBase, setup.statuses[1][0])
                if not os.path.exists(rawPath):
                    targets = [[None, prePath]]



            #CREATE FOLDERS FOR DATA
            for lib in folderLibs:
                tech     = lib[0]
                libSizes = lib[1]
                print "      TECH",tech

                for libSizeData in libSizes:
                    libSize   = libSizeData[0]
                    libType   = libSizeData[1]
                    libLength = libSizeData[2]
                    print "        LIB SIZE",libSize,"TYPE",libType,"LENGTH",libLength

                    for target in targets:
                        targetName   = target[0]
                        subPath      = target[1]
                        techPath     = os.path.join(subPath,  tech   )
                        libPath      = os.path.join(techPath, libSize)

                        if not os.path.exists(libPath):
                            print "          LIB PATH %s DOES NOT EXISTS. CREATING" % libPath
                            try:
                                if for_real_really: os.makedirs(sub1path)
                            except OSError, e:
                                raise
                        else:
                            print "          LIB PATH %s EXISTS. SKIPPING" % libPath



        if checkFiles:
            print "CHECKING FILES"
            seen = {}

            #CHEKING FOR FILES
            for status in setup.statuses:
                statusFolder = status[0]
                statusName   = status[1]
                subPath      = os.path.join(folderBase, statusFolder)
                print "  STATUS FOLDER",statusFolder,"NAME",statusName,"SUB PATH",subPath

                if folderSubs is not None:
                    for sub in folderSubs:
                        subSubPath = os.path.join(subPath, sub)
                        print "    SUB",sub,"PATH",subSubPath
                        if not os.path.exists(subSubPath): continue

                        checkLibs(folderRoot, folderBase, statusFolder, folderName, folderType, sub, statusName, folderLibs, subSubPath, seen, dscTemplate, dscFile, debug)
                else: #no folder subs.
                    checkLibs(    folderRoot, folderBase, statusFolder, folderName, folderType, "" , statusName, folderLibs, subPath,    seen, dscTemplate, dscFile, debug)

    dscFile.close()
    print "finished"

if __name__ == '__main__': main()
