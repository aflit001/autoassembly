#!/usr/bin/python
import os,sys
import pprint
import shutil
from glob import glob

sys.path.append('..')
import setup
import run

runForReal = True
outDir     = '/mnt/nexenta/assembly/nobackup/virus/'
inputList  = '/home/assembly/tomato150/project_description.csv'
bowtieBase = '/home/assembly/progs/fastqscreen/FastQScreen_v0.3.1/db/'
bowtieDBs  = ['Viruses', 'PhX174', 'EcoliM', 'Viruses', 'Viruses2', 'Insects' ]
bowtieExe  = '/usr/bin/bowtie2'
bowtieOpts = '--threads 12 --local --very-fast-local -k 2 --maxins 100 --time'
#--sam-nohead --sam-nosq
forbiddenStats = ['FILTERED', 'PREFILTER', 'PRELIMINARY']
desiredFolders = ['reseq']

def main():
    pp      = pprint.pprint
    db      = {}
    classes =     {
                    'PRELIMINARY': 0,
                    'PREFILTER'  : 1,
                    'CHECKED'    : 2,
                    'FILTERED'   : 3
                }

    with open(inputList, 'r') as il:
        for line in il.readlines():
            if line[0] == '#': continue
            line     = line.strip()
            #print "  LINE",line

            cols     = line.split("\t")
            #print "    COLS",cols


            fn       = cols[0]
            folder   = cols[1]
            libType  = cols[2]
            libTech  = cols[4]
            libStat  = cols[8]
            sub      = setup.getStatusFolder(libStat)
            base     = setup.getProjectRoot( folder)

            if libTech != "illumina":
                #sys.stderr.write("LIBTYPE %s NOT ALLOWED\n" % libTech)
                continue
            if libStat in forbiddenStats:
                continue

            if folder not in desiredFolders:
                continue

            filePath = ""
            filePath = os.path.join(base, folder, sub, fn)

            print "FILENAME %s FOLDER %s LIBTYPE %s LIBTECH %s LIBSTAT %s SUB %s BASE %s PATH %s" % \
            (fn, folder, libType, libTech, libStat, sub, base, filePath)

            if not os.path.exists(filePath):
                sys.stderr.write("FILE PATH %s DOES NOT EXISTS\n" % filePath)
                sys.exit(1)

            #print "FN",filePath
            if not   folder in db:
                db[folder]          = {}
                db[folder][libStat] = []
            elif libStat not in db[folder]:
                db[folder][libStat] = []

            db[folder][libStat].append([fn, filePath])

    #sys.exit(0)
    db2    = {}
    dbKeys = db.keys()
    dbKeys.sort()
    for folder in dbKeys:
        stats = db[folder]
        if folder not in db2:
            db2[folder] = []

        if len(stats) == 1:
            data = stats[stats.keys()[0]]
            db2[folder].extend(data)
        else:
            lastKey = stats.keys()[0]
            for key in stats.keys():
                if classes[key] > lastKey:
                    lastKey = classes[key]
            data = stats[lastKey]
            db2[folder].extend(data)

    #pp(db2)


    tmpGlob = os.path.join(outDir, "*.tmp")
    #print fqGlob
    for f in glob(tmpGlob):
        print "    FILES TO DELETE %s"%f
        try:            os.remove(f)
        except OSError: pass


    folderCount  = 0
    somethinRun1 = False
    db2Keys      = db2.keys()
    db2Keys.sort()
    for folder in db2Keys:
        folderCount += 1
        print "FOLDER",folder,"%d/%d" % (folderCount, len(db2Keys))
        files = db2[folder]
        files.sort()

        fileCount    = 0
        somethinRun2 = False
        for fileData in files:
            fileCount += 1
            fileName      = fileData[0]
            filePath      = fileData[1]
            print "  FILE NAME %s %d/%d - %d/%d FILE PATH %s" % (fileName, folderCount, len(db2Keys), fileCount, len(files), filePath)

            dbCount      = 0
            somethinRun3 = False
            for bowtieDB in bowtieDBs:
                dbCount      += 1
                outName       = folder + "_" + fileName.replace('/', '_')
                outPathSam    = os.path.join(outDir, outName + '_' + bowtieDB + ".sam")
                outPathBam    = outPathSam  + ".bam"
                outPathBamS   = outPathBam  + ".sorted.bam"
                outPathBai    = outPathBamS + ".bai"
                outPathCov    = outPathBamS + ".cov"

                print "    DATABASE %s %d/%d - %d/%d - %d/%d" % (bowtieDB, folderCount, len(db2Keys), fileCount, len(files), dbCount, len(bowtieDBs))

                if runForReal:
                    if not os.path.exists(outPathSam) and not os.path.exists(outPathBam) and not os.path.exists(outPathBamS):
                        outPathTmp = outPathSam + '.tmp'
                        if os.path.exists(outPathTmp):
                            os.remove(outPathTmp)

                        cmd = "%(exe)s %(opts)s %(db)s <(pigz -dc %(in)s) | perl -ane 'print if $F[2] ne \"*\"' | pv --buffer-size 16M -q > %(out)s" %    {
                                                                'exe' : bowtieExe,
                                                                'opts': bowtieOpts,
                                                                'db'  : os.path.join(bowtieBase, bowtieDB),
                                                                'in'  : filePath,
                                                                'out' : outPathTmp
                                                            }
                        print "      CRETING SAM FROM %s TO %s" % (filePath, outPathSam)
                        res = run.runString("        creating sam", cmd)
                        if res != 0:
                            sys.stderr.write('ERROR RUNNING\n')
                            sys.exit(1)

                        if not os.path.exists(outPathTmp):
                            sys.exit(1)

                        if os.path.exists(outPathSam):
                            os.remove(outPathSam)

                        if os.path.exists(outPathTmp):
                            shutil.move(outPathTmp, outPathSam)
                        else:
                            sys.stderr.write('COULD NOT FIND OUTPUT: %s' % outPathTmp)
                            sys.exit(1)



                    if not os.path.exists(outPathBam) and not os.path.exists(outPathBamS):
                        outPathTmp = outPathBam + '.tmp'
                        if os.path.exists(outPathTmp):
                            os.remove(outPathTmp)

                        with open(outPathSam, 'r') as s:
                            samSize = os.path.getsize(outPathSam)
                            if samSize > (1024 * 1024):
                                s.seek(-(1024 * 1024), 2)

                            lastLine = ""
                            for line in s:
                                lastLine = line
                            if lastLine[0] == '@':
                                print "empty sam file"
                                continue

                        print "      CONVERTING %s TO %s" % (outPathSam, outPathBam)
                        res1 = run.runString("        creating sam to bam", 'samtools view -Sb %s  | pv --buffer-size 16M -q> %s' % (outPathSam, outPathTmp))
                        if res1 != 0:
                            if os.path.exists(outPathTmp): os.remove(outPathTmp)
                            sys.exit(1)

                        if not os.path.exists(outPathTmp):
                            sys.exit(1)

                        if os.path.exists(outPathBam):
                            os.remove(outPathBam)

                        if os.path.exists(outPathTmp):
                            shutil.move(outPathTmp, outPathBam)
                        else:
                            sys.stderr.write('COULD NOT FIND OUTPUT: %s' % outPathTmp)
                            sys.exit(1)



                    if not os.path.exists(outPathBamS):
                        outPathTmp = outPathBamS + '.tmp'
                        if os.path.exists(outPathTmp):
                            os.remove(outPathTmp)

                        print "      SORTING BAM %s TO %s" % (outPathBam, outPathBamS)
                        res2 = run.runString("        sorting bam", 'samtools sort %s -o %s  | pv --buffer-size 16M -q > %s' % (outPathBam, outPathTmp, outPathTmp))
                        if res2 != 0:
                            if os.path.exists(outPathTmp): os.remove(outPathTmp)
                            sys.exit(1)


                        if not os.path.exists(outPathTmp):
                            sys.exit(1)

                        if os.path.exists(outPathBamS):
                            os.remove(outPathBamS)

                        if os.path.exists(outPathTmp):
                            shutil.move(outPathTmp, outPathBamS)
                        else:
                            sys.stderr.write('COULD NOT FIND OUTPUT: %s' % outPathTmp)
                            sys.exit(1)

                        if os.path.exists(outPathSam):
                            os.remove(outPathSam)

                        if os.path.exists(outPathBam):
                            os.remove(outPathBam)



                    if not os.path.exists(outPathBai):
                        print "      CREATING INDEX TO %s IN %s" % (outPathBamS, outPathBai)
                        res3 = run.runString("        creating bam index", 'samtools index %s' % (outPathBamS))
                        if res3 != 0:
                            if os.path.exists(outPathBai): os.remove(outPathBai)
                            sys.exit(1)

                        if not os.path.exists(outPathBai):
                            sys.exit(1)



                    if not os.path.exists(outPathCov):
                        outPathTmp = outPathCov + '.tmp'
                        if os.path.exists(outPathCov):
                            os.remove(outPathCov)

                        print "      CREATING COVERAGE TO %s IN %s" % (outPathBamS, outPathCov)
                        res4 = run.runString("        creating coverage", 'genomeCoverageBed -d -ibam %s | pv --buffer-size 16M -q > %s' % (outPathBamS, outPathTmp))
                        if res4 != 0:
                            if os.path.exists(outPathTmp): os.remove(outPathTmp)
                            sys.exit(1)

                        if not os.path.exists(outPathTmp):
                            sys.exit(1)

                        if os.path.exists(outPathCov):
                            os.remove(outPathCov)

                        if os.path.exists(outPathTmp):
                            shutil.move(outPathTmp, outPathCov)
                        else:
                            sys.stderr.write('COULD NOT FIND OUTPUT: %s' % outPathTmp)
                            sys.exit(1)



                else:
                    print "      FILE ALREADY EXISTS. SKIPPING"
            if somethinRun3:
                run.runString('sending message', \
                  'sendMessage %s' % \
                  "Dear Master. I\\'ve finished running "+sys.argv[0]+" for "+folder+" file "+fileData[0]+". If you may, please check the result")
        if somethinRun2:
            run.runString('sending message', \
                  'sendMessage %s' % \
                  "Dear Master. I\\'ve finished running "+sys.argv[0]+" for "+folder+". If you may, please check the result")
    if somethinRun1:
        run.runString('sending message', \
                  'sendMessage %s' % \
                  "Dear Master. I\\'ve finished running "+sys.argv[0]+". If you may, please check the result")

#outDir     = '/home/assembly/nobackup/virus'
#bowtieDB   = '/home/assembly/progs/fastqscreen/FastQScreen_v0.3.1/db/Viruses'
#bowtieExe  = '/usr/bin/bowtie2'
#bowtieOpts = '-p 8 --local --very-fast-local -k 2 --maxins 100'

if __name__ == '__main__': main()
