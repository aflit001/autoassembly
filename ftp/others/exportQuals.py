#!/usr/bin/python
#prepare files for ftp

import cPickle as pickle
import pprint
import time, datetime
import prepareFtpFiles
from prepareFtpFiles import GenData

try:
    #import yaml
    import yaml
    useYaml = True

    try:
        from yaml import CLoader as Loader, CDumper as Dumper
        print "USING YAML C VERSION"
    except ImportError:
        from yaml import Loader, Dumper
        print "USING YAML PYTHON VERSION"
except ImportError:
    print "NO YAML INSTALLED"
    useYaml = False



useYAML               = prepareFtpFiles.useYAML  # TRUE use yaml (if available) or FALSE use pickle 

startTime  = time.time()
dbFile     = prepareFtpFiles.dbFile  # db file prefix to be added .yaml and/or .pickle containing the files information
dumpStruct = prepareFtpFiles.dumpStruct # db file prefix to be added .yaml and/or .pickle containing the structure and files information
dumpSetup  = prepareFtpFiles.dumpSetup  # db file prefix to be added .yaml and/or .pickle containing the setup of the project
loadDb     = prepareFtpFiles.loadDump
pp         = pprint.pprint

if __name__ == '__main__':
	db     = loadDb(dbFile)
	dbKeys = db.keys()
	dbKeys.sort()
	
	byLetter  = {}
	byPhred   = []
	filesSeen = {}
	maxPhred  = 0
	
	for fileName in dbKeys:
		data          = db[fileName]
		quals         = data.getQuals()
		qualsConv     = data.getQualsConv()
		qualsConvProp = data.getQualsConvProp()
		fqType        = data.getFastqType()
		fqTypeName    = prepareFtpFiles.fqFormats[fqType][3]
		fqTypeSub     = prepareFtpFiles.fqFormats[fqType][4]
		print fileName, "type",fqTypeName
		
		qualsKeys     = quals.keys()
		qualsKeys.sort()
		
		filesSeen[fileName] = 1
		
		for keyPos in range(len(qualsKeys)):
			keyName  = qualsKeys[keyPos]
			keyOrd   = ord(keyName)
			keyPhred = keyOrd - fqTypeSub
			print "KEY",keyName,"ORD",keyOrd,"PHRED",keyPhred
			
			if not byLetter.has_key(keyName):
				byLetter[keyName]        = {}
			
			if not byLetter[keyName].has_key(fileName):
				byLetter[keyName][fileName] = 0
			
			byLetter[keyName][fileName]  = quals[keyName]
			qualConv     = qualsConv[    keyPhred + 5]
			qualConvProp = qualsConvProp[keyPhred + 5]
			if keyPhred + 6 > maxPhred: maxPhred = keyPhred + 6
			
			while len(byPhred) <= keyPhred + 5:
				byPhred.append({})
			
			byPhred[keyPhred + 5][fileName] = [qualConv, qualConvProp]
	

	qualTypes =	[
					open('quals.csv',         'w'),
					open('qualsConv.csv',     'w'),
					open('qualsConvProp.csv', 'w')
				]
	
	#pp(res)
	letterKeys = byLetter.keys()
	letterKeys.sort()
	
	lineNum = 0
	for qualName in letterKeys:
		qualOrd   = str(ord(qualName))
		files     = byLetter[qualName]
		filesKeys = filesSeen.keys()
		filesKeys.sort()
	
		if lineNum == 0:
			qualTypes[0].write("#CODE\tORD\t")
			qualTypes[0].write("\t".join(filesKeys) + "\n")
	
	
		colNum = 0
		for fileName in filesKeys:
			fileData = 0
			
			if files.has_key(fileName):
				fileData = files[fileName]
			
			if colNum == 0:
				qualTypes[0].write(qualName + "\t")
				qualTypes[0].write(qualOrd  + "\t")
			else:
				qualTypes[0].write("\t")
				
			qualTypes[0].write(str(fileData))
			colNum += 1
			
		qualTypes[0].write("\n")
		lineNum += 1
	
	print "MAX",maxPhred
	lineNum = 0
	for qual in range(maxPhred):
		qualReal  = qual - 5
		files     = byPhred[qual]
		filesKeys = filesSeen.keys()
		filesKeys.sort()
		
		if lineNum == 0:
			qualTypes[1].write("#PHRED\t")
			qualTypes[1].write("\t".join(filesKeys) + "\n")
			qualTypes[2].write("#PHRED\t")
			qualTypes[2].write("\t".join(filesKeys) + "\n")
		colNum = 0
		for fileName in filesKeys:
			fileData = [0,0]
			
			if files.has_key(fileName):
				fileData = files[fileName]
			
			if colNum == 0:
				qualTypes[1].write("%d"%qualReal + "\t")
				qualTypes[2].write("%d"%qualReal + "\t")
			else:
				qualTypes[1].write("\t")
				qualTypes[2].write("\t")
				
			qualTypes[1].write(str(fileData[0]))
			qualTypes[2].write(str(fileData[1]))
			colNum += 1
			
		qualTypes[1].write("\n")
		qualTypes[2].write("\n")
		lineNum += 1
	
	for f in qualTypes:
		f.close()

#2058839706
#20184703
#2038655003
#
#18330347792
#181488592
